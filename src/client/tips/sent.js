import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { FormattedRelative } from "react-intl";
import { compose } from 'redux';
import { Icon, Table } from 'antd';
import Avatar from '../components/Avatar';
import ReduxInfiniteScroll from "../vendor/ReduxInfiniteScroll";
import Loading from "../components/Icon/Loading";
import { fetchTipsSent, fetchTipsSentMore } from './actions';
import '../styles/modules/account_list.less';
import ShowMemo from '../components/ShowMemo';

class TipsSent extends React.Component {
  componentDidMount() {
    if (this.props.tips_sent === false) this.props.fetchTipsSent();
  }
  
  render() {
    const columns = [
      {
        title: 'To',
        dataIndex: 'to',
        key: 'to',
        render: (text) => (text ? <Link to={`/@${text}`} style={{display: 'inline-flex'}}>
          <Avatar size={24} username={text}/>
          <span style={{marginLeft: '5px'}}>{text}</span>
        </Link> : null),
      },
      {
        title: 'Date',
        dataIndex: 'timestamp',
        key: 'timestamp',
        render: (text) => (<FormattedRelative value={`${text}Z`}/>)
      },
      {
        title: 'Amount',
        dataIndex: 'amount',
        key: 'amount',
      },
      {
        title: '',
        dataIndex: 'memo',
        key: 'memo',
        render: (text) => ( <ShowMemo user={this.props.user.name} memo={text} icon={true} /> )
      },
      {
        title: '',
        dataIndex: 'link',
        key: 'link',
        render: (text) => (text ? <Link to={text}><Icon type="link" /></Link> : null),
      },
    ];

    let data = [];
    if (this.props.tips_sent) {
      data = this.props.tips_sent.map((item) => {
          const [idx, obj] = item;
          const {timestamp, op} = obj;
          const [op_type, op_obj] = op;
          let to = null;
          let amount = null;
          let link = null;
          let memo = null;
          if (op_type === 'social_action') {
            const [action_type, action_obj] = op_obj.action;
            memo = action_obj.memo;
            if (action_type === 5) { // to user
              to = action_obj.to || op_obj.account;
              amount = action_obj.amount;
            } else if (action_type === 6) { // to comment
              to = action_obj.author;
              amount = action_obj.amount;
              link = `/@${action_obj.author}/${action_obj.permlink}`;
            }
          }

          return {
            id: `id_${idx}`,
            to,
            timestamp,
            amount,
            link,
            memo,
          };
        }
      );
    }

    return (
      <div>
        <ReduxInfiniteScroll
          hasMore={this.props.tips_sent_hasMore}
          loadingMore={this.props.loading}
          loader={<Loading/>}
          loadMore={this.props.fetchTipsSentMore}
          elementIsScrollable={false}
          items={[<Table key="sent-tips-list" rowKey="id" pagination={false} columns={columns} dataSource={data}/>]}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    ...ownProps,
    ...state.tips,
    ...state.auth,
  };
};
const mapDispatchToProps = {
  fetchTipsSent,
  fetchTipsSentMore
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);
export default compose(
  withRouter,
  withConnect
)(TipsSent);
