/* eslint-disable react/no-unescaped-entities */
import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { withRouter } from 'react-router-dom';
import { injectIntl } from 'react-intl';
import './TextPage.less';

const DonatePage = ({ staticContext }) => {
  if (staticContext) {
    staticContext.status = 404; // eslint-disable-line no-param-reassign
  }
  return (
    <div className="TextPage container">
      <Helmet>
        <title>Donate</title>
      </Helmet>
      <div className="TextPage__body">
        <h2>Donate to Whaleshares!</h2>
        <div>
          <h4>[BTC] Donate To Bitcoin Address:</h4>
          <p align="center">
            <b>13wGUBdnDrbvUrGFAvwvj1U4Q7rbHoBg9i</b>
          </p>
          <p align="center" className="qr-code">
            <img style={{ margin: 5 }} src="/images/qr_btc.png" alt="" />
          </p>
          <hr />
        </div>

        <div>
          <h4>[ETH] Donate To Ethereum Address:</h4>
          <p align="center">
            <b>0xf1185D64C1b03AaE2B903830EB89F734eC8f7620</b>
          </p>
          <p align="center" className="qr-code">
            <img style={{ margin: 5 }} src="/images/qr_eth.png" alt="" />
          </p>
          <hr />
        </div>

        <div>
          <h4>[LTC] Donate To Litecoin Address:</h4>
          <p align="center">
            <b>LMwAWqbzRpgmFtrGzJajxsEEoo8Hd4Vq4f</b>
          </p>
          <p align="center" className="qr-code">
            <img style={{ margin: 5 }} src="/images/qr_ltc.png" alt="" />
          </p>
          <hr />
        </div>

        <div>
          <h4>[BTS] Donate To Bitshares account:</h4>
          <p align="center">
            <b>wlsbts</b>
          </p>
          <hr />
        </div>

        <div>
          <h4>[WLS] Donate To Whaleshares account:</h4>
          <p align="center">
            <b>devfund</b>
          </p>
          <hr />
        </div>

        <p>Wishing you all the best,</p>
        <p>The Whaleshares Development Team</p>
      </div>
    </div>
  );
};

DonatePage.propTypes = {
  staticContext: PropTypes.shape(),
};

DonatePage.defaultProps = {
  staticContext: null,
};

export default withRouter(injectIntl(DonatePage));

/* eslint-enable react/no-unescaped-entities */
