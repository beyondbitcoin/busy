import React from 'react';
import { FormattedMessage } from 'react-intl';
// import steemAPI from '../steemAPI';
import busyAPI from '../busyAPI';
import Loading from '../components/Icon/Loading';
import Affix from '../components/Utils/Affix';
import SidenavMain from '../components/Navigation/SidenavMain';
import Tag from './Tag';
// import TopicsList from './TopicsList.less';

const sortTags = tags =>
  Object.keys(tags)
    .sort((key1, key2) => tags[key1].trending - tags[key2].trending)
    .reverse()
    .map(tagKey => tags[tagKey]);

export default class Tags extends React.Component {
  state = {
    tags: {},
  };

  componentDidMount() {
    // // {"id":0,"jsonrpc":"2.0","method":"call","params":["database_api","get_state",["tags"]]}
    // steemAPI.chainLib.api.getState('tags', (err, result) => {
    //   this.setState({ tags: result.tags });
    // });

    (async () => {
      try {
        const result = await busyAPI.sendAsync('database_api', 'get_state', ['tags']);
        this.setState({ tags: result.tags });
      } catch (error) {
        console.log(error);
      }
    })();
  }

  render() {
    const { tags } = this.state;
    const sortedTags = sortTags(tags);
    const isFetching = !sortedTags.length;

    return (
      <div className="main-panel">
        <div className="feed-layout container">
          <Affix className="leftContainer leftContainer__user" stickPosition={72}>
            <div className="left">
              <SidenavMain />
            </div>
          </Affix>
          <div className="center_full">
            <div className="TopicsList">
              <h1>
                <FormattedMessage id="trending_topics" defaultMessage="Trending topics" />
              </h1>
              <ul>{sortedTags.map(tag => (tag.name ? <Tag key={tag.name} tag={tag} /> : []))}</ul>

              {isFetching && (
                <div className="my-5">
                  <Loading />
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
