import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import { FormattedMessage, FormattedRelative } from 'react-intl';
import Avatar from '../../Avatar';
import { Icon, Popover } from 'antd';
import ShowMemo from '../../ShowMemo';
import { EXPLORER_URL } from '../../../../common/constants/settings';

const NotificationItem = (props) => {
  const {obj, currentAuthUsername, read, onClick} = props;
  const {timestamp} = obj;
  const [opt_type, opt_obj] = obj.op;

  const notificationTimestamp = (
    <div className="Notification__text__date"><FormattedRelative value={`${timestamp}Z`}/></div>
  );

  const defaultItem = (
    <a
      href={`${EXPLORER_URL}/#/block/${obj.block}`}
      onClick={onClick}
      target='_blank'
      rel = 'noopener noreferrer'
      className={classNames('Notification', {
        'Notification--unread': !read,
      })}
    >
      <div className="Notification__text">
        <div className="Notification__text__message">{opt_type} <br />{JSON.stringify(opt_obj)}</div>
        {notificationTimestamp}
      </div>
    </a>
  );

  const memoField = text => {
    return ( <ShowMemo user={currentAuthUsername} memo={text} icon={false} /> );
  }

  switch (opt_type) {
    case 'vote': {
      const { voter, author, permlink, weight } = opt_obj;
      if (voter === currentAuthUsername) return defaultItem;

      const postURL = `/@${author}/${permlink}`;
      let message = (
        <FormattedMessage
          id="notification_unvoted_username_post"
          defaultMessage="{username} unvoted your post"
          values={{ username: <span className="username">{voter}</span> }}
        />
      );

      if (weight > 0) {
        message = (
          <FormattedMessage
            id="notification_upvoted_username_post"
            defaultMessage="{username} upvoted your post"
            values={{ username: <span className="username">{voter}</span> }}
          />
        );
      } else if (weight < 0) {
        message = (
          <FormattedMessage
            id="notification_downvoted_username_post"
            defaultMessage="{username} downvoted your post"
            values={{ username: <span className="username">{voter}</span> }}
          />
        );
      }

      return (
        <Link
          role="presentation"
          onClick={onClick}
          className={classNames('Notification', {
            'Notification--unread': !read,
          })}
          to={postURL}
        >
          <Avatar username={voter} size={32} />
          <div className="Notification__text">
            <div className="Notification__text__message">{message}</div>
            {notificationTimestamp}
          </div>
        </Link>
      );
    }
    case 'transfer': {
      const {from, to, amount, memo} = opt_obj;
      const memoText = memoField(memo);

      if (from === currentAuthUsername) return defaultItem;

      return (
        <Link
          to={`/@${from}`}
          onClick={onClick}
          className={classNames('Notification', {
            'Notification--unread': !read,
          })}
        >
          <Avatar username={from} size={32}/>
          <div className="Notification__text">
            <div className="Notification__text__message">
              <span className="username">{from}</span> transfered {amount} to your balance
            </div>
            {memoText}
            {notificationTimestamp}
          </div>
        </Link>
      );
    }
    case 'transfer_to_vesting': {
      const {from, to, amount} = opt_obj;
      if (from === currentAuthUsername) return defaultItem;

      return (
        <Link
          to={`/@${from}`}
          onClick={onClick}
          className={classNames('Notification', {
            'Notification--unread': !read,
          })}
        >
          <Avatar username={from} size={32}/>
          <div className="Notification__text">
            <div className="Notification__text__message">
              <span className="username">{from}</span> transfered {amount} to your vesting
            </div>
            {notificationTimestamp}
          </div>
        </Link>
      );
    }
    case 'withdraw_vesting':
      return defaultItem;
    case 'account_update' :
      return defaultItem;
    case 'account_action': {
      const {account} = opt_obj;
      const [action_type, action] = opt_obj.action;
      if (account === currentAuthUsername) return defaultItem;

      switch (action_type) {
        // case 0 : { // account_action_create_pod
        //   break;
        // }
        // case 1: { // account_action_transfer_to_tip
        //   break;
        // }
        case 2: { // account_action_htlc_create
          const {to, amount} = action;
          return (
            <Link
              to={`/@${account}`}
              onClick={onClick}
              className={classNames('Notification', {
                'Notification--unread': !read,
              })}
            >
              <Avatar username={account} size={32}/>
              <div className="Notification__text">
                <div className="Notification__text__message">
                  <span className="username">{account}</span> created a htlc amount {amount} to you.
                </div>
                {notificationTimestamp}
              </div>
            </Link>
          );
        }
        // case 3: { // account_action_htlc_update
        //   break;
        // }
        // case 4: { // account_action_htlc_redeem
        //   break;
        // }
        default:
          console.log(`opt_type=${opt_type}, action_type=${action_type}`);
          return defaultItem;
      }
    }
    case 'social_action': {
      const {account} = opt_obj;
      const [action_type, action] = opt_obj.action;
      if (account === currentAuthUsername) return defaultItem;

      switch (action_type) {
        case 0: { // social_action_comment_create
          const {permlink} = action;
          const commentURL = `/@${account}/${permlink}`;
          return (
            <Link
              to={commentURL}
              onClick={onClick}
              className={classNames('Notification', {
                'Notification--unread': !read,
              })}
            >
              <Avatar username={account} size={32}/>
              <div className="Notification__text">
                <div className="Notification__text__message">
                  <FormattedMessage
                    id="notification_reply_username_post"
                    defaultMessage="{username} replied to your post"
                    values={{
                      username: <span className="username">{account}</span>,
                    }}
                  />
                </div>
                {notificationTimestamp}
              </div>
            </Link>
          );
        }
        case 1: // social_action_comment_update
          return defaultItem;
        case 2: // social_action_comment_delete
          return defaultItem;
        case 3: { // social_action_claim_vesting_reward
          const {amount, memo} = action;
          const memoText = memoField(memo);

          return (
            <Link
              to={`/@${account}`}
              onClick={onClick}
              className={classNames('Notification', {
                'Notification--unread': !read,
              })}
            >
              <Avatar username={account} size={32}/>
              <div className="Notification__text">
                <div className="Notification__text__message">
                  <span className="username">{account}</span> claimed {amount} to your WHALESTAKE
                </div>
                {memoText}
                {notificationTimestamp}
              </div>
            </Link>
          );
        }
        // case 4: // social_action_claim_vesting_reward_tip
        case 5: { // social_action_user_tip
          const {amount, memo} = action;
          const memoText = memoField(memo);

          return (
            <Link
              to={`/@${account}`}
              onClick={onClick}
              className={classNames('Notification', {
                'Notification--unread': !read,
              })}
            >
              <Avatar username={account} size={32}/>
              <div className="Notification__text">
                <div className="Notification__text__message">
                  <span className="username">{account}</span> tipped you {amount}
                </div>
                {memoText}
                {notificationTimestamp}
              </div>
            </Link>
          );
        }
        case 6: { // social_action_comment_tip
          const {author, permlink, amount, memo} = action;
          const commentURL = `/@${author}/${permlink}`;
          const memoText = memoField(memo);         

          return (
            <Link
              to={commentURL}
              onClick={onClick}
              className={classNames('Notification', {
                'Notification--unread': !read,
              })}
            >
              <Avatar username={account} size={32}/>
              <div className="Notification__text">
                <div className="Notification__text__message">
                  <span className="username">{account}</span> tipped {amount} to your post
                </div>
                {memoText}
                {notificationTimestamp}
              </div>
            </Link>
          );
        }
        default:
          console.log(`opt_type=${opt_type}, action_type=${action_type}`);
          return defaultItem;
      }
    }
    case 'account_witness_vote' : {
      const {account, witness, approve} = opt_obj;
      if (account === currentAuthUsername) return defaultItem;
      const verb = approve ? 'approved' : 'unapproved';

      return (<Link
        to={`/@${account}`}
        className={classNames('Notification', {
          'Notification--unread': !read,
        })}
        onClick={onClick}
      >
        <Avatar username={account} size={32}/>
        <div className="Notification__text">
          <div className="Notification__text__message">
              <FormattedMessage
                id={`notification_${verb}_witness`}
                defaultMessage="{username} {verb} your witness"
                values={{
                  username: <span className="username">{account}</span>,
                  verb: verb
                }}
              />
          </div>
          {notificationTimestamp}
        </div>
      </Link>);
    }
    case 'account_witness_proxy' : {
      const {account, proxy} = opt_obj;
      if (account === currentAuthUsername) return defaultItem;

      return (<Link
        to={`/@${account}`}
        className={classNames('Notification', {
          'Notification--unread': !read,
        })}
        onClick={onClick}
      >
        <Avatar username={account} size={32}/>
        <div className="Notification__text">
          <div className="Notification__text__message">
            <span className="username">{account}</span> appointed you as proxy for their witness votes
          </div>
          {notificationTimestamp}
        </div>
      </Link>);
    }    
    case 'comment_options' :
      return defaultItem;
    case 'htlc_virtual': {
      const [action_type, action] = opt_obj.action;
      switch (action_type) {
        case 0: { // htlc_redeemed_virtual_action
          const {htlc_id, from, to, amount, redeemer} = action;
          return (
            <Link
              to={'###'}
              onClick={onClick}
              className={classNames('Notification', {
                'Notification--unread': !read,
              })}
            >
              <Avatar username={redeemer} size={32}/>
              <div className="Notification__text">
                <div className="Notification__text__message">
                  <span className="username">{redeemer}</span> redeemed {amount} of htlc #{htlc_id} from <span
                  className="username">{from}</span> to <span className="username">{to}</span>
                </div>
                {notificationTimestamp}
              </div>
            </Link>
          );
        }
        // case 1: { // htlc_refund_virtual_action
        // }
        default:
          console.log(`opt_type=${opt_type}, action_type=${action_type}`);
          return defaultItem;
      }
    }
    case 'friend_action': {
      const {account, another} = opt_obj;
      const [action_type, action] = opt_obj.action;
      if (account === currentAuthUsername) return defaultItem;
      const messages = [
        'sent you a friend request', // action_type = 0
        'cancelled the friend request', // action_type = 1
        'accepted your friend request', // action_type = 2
        'rejected your friend request', // action_type = 3
        'unfriended you', // action_type = 4
        ]

      let message = messages[action_type];

      switch (action_type) {
        case 0:  // friend_action_send_request
        case 2:  // friend_action_accept_request
                message += '<br/> <h6>(Remove Friends from your Followed list if present)</h6>';
        case 0:  // friend_action_send_request
        case 1:  // friend_action_cancel_request
        case 3:  // friend_action_reject_request
        case 4: { // friend_action_unfriend
          return (
            <Link
              to={`/@${account}`}
              onClick={onClick}
              className={classNames('Notification', {
                'Notification--unread': !read,
              })}
            >
              <Avatar username={account} size={32}/>
              <div className="Notification__text">
                <div className="Notification__text__message">
                  <span className="username">{account}</span><div dangerouslySetInnerHTML={{ __html: message }}/>
                </div>
                {notificationTimestamp}
              </div>
            </Link>
          );
        }
        default:
          console.log(`opt_type=${opt_type}, action_type=${action_type}`);
          return defaultItem;
      }
    }
    case 'pod_action': {
      const {account, pod} = opt_obj;
      const [action_type, action] = opt_obj.action;
      if (account === currentAuthUsername) return defaultItem;

      switch (action_type) {
        case 0: { // pod_action_join_request
          // const {join_fee, fee, memo} = action;
          return (
            <Link
              to={`/@${account}`}
              onClick={onClick}
              className={classNames('Notification', {
                'Notification--unread': !read,
              })}
            >
              <Avatar username={account} size={32}/>
              <div className="Notification__text">
                <div className="Notification__text__message">
                  <span className="username">{account}</span> created a request to join your pod.
                </div>
                {notificationTimestamp}
              </div>
            </Link>
          );
        }
        // case 1: { // pod_action_cancel_join_request
        //
        // }
        case 2: { // pod_action_accept_join_request
          // const {account_accepted} = action;
          return (
            <Link
              to={`/@${account}`}
              onClick={onClick}
              className={classNames('Notification', {
                'Notification--unread': !read,
              })}
            >
              <Avatar username={account} size={32}/>
              <div className="Notification__text">
                <div className="Notification__text__message">
                  <span className="username">{account}</span> accepted your pod join request.
                </div>
                {notificationTimestamp}
              </div>
            </Link>
          );
        }
        // case 3: { // pod_action_reject_join_request
        //
        // }
        case 4: { // pod_action_leave
          return (
            <Link
              to={`/@${account}`}
              onClick={onClick}
              className={classNames('Notification', {
                'Notification--unread': !read,
              })}
            >
              <Avatar username={account} size={32}/>
              <div className="Notification__text">
                <div className="Notification__text__message">
                  <span className="username">{account}</span> left your pod.
                </div>
                {notificationTimestamp}
              </div>
            </Link>
          );
        }
        case 5: { // pod_action_kick
          return (
            <Link
              to={`/@${account}`}
              onClick={onClick}
              className={classNames('Notification', {
                'Notification--unread': !read,
              })}
            >
              <Avatar username={account} size={32}/>
              <div className="Notification__text">
                <div className="Notification__text__message">
                  <span className="username">{account}</span> kicked you out of pod.
                </div>
                {notificationTimestamp}
              </div>
            </Link>
          );
        }
        // case 6: { // pod_action_update
        //
        // }
        default:
          console.log(`opt_type=${opt_type}, action_type=${action_type}`);
          return defaultItem;
      }
    }
    case 'shutdown_witness': {
      const {owner} = opt_obj;
      return (<Link
        to={'###'}
        className={classNames('Notification', {
          'Notification--unread': !read,
        })}
        onClick={onClick}
      >
        <Avatar username={owner} size={40}/>
        <div className="Notification__text">
          <div className="Notification__text__message">
            Your witness has been shutdown (Failed to produce blocks in 24h hours).
          </div>
          {notificationTimestamp}
        </div>
      </Link>);
    }
    // case notificationConstants.REPLY:
    //   return (
    //     <NotificationReply
    //       notification={notification}
    //       currentAuthUsername={currentAuthUsername}
    //       read={read}
    //       onClick={this.handleNotificationsClick}
    //     />
    //   );
    // case notificationConstants.FOLLOW:
    //   return (
    //     <NotificationFollowing
    //       notification={opt}
    //       read={read}
    //       onClick={this.handleNotificationsClick}
    //     />
    //   );
    // case notificationConstants.MENTION:
    //   return (
    //     <NotificationMention
    //       notification={opt}
    //       read={read}
    //       onClick={this.handleNotificationsClick}
    //     />
    //   );
    // case notificationConstants.REBLOG:
    //   return (
    //     <NotificationReblog
    //       notification={opt}
    //       read={read}
    //       currentAuthUsername={currentAuthUsername}
    //       onClick={this.handleNotificationsClick}
    //     />
    //   );
    // case notificationConstants.TRANSFER:
    //   return (
    //     <NotificationTransfer
    //       notification={opt}
    //       read={read}
    //       onClick={this.handleNotificationsClick}
    //     />
    //   );
    // case notificationConstants.WITNESS_VOTE:
    //   return (
    //     <NotificationVoteWitness
    //       notification={opt}
    //       read={read}
    //       onClick={this.handleNotificationsClick}
    //     />
    //   );
    default:
      console.log(obj);
      return defaultItem;
  }
};

NotificationItem.propTypes = {
  read: PropTypes.bool.isRequired,
  obj: PropTypes.shape().isRequired,
  currentAuthUsername: PropTypes.string.isRequired,
  onClick: PropTypes.func,
};

NotificationItem.defaultProps = {
  read: false,
  currentAuthUsername: '',
  onClick: () => {},
};

export default NotificationItem;
