import { expect } from 'chai';
import settingsReducer, { getIsLoading, getLocale } from '../settingsReducer';
import * as settingsTypes from '../settingsActions';
import * as authTypes from '../../auth/authActions';

describe('settingsReducer', () => {
  const initialState = {
    locale: 'en-US',
    votePercent: 10000,
    showNSFWPosts: false,
    rewriteLinks: false,
    loading: false,
  };

  it('should return initial state', () => {
    const stateBefore = undefined;
    const stateAfter = initialState;
    const action = {};

    expect(settingsReducer(stateBefore, action)).to.eql(stateAfter);
  });

  it('should not change on unknown action', () => {
    const stateBefore = initialState;
    const stateAfter = stateBefore;
    const action = { type: 'UNKNOWN_ACTION_TYPE' };

    expect(settingsReducer(stateBefore, action)).to.eql(stateAfter);
  });

  it('should set loading to true when saving', () => {
    const stateBefore = initialState;
    const stateAfter = {
      ...stateBefore,
      loading: true,
    };
    const action = {
      type: settingsTypes.SAVE_SETTINGS_START,
    };

    expect(settingsReducer(stateBefore, action)).to.eql(stateAfter);
  });

  it('should set loading to false after saving failed', () => {
    const stateBefore = {
      ...initialState,
      loading: true,
    };
    const stateAfter = {
      ...stateBefore,
      loading: false,
    };
    const action = {
      type: settingsTypes.SAVE_SETTINGS_ERROR,
    };

    expect(settingsReducer(stateBefore, action)).to.eql(stateAfter);
  });

  it('should set locale, vote percent, loading, showNSFWPosts, and rewriteLinks after saving succeeded', () => {
    const stateBefore = {
      ...initialState,
      loading: true,
    };
    const stateAfter = {
      ...stateBefore,
      loading: false,
      locale: 'pl',
      votePercent: 10000,
      showNSFWPosts: true,
      rewriteLinks: true,
    };
    const action = {
      type: settingsTypes.SAVE_SETTINGS_SUCCESS,
      payload: {
        locale: 'pl',
        votePercent: 10000,
        showNSFWPosts: true,
        rewriteLinks: true,
      },
    };

    expect(settingsReducer(stateBefore, action)).to.eql(stateAfter);
  });

  it('should set locale after login success', () => {
    const stateBefore = initialState;
    const stateAfter = {
      ...stateBefore,
      locale: 'fr',
    };
    const action = {
      type: authTypes.LOGIN_SUCCESS,
      payload: {
        user_metadata: {
          settings: {
            locale: 'fr',
          },
        },
      },
    };

    expect(settingsReducer(stateBefore, action)).to.eql(stateAfter);
  });

  it('should set locale after login success', () => {
    const stateBefore = initialState;
    const stateAfter = {
      ...stateBefore,
      locale: 'fr',
    };
    const action = {
      type: authTypes.LOGIN_SUCCESS,
      payload: {
        user_metadata: {
          settings: {
            locale: 'fr',
          },
        },
      },
    };

    expect(settingsReducer(stateBefore, action)).to.eql(stateAfter);
  });

  it('should return previous state after login success without settings', () => {
    const stateBefore = {
      ...initialState,
      locale: 'fr',
    };
    const action = {
      type: authTypes.LOGIN_SUCCESS,
      payload: {
        user_metadata: {},
      },
    };

    expect(settingsReducer(stateBefore, action)).to.eql(stateBefore);
  });

  it('should set locale after reload success', () => {
    const stateBefore = initialState;
    const stateAfter = {
      ...stateBefore,
      locale: 'fr',
    };
    const action = {
      type: authTypes.RELOAD_SUCCESS,
      payload: {
        user_metadata: {
          settings: {
            locale: 'fr',
          },
        },
      },
    };

    expect(settingsReducer(stateBefore, action)).to.eql(stateAfter);
  });

  it('should set locale after reload success', () => {
    const stateBefore = initialState;
    const stateAfter = {
      ...stateBefore,
      locale: 'fr',
    };
    const action = {
      type: authTypes.RELOAD_SUCCESS,
      payload: {
        user_metadata: {
          settings: {
            locale: 'fr',
          },
        },
      },
    };

    expect(settingsReducer(stateBefore, action)).to.eql(stateAfter);
  });

  it('should return previous state after reload success without settings', () => {
    const stateBefore = {
      ...initialState,
      locale: 'fr',
    };
    const action = {
      type: authTypes.RELOAD_SUCCESS,
      payload: {
        user_metadata: {},
      },
    };

    expect(settingsReducer(stateBefore, action)).to.eql(stateBefore);
  });
});

describe('settingsReducer selectors', () => {
  const stateVar1 = {
    locale: 'auto',
    loading: false,
  };

  const stateVar2 = {
    locale: 'pl',
    loading: true,
  };

  it('should return locale', () => {
    expect(getLocale(stateVar1)).to.equal('auto');
    expect(getLocale(stateVar2)).to.equal('pl');
  });

  it('should return loading', () => {
    expect(getIsLoading(stateVar1)).to.equal(false);
    expect(getIsLoading(stateVar2)).to.equal(true);
  });
});
