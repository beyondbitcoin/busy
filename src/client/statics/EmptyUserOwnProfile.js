import React from 'react';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';

const EmptyUserProfile = (props) => (

  <div className="text-center">
    <h3>
      <FormattedMessage id="empty_user_own_profile" 
        defaultMessage="You didn't {type} any stories yet."
        values={{type:(props.type && props.type.length > 0) ? props.type : "publish"}}
      />
    </h3>

    {!props.type && (
      <>
        <div>
          Write a short post to introduce yourself. <br/>
          Use the 'introduceyourself' tag for this first post.<br/>
          Our supportive community members will welcome you.
        </div>
        <Link to="/editor">
          <FormattedMessage id="start_now" defaultMessage="Start now" />
        </Link>
      </>
    )}
    {props.type && (
      <div>
        Engagement is an important part of the Whaleshares project.<br/>
        Your Shares-tab, shows your level of engagement and can be<br/>
        a source of inspiration for those who visit your profile.<br/>
        Fill this tab by liking/rewarding posts.<br/>
        <br/>
      </div>
    )}
  </div>
);

export default EmptyUserProfile;
