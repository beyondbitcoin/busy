import React, { Component } from 'react';
import Icon from 'antd/lib/icon';
import '../../components/Sidebar/SidebarContentBlock.less';
import '../../components/Sidebar/SidebarBlock.less';

const SidebarBlock = (props) => {
	const { icon, title, content } = props;

    return (
      <div className='SidebarContentBlock'>

      	<h4 className='SidebarContentBlock__title'>
        	<Icon type={icon} className="iconfont SidebarContentBlock__icon" />{' '}
        	{title}
      	</h4>
      	<div className='SidebarContentBlock__content'>
      		{content}
      	</div>
      </div>
    );
};

export default SidebarBlock;