import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { getCurrentDisplayedActions, getCurrentFilteredActions, getAccountHistoryFilter } from '../reducers';

const UserActivitySearchMessage = ({ currentDisplayedActions, currentFilteredActions, accountHistoryFilter }) => {
  const displayedActions = _.isEmpty(accountHistoryFilter) ? currentDisplayedActions : currentFilteredActions;
  if (_.isEmpty(displayedActions)) {
    return (
      <div className="UserActivityActions__search__container">
        <FormattedMessage id="no_results_found_for_search" defaultMessage="No results were found for your filters." />
      </div>
    );
  }
  return null;
};

UserActivitySearchMessage.propTypes = {
  currentDisplayedActions: PropTypes.arrayOf(PropTypes.shape()),
  currentFilteredActions: PropTypes.arrayOf(PropTypes.shape()),
  accountHistoryFilter: PropTypes.arrayOf(PropTypes.string),
};

UserActivitySearchMessage.defaultProps = {
  currentDisplayedActions: [],
  currentFilteredActions: [],
  accountHistoryFilter: [],
};

export default connect(state => ({
  currentDisplayedActions: getCurrentDisplayedActions(state),
  currentFilteredActions: getCurrentFilteredActions(state),
  accountHistoryFilter: getAccountHistoryFilter(state),
}))(UserActivitySearchMessage);
