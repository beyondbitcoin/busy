import React from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import { FormattedMessage } from 'react-intl';
import { Icon, Input } from 'antd';
import Dropzone from 'react-dropzone';
import { HotKeys } from 'react-hotkeys';
import 'emoji-mart/css/emoji-mart.css';
import Picker from '../../helpers/emojiHelpers/Picker';
import { MAXIMUM_UPLOAD_SIZE, isValidImage } from '../../helpers/image';
import EditorToolbar from './EditorToolbar';
import './EditorInput.less';

class EditorInput extends React.Component {
  static propTypes = {
    addon           : PropTypes.node,
    inputId         : PropTypes.string,
    inputRef        : PropTypes.func,
    minRows         : PropTypes.number,
    onChange        : PropTypes.func,
    onImageInvalid  : PropTypes.func,
    onImageUpload   : PropTypes.func,
    value           : PropTypes.string, // eslint-disable-line react/require-default-props
  };

  static defaultProps = {
    addon           : null,
    inputId         : '',
    inputRef        : () => {},
    minRows         : 3,
    onChange        : () => {},
    onImageInvalid  : () => {},
    onImageUpload   : () => {},
  };

  static hotkeys = {
    h1: 'ctrl+shift+1',
    h2: 'ctrl+shift+2',
    h3: 'ctrl+shift+3',
    h4: 'ctrl+shift+4',
    h5: 'ctrl+shift+5',
    h6: 'ctrl+shift+6',
    bold: 'ctrl+b',
    italic: 'ctrl+i',
    quote: 'ctrl+q',
    link: 'ctrl+k',
    image: 'ctrl+m',
  };

  constructor(props) {
    super(props);

    this.state = {
      imageUploading: false,
      dropzoneActive: false,
      emojiOpen: false,
      isFocused: false,
      currentValue: props.value,
    };

    this.setInput = this.setInput.bind(this);
    this.disableAndInsertImage = this.disableAndInsertImage.bind(this);
    this.insertCode = this.insertCode.bind(this);
    this.handlePastedImage = this.handlePastedImage.bind(this);
    this.handleImageChange = this.handleImageChange.bind(this);
    this.handleDrop = this.handleDrop.bind(this);
    this.handleDragEnter = this.handleDragEnter.bind(this);
    this.handleDragLeave = this.handleDragLeave.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    if (this.input) {
      this.input.addEventListener('paste', this.handlePastedImage);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!this.state.isFocused) {
      this.setState({ currentValue: nextProps.value });
    }
  }

  onEmoji = () => {
    this.setState({
      emojiOpen: !this.state.emojiOpen,
    });
  };

  setInput(input) {
    if (input) {
      this.originalInput = input;
      // eslint-disable-next-line react/no-find-dom-node
      this.input = ReactDOM.findDOMNode(input);
      this.props.inputRef(this.input);
    }
  }

  setValue(value, start, end) {
    this.setState({ currentValue: value });
    this.props.onChange(value);
    if (start && end) {
      setTimeout(() => {
        this.input.setSelectionRange(start, end);
      }, 0);
    }
  }

  insertAtCursor(before, after, deltaStart = 0, deltaEnd = 0) {
    if (!this.input) return;

    const { value } = this.props;

    const startPos = this.input.selectionStart;
    const endPos = this.input.selectionEnd;
    const newValue =
      value.substring(0, startPos) +
      before +
      value.substring(startPos, endPos) +
      after +
      value.substring(endPos, value.length);

    this.setValue(newValue, startPos + deltaStart, endPos + deltaEnd);
  }

  disableAndInsertImage(image, imageName = 'image') {
    this.setState({
      imageUploading: false,
    });
    this.insertImage(image, imageName);
  }

  insertImage(image, imageName = 'image') {
    if (!this.input) return;

    const { value } = this.props;

    const startPos = this.input.selectionStart;
    const endPos = this.input.selectionEnd;
    const imageText = `![${imageName}](${image})\n`;
    const newValue = `${value.substring(0, startPos)}${imageText}${value.substring(endPos, value.length)}`;
    this.setValue(newValue, startPos + imageText.length, startPos + imageText.length);
  }

  insertCode(type) {
    if (!this.input) return;
    this.input.focus();
    
    const htmlTags = {
      h1: ['# ', '', 2],
      h2: ['## ', '', 3],
      h3: ['### ', '', 4],
      h4: ['#### ', '', 5],
      h5: ['##### ', '', 6],
      h6: ['###### ', '', 7],
      b: ['**', '**', 2],
      i: ['*', '*', 1],
      q: ['> ', '', 2],
      link: ['[', '](url)', 1],
      image: ['![', '](url)', 2],
    };

    if ( htmlTags[type] ) {
        this.insertAtCursor(
          htmlTags[type][0],
          htmlTags[type][1],
          htmlTags[type][2],
          htmlTags[type][2]);  
    }
  }

  handlers = {
    h1: () => this.insertCode('h1'),
    h2: () => this.insertCode('h2'),
    h3: () => this.insertCode('h3'),
    h4: () => this.insertCode('h4'),
    h5: () => this.insertCode('h5'),
    h6: () => this.insertCode('h6'),
    bold: () => this.insertCode('b'),
    italic: () => this.insertCode('i'),
    quote: () => this.insertCode('q'),
    link: e => {
      e.preventDefault();
      this.insertCode('link');
    },
    image: () => this.insertCode('image'),
  };

  handlePastedImage(e) {
    if (e.clipboardData && e.clipboardData.items) {
      const items = e.clipboardData.items;
      Array.from(items).forEach(item => {
        if (item.kind === 'file') {
          e.preventDefault();

          const blob = item.getAsFile();

          if (!isValidImage(blob)) {
            this.props.onImageInvalid();
            return;
          }

          this.setState({
            imageUploading: true,
          });

          this.props.onImageUpload(blob, this.disableAndInsertImage, () =>
            this.setState({
              imageUploading: false,
            }),
          );
        }
      });
    }
  }

  handleImageChange(e) {
    if (e.target.files && e.target.files[0]) {
      if (!isValidImage(e.target.files[0])) {
        this.props.onImageInvalid();
        return;
      }

      this.setState({
        imageUploading: true,
      });

      this.props.onImageUpload(e.target.files[0], this.disableAndInsertImage, () =>
        this.setState({
          imageUploading: false,
        }),
      );
      e.target.value = '';
    }
  }

  handleDrop(files) {
    if (files.length === 0) {
      this.setState({
        dropzoneActive: false,
      });
      return;
    }

    this.setState({
      dropzoneActive: false,
      imageUploading: true,
    });
    let callbacksCount = 0;
    Array.from(files).forEach(item => {
      this.props.onImageUpload(
        item,
        (image, imageName) => {
          callbacksCount += 1;
          this.insertImage(image, imageName);
          if (callbacksCount === files.length) {
            this.setState({
              imageUploading: false,
            });
          }
        },
        () => {
          this.setState({
            imageUploading: false,
          });
        },
      );
    });
  }

  handleDragEnter() {
    this.setState({ dropzoneActive: true });
  }

  handleDragLeave() {
    this.setState({ dropzoneActive: false });
  }

  handleChange(e) {
    e.persist();
    this.setValue(e.target.value);
  }

  handleFocus = () => {
    this.setState({ isFocused: true });
  };

  handleBlur = () => {
    this.setState({ isFocused: false });
  };

  render() {
    const { addon, inputId, inputRef, minRows, onImageUpload, onImageInvalid, ...restProps } = this.props;
    const { dropzoneActive, emojiOpen, currentValue } = this.state;

    const picker = (
      <Picker
        onClick={emoji => {
          this.insertAtCursor(emoji.colons, ' ');
          this.onEmoji();
        }}
        title="Pick your emoji…"
        emoji="point_up"
        style={{
          width: '100%',
          position: 'relative',
          borderRadius: 0,
          borderBottom: 0,
        }}
      />
    );

    return (
      <React.Fragment>
        <EditorToolbar onSelect={this.insertCode} onEmoji={this.onEmoji} />
        {emojiOpen && picker}
        <div className="EditorInput__dropzone-base">
          <Dropzone
            disableClick
            style={{}}
            accept="image/*"
            maxSize={MAXIMUM_UPLOAD_SIZE}
            onDropRejected={this.props.onImageInvalid}
            onDrop={this.handleDrop}
            onDragEnter={this.handleDragEnter}
            onDragLeave={this.handleDragLeave}
          >
            {dropzoneActive && (
              <div className="EditorInput__dropzone">
                <div>
                  <i className="iconfont icon-picture" />
                  <FormattedMessage id="drop_image" defaultMessage="Drop your images here" />
                </div>
              </div>
            )}
            <HotKeys keyMap={this.constructor.hotkeys} handlers={this.handlers}>
              <Input.TextArea
                {...restProps}
                autosize  = {{minRows:minRows}}
                onBlur    = {this.handleBlur}
                onFocus   = {this.handleFocus}
                onChange  = {this.handleChange}
                value     = {currentValue}
                ref       = {this.setInput}
              />
            </HotKeys>
          </Dropzone>
        </div>
        <p className="EditorInput__imagebox">
          <input
            type="file"
            id={this.props.inputId || 'inputfile'}
            accept="image/*;capture=camera"
            onChange={this.handleImageChange}
          />
          <label htmlFor={this.props.inputId || 'inputfile'}>
            {this.state.imageUploading ? <Icon type="loading" /> : <i className="iconfont icon-picture" />}
            {this.state.imageUploading ? (
              <FormattedMessage id="image_uploading" defaultMessage="Uploading your image..." />
            ) : (
              <FormattedMessage
                id="select_or_past_image"
                defaultMessage="Select image or paste it from the clipboard (max size 4 mb)"
              />
            )}
          </label>
          <label htmlFor="reading_time" className="EditorInput__addon">
            {addon}
          </label>
        </p>
      </React.Fragment>
    );
  }
}

export default EditorInput;
