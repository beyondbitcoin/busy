/* eslint-disable global-require */
require('dotenv').config();
const MiniCssExtractPlugin = require("mini-css-extract-plugin"); // replace extract-text-webpack-plugin
const LodashModuleReplacementPlugin = require('lodash-webpack-plugin');
const Visualizer = require('webpack-visualizer-plugin');
const _ = require('lodash');
const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin');
const configUtils = require('./configUtils');

const isDevelopment = process.env.NODE_ENV !== 'production';
const baseDir = path.resolve(__dirname, '..');

module.exports = {
  mode: process.env.NODE_ENV || 'production',
  devtool: isDevelopment ? 'eval-source-map' : '',
  entry: {
    main: (isDevelopment
        ? [
          'webpack-hot-middleware/client?reload=true',
          'react-hot-loader/patch',
          // activate HMR for React
          'webpack/hot/only-dev-server',
          // bundle the client for hot reloading
          // only- means to only hot reload for successful updates
        ]
        : []
    ).concat([path.join(baseDir, 'src/client/index.js')]),
  },
  output: {
    path: path.join(baseDir, '/public/js'),
    filename: isDevelopment ? 'bundle.js' : 'busyapp-[name].[chunkhash].js',
    publicPath: '/js/',
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        // This has effect on the react lib size
        NODE_ENV: isDevelopment ? JSON.stringify('development') : JSON.stringify('production'),
        ENABLE_LOGGER: JSON.stringify(process.env.ENABLE_LOGGER),
        WLS_RPC_URL: JSON.stringify(process.env.WLS_RPC_CLIENT_URL || 'https://pubrpc.whaleshares.io'),
        WALLET_URL: JSON.stringify(process.env.WALLET_URL || 'https://wallet.whaleshares.io'),
        EXPLORER_URL: JSON.stringify(process.env.EXPLORER_URL || 'https://explorer.whaleshares.io'),
        IS_BROWSER: JSON.stringify(true),
      },
    }),
    new LodashModuleReplacementPlugin({
      collections: true,
      paths: true,
      shorthands: true,
      flattening: true,
    }),
    new Visualizer({
      filename: './statistics.html',
    }),
    new SWPrecacheWebpackPlugin({
      filepath: path.resolve(baseDir, 'public/service-worker.js'),
      stripPrefix: path.resolve(baseDir, 'public'),
    }),
    new CleanWebpackPlugin({
      cleanOnceBeforeBuildPatterns: [ path.join(baseDir, '/public') ],
      dangerouslyAllowCleanPatternsOutsideProject: false
    }),
    new CopyWebpackPlugin([
      {
        from: path.join(baseDir, '/assets'),
        to: path.join(baseDir, '/public'),
      },
    ]),
    new webpack.optimize.ModuleConcatenationPlugin(),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    new webpack.optimize.AggressiveMergingPlugin(),
    new MiniCssExtractPlugin({
      filename: '../css/style.[contenthash].css',
    }),
    new HtmlWebpackPlugin({
      title: 'Busy',
      filename: '../index.html',
      template: path.join(baseDir, '/templates/production_index.html'),
    }),
  ],
  module: {
    rules: [
      {
        test: configUtils.MATCH_JS_JSX,
        exclude: /node_modules/,
        use: (isDevelopment ? [{ loader: 'react-hot-loader/webpack' }] : []).concat([
          {
            loader: 'babel-loader',
          },
        ]),
      },
      {
        test: /\.(eot|ttf|woff|woff2)(\?.+)?$/,
        loader: 'url-loader',
        options: {
          name: '../fonts/[name].[ext]',
          // load fonts through data-url in development
          limit: isDevelopment ? 5000000 : 1,
        },
      },
      {
        test: /\.png$/,
        loader: 'file-loader',
      },
      {
        test: /\.html$/,
        loader: 'html-loader',
        options: {
          removeComments: false,
        },
      },
      {
        test: configUtils.MATCH_CSS_LESS,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              ident: 'postcss', // https://webpack.js.org/guides/migrating/#complex-options
              plugins: () => [
                require('autoprefixer')({
                  browsers: [
                    '>1%',
                    'last 4 versions',
                    'Firefox ESR',
                    'not ie < 9', // React doesn't support IE8 anyway
                  ],
                }),
              ],
            },
          },
          {
            loader: 'less-loader',
            options: {
              javascriptEnabled: true
            },
          },
        ]
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack'],
      }
    ],
  },
  optimization: {
    splitChunks: {
      chunks: 'initial',
      minSize: 30000,
      minChunks: 1,
      maxAsyncRequests: 5,
      maxInitialRequests: 3,
      cacheGroups: {
        vendor: {
          name: 'vendor',
          test: /[\\/]node_modules[\\/]/,
        },
        main: {
          name: 'main',
          minChunks: 2,
          reuseExistingChunk: true,
          enforce: true,
        },
      },
    },
    runtimeChunk: {
      name: 'manifest',
    },
  }
};
