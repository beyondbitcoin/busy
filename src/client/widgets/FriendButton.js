import PropTypes from "prop-types";
import React from 'react';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { compose } from 'redux';
import { Button } from 'antd';
import { getAuthenticatedUserName } from "../reducers";
import {
  fetchFriends,
  fetchPendingSentRequests,
  fetchPendingReceivedRequests,
  sendFriendRequest,
  unfriend,
} from '../friends/actions';

class FriendButton extends React.Component {
  static propTypes = {
    username: PropTypes.string.isRequired,
    authenticatedUserName: PropTypes.string,
    intl: PropTypes.shape().isRequired,
  };

  static defaultProps = {
    username: undefined,
    authenticatedUserName: undefined,
  };

  componentDidMount() {
    if( this.props.friends === false) this.props.fetchFriends();
    if( this.props.pendingSentRequests === false) this.props.fetchPendingSentRequests();
    if( this.props.pendingReceivedRequests === false) this.props.fetchPendingReceivedRequests();
  }

  handleSendFriendRequest = async () => {
    await this.props.sendFriendRequest(this.props.username);
    await this.props.fetchPendingSentRequests();
  };

  handleUnfriend = async () => {
    await this.props.unfriend(this.props.username);
    await this.props.fetchFriends();
  };

  render() {
    const {
      username, authenticatedUserName, friends, pendingReceivedRequests, pendingSentRequests,
      submitting
    } = this.props;
    if ((typeof username === `undefined`) || (typeof authenticatedUserName === `undefined`) ||
      (authenticatedUserName === username)) return null;
    if (submitting) return (<span>Friend (submitting...)</span>);

    const isFriend = friends && friends.includes(username);
    const isPendingSent = pendingReceivedRequests && pendingReceivedRequests.filter(r => {
      return r.account === username;
    }).length === 1;
    const isPendingReceived = pendingSentRequests && pendingSentRequests.filter(r => {
      return r.another === username;
    }).length === 1;

    /**
     * friendState value:
     *   + 0: not friend
     *   + 1: friend
     *   + 2: pending
     */
    const friendState = isFriend ? 1 : ((isPendingSent || isPendingReceived) ? 2 : 0);
    let result = null;
    switch (friendState) {
      case 0: // not friend
        result = (
          <Button type="dashed" size="small" onClick={this.handleSendFriendRequest}>
            {this.props.intl.formatMessage({id: 'add_friend', defaultMessage: 'Add Friend'})}
          </Button>
        );
        break;
      case 1: // friend
        result = (
          <Button type="dashed" size="small" onClick={this.handleUnfriend}>
            {this.props.intl.formatMessage({id: 'unfriend', defaultMessage: 'Unfriend'})}
          </Button>
        );
        break;
      case 2: // pending
        result = (<Button type="dashed" size="small" disabled>Pending Friend</Button>);
        break;
      default:
        break;
    }

    return result;
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    ...ownProps,
    ...state.friends,
    authenticatedUserName: getAuthenticatedUserName(state),
  };
};
const mapDispatchToProps = {
  fetchFriends,
  fetchPendingSentRequests,
  fetchPendingReceivedRequests,
  sendFriendRequest,
  unfriend
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);
export default compose(
  injectIntl,
  withConnect
)(FriendButton);
