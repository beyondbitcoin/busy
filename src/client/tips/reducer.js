import produce from 'immer';
import * as actions from './actions';

const initialState = {
  tips_sent: false, // is array, is false if not loaded
  tips_received: false, // is array, is false if not loaded
  tips_sent_hasMore: false,
  tips_received_hasMore: false,
  loading: false,
};

const reducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case actions.SET:
        draft[action.payload.key] = action.payload.value;
        break;
      default:
        break;
    }
  });

export default reducer;

