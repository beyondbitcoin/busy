import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from "react-router-dom";
import Alert from 'antd/lib/alert';
import Feed from '../feed/Feed';
import { getFeed, getUser } from '../reducers';
import {
  getFeedLoadingFromState,
  getFeedHasMoreFromState,
  getFeedFromState,
  getFeedFetchedFromState
} from '../helpers/stateHelpers';
import { getFeedContent, getMoreFeedContent } from '../feed/feedActions';
import { hidePostModal, showPostModal } from "../app/appActions";
import EmptyUserProfile from '../statics/EmptyUserProfile';
import PostModal from '../post/PostModalContainer';

@withRouter
@connect(
  (state, ownProps) => ({
    feed: getFeed(state),
    pod: getUser(state, ownProps.match.params.name),
  }),
  {
    showPostModal,
    hidePostModal,
    getFeedContent,
    getMoreFeedContent,
  },
)
class PodNews extends React.Component {
  static propTypes = {
    feed: PropTypes.shape(),
    match: PropTypes.shape(),
    limit: PropTypes.number,
    showPostModal: PropTypes.func.isRequired,
    hidePostModal: PropTypes.func.isRequired,
  };

  static defaultProps = {
    limit: 10,
    location: {},
  };

  componentDidMount() {
    const {feed, match, limit} = this.props;
    const sortBy = 'posts';  //'news'
    const podname = match.params.name;
    const content = getFeedFromState(sortBy, podname, feed);

    if (_.isEmpty(content)) this.props.getFeedContent({sortBy, category: podname, limit});
  }

  componentDidUpdate(nextProps) {
    if (typeof window === 'undefined') return;
    const {feed, match, limit} = nextProps;
    const podname = nextProps.match.params.name;
    const content = getFeedFromState(this.sortBy, podname, feed);
    const sortBy = 'posts';  //'news'

    if (_.isEmpty(content) && (match.url !== this.props.match.url)) {
      if (window) window.scrollTo(0, 0);

      this.props.getFeedContent({sortBy, category: podname, limit});
      this.props.hidePostModal();
    }
  }

  render() {
    const {feed, match} = this.props;
    const podname = match.params.name;
    const sortBy = 'posts';  //'news'
    const content = getFeedFromState(sortBy, podname, feed);
    const isFetching = getFeedLoadingFromState(sortBy, podname, feed);
    const fetched = getFeedFetchedFromState(sortBy, podname, feed);
    const hasMore = getFeedHasMoreFromState(sortBy, podname, feed);
    const loadMoreContentAction = () =>
      this.props.getMoreFeedContent({
        sortBy,
        category: podname,
        limit: this.props.limit,
      });

    return (
      <div>
        <div style={{paddingBottom: '10px'}}>
          <Alert
            message={
              <span>
              Public posts by the pod owner will be listed here.
            </span>
            }
            type="info"
            showIcon
            style={{width: '100%'}}
          />
        </div>
        <div className="profile">
          <Feed
            content={content}
            isFetching={isFetching}
            hasMore={hasMore}
            loadMoreContent={loadMoreContentAction}
            showPostModal={this.props.showPostModal}
            feedType='podnews'
          />
        </div>
        {_.isEmpty(content) && fetched && <EmptyUserProfile user={podname}/>}
        {<PostModal/>}
      </div>
    );
  }
}

export default PodNews;
