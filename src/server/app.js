/* eslint-disable no-console */
import { compileAmpTemplate } from './renderers/ampRenderer';
import createSsrHandler from './handlers/createSsrHandler';
import createAmpHandler from './handlers/createAmpHandler';

const fs = require('fs');
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const http = require('http');
const https = require('https');

const helmet = require('helmet');

http.globalAgent.maxSockets = Infinity;
https.globalAgent.maxSockets = Infinity;

const ONE_WEEK = 1000 * 60 * 60 * 24 * 7;
// const EIGHT_HOURS = 1000 * 60 * 60 * 8;

const app = express();
const server = http.Server(app);

const rootDir = path.join(__dirname, '../..');

app.locals.env = process.env;
// app.enable('trust proxy');

app.use((req, res, next) => {
  if (req.url.match(/.(png|jpeg|woff2|woff|jpg)/gi)) res.setHeader('Cache-Control', `public, max-age=${ONE_WEEK}`);
  // else if (req.url.match(/busyapp-.*\.js/gi)) res.setHeader('Cache-Control', `public, max-age=${EIGHT_HOURS}`);

  next();
});

app.use(cookieParser());
app.use(
  bodyParser.json({
    type: ['json', 'application/csp-report'],
  }),
);
app.use(bodyParser.urlencoded({ extended: false }));

app.use(helmet());
app.use(
  helmet.contentSecurityPolicy({
    // Specify directives as normal.
    directives: {
      childSrc: [
        "'self'",
        'www.youtube.com',
        'staticxx.facebook.com',
        'w.soundcloud.com',
        'player.vimeo.com',
        'player.twitch.tv',
        'www.tiktok.com',
        'musicoin.org',
        'www.instagram.com',
        'www.bitchute.com',
        'emb.d.tube',
        'rumble.com',
        'www.brighteon.com',
        'lbry.tv',
        'media.gab.com',
        'tv.gab.com',
        'video.twimg.com',
        'threespeakvideo.b-cdn.net',
        'ad.a-ads.com',
      ],
      connectSrc: [
        "'self'",
        'whaleshares.io',
        'pubrpc.whaleshares.io',
        'api.whaleshares.io',
        '*.google-analytics.com',
        '*.googletagmanager.com'
      ],
      defaultSrc: [
        "'self'",
        'whaleshares.io',
        'www.youtube.com',
        'staticxx.facebook.com',
        'player.vimeo.com',
        'player.twitch.tv',
        'www.tiktok.com',
        'musicoin.org',
        'www.instagram.com',
        'www.bitchute.com',
        'emb.d.tube',
        'rumble.com',
        'www.brighteon.com',
        'lbry.tv',
        'media.gab.com',
        'tv.gab.com',
        'video.twimg.com',
        'threespeakvideo.b-cdn.net',
        'ad.a-ads.com',
      ],
      scriptSrc: [
        "'self'",
        "'unsafe-eval'",
        "'unsafe-inline'",
        'whaleshares.io',
        '*.google-analytics.com',
        '*.googletagmanager.com',
        'connect.facebook.net',
        'cdn.polyfill.io',
        'cse.google.com',
        'www.google.com',
        'cdn.ampproject.org',
      ],
      styleSrc: [
        "'self'",
        "'unsafe-inline'",
        'whaleshares.io',
        'fonts.googleapis.com',
        'at.alicdn.com',
        'www.google.com',
      ],
      fontSrc: ["'self'", 'data:', 'fonts.gstatic.com', 'fonts.googleapis.com', 'at.alicdn.com'],
      frameAncestors: ["'none'"],
      imgSrc: ['*', 'data:'],
      // sandbox: ['allow-forms', 'allow-scripts'],
      reportUri: '/csp_violation',
      objectSrc: ["'none'"],
      // upgradeInsecureRequests: true,
      // workerSrc: false  // This is not set.
    },

    // This module will detect common mistakes in your directives and throw errors
    // if it finds any. To disable this, enable "loose mode".
    loose: false,

    // Set to true if you only want browsers to report errors, not block them.
    // You may also set this to a function(req, res) in order to decide dynamically
    // whether to use reportOnly mode, e.g., to allow for a dynamic kill switch.
    reportOnly: false,

    // Set to true if you want to blindly set all headers: Content-Security-Policy,
    // X-WebKit-CSP, and X-Content-Security-Policy.
    setAllHeaders: true,

    // Set to true if you want to disable CSP on Android where it can be buggy.
    disableAndroid: false,

    // Set to false if you want to completely disable any user-agent sniffing.
    // This may make the headers less compatible but it will be much faster.
    // This defaults to `true`.
    browserSniff: true,
  }),
);
app.use(helmet.frameguard({ action: 'sameorigin' }));

app.use(express.static(path.join(rootDir, 'public'), { index: false }));

const indexPath = `${rootDir}/public/index.html`;
const indexHtml = fs.readFileSync(indexPath, 'utf-8');

const ampIndexPath = `${rootDir}/templates/amp_index.hbs`;
const ampIndexHtml = fs.readFileSync(ampIndexPath, 'utf-8');
const ampTemplate = compileAmpTemplate(ampIndexHtml);

const ssrHandler = createSsrHandler(indexHtml);
const ampHandler = createAmpHandler(ampTemplate);

app.get('/robots.txt',  (req, res) => {
  res.type('text/plain');
  res.send("User-agent: *\nAllow: /");
});

app.get('/@:author/:permlink/amp', ampHandler);
app.get('/:category/@:author/:permlink/amp', (req, res) => {
  const { author, permlink } = req.params;
  res.redirect(301, `/@${author}/${permlink}/amp`);
});
app.get('/:category/@:author/:permlink', (req, res) => {
  const { author, permlink } = req.params;
  res.redirect(301, `/@${author}/${permlink}`);
});

// force CSR (no SSR) these paths (https://gitlab.com/beyondbitcoin/busy/issues/17)
app.get(/^\/(myfeed|bookmarks|drafts|replies|activity|tags|rewards|editor|settings|notifications)/, (req, res) => {
  res.send(indexHtml);
});

app.get('/*', ssrHandler);

app.post('/csp_violation', (req, res) => {
  if (req.body) {
    console.log('CSP Violation: ', req.body);
  } else {
    console.log('CSP Violation: No data received!');
  }
  res.status(204).end();
});

export const appExp = app;
export const serverExp = server;
