/* eslint-disable react/no-danger */
import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { Emoji } from 'emoji-mart';
import customEmojis from './CustomImagesObject.json';

const emojiNameRegex = /:([a-zA-Z0-9_\-+]+):/g;

export default string => {
  if (typeof window === 'undefined') {
    return string;
  }

  return string
    .split(emojiNameRegex)
    .map((emojiInstance, i) => {
      if (i % 2 === 0) return emojiInstance;
      try {
        if (emojiInstance.startsWith('wls')) {
          return ReactDOMServer.renderToString(
            <span
              dangerouslySetInnerHTML={{
                __html: Emoji({
                  html: true,
                  emoji: customEmojis[emojiInstance],
                  size: 24,
                }),
              }}
            />,
          );
        }
        return ReactDOMServer.renderToString(
          <span
            dangerouslySetInnerHTML={{
              __html: Emoji({
                html: true,
                emoji: emojiInstance,
                size: 24,
                set: 'twitter',
              }),
            }}
          />,
        );
      } catch (e) {
        return '';
      }
    })
    .join('');
};

/* eslint-enable react/no-danger */
