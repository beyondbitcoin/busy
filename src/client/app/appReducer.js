import _ from 'lodash';
import { LOCATION_CHANGE } from 'react-router-redux/reducer';
import * as appTypes from './appActions';
import * as authActions from '../auth/authActions';
import { getCryptoPriceIncreaseDetails } from '../helpers/cryptosHelper';

const initialState = {
  isFetching: false,
  isLoaded: false,
  rate: 0,
  bannerClosed: false,
  appUrl: 'https://whaleshares.io',
  usedLocale: 'en',
  cryptosPriceHistory: {},
  showPostModal: false,
  currentShownPost: {},
  pods: {
    loading: false,
    hasMore: false,
    list: [],
  },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case authActions.LOGIN_SUCCESS:
      if (action.meta && action.meta.refresh) return state;
      return {
        ...state,
        locale:
          (action.payload.user_metadata && action.payload.user_metadata.locale) ||
          initialState.locale,
      };
    case appTypes.GET_PODS_BY_MEMBERS.ERROR:
      return {
        ...state,
        pods: {
          ...state.pods,
          loading: false,
          hasMore: false,
        },
      };
    case appTypes.GET_PODS_BY_MEMBERS.START:
      return {
        ...state,
        pods: {
          ...state.pods,
          loading: true,
        },
      };
    case appTypes.GET_PODS_BY_MEMBERS.SUCCESS:
      return {
        ...state,
        pods: {
          ...state.pods,
          loading: false,
          hasMore: action.meta.limit - action.payload.length < 1,
          list: [...state.pods.list, ...action.payload],
        },
      };
    case appTypes.CLOSE_BANNER:
      return {
        ...state,
        bannerClosed: true,
      };
    case appTypes.SET_APP_URL:
      return {
        ...state,
        appUrl: action.payload,
      };
    case appTypes.SET_USED_LOCALE:
      return {
        ...state,
        usedLocale: action.payload,
      };
    case appTypes.REFRESH_CRYPTO_PRICE_HISTORY:
      return {
        ...state,
        cryptosPriceHistory: {
          ...state.cryptosPriceHistory,
          [action.payload]: null,
        },
      };
    case appTypes.GET_CRYPTO_PRICE_HISTORY.SUCCESS: {
      const { symbol, usdPriceHistory, btcPriceHistory } = action.payload;
      const usdPriceHistoryByClose = _.map(usdPriceHistory.Data, data => data.close);
      const btcPriceHistoryByClose = _.map(btcPriceHistory.Data, data => data.close);
      const priceDetails = getCryptoPriceIncreaseDetails(
        usdPriceHistoryByClose,
        btcPriceHistoryByClose,
      );
      const btcAPIError = btcPriceHistory.Response === 'Error';
      const usdAPIError = usdPriceHistory.Response === 'Error';

      return {
        ...state,
        cryptosPriceHistory: {
          ...state.cryptosPriceHistory,
          [symbol]: {
            usdPriceHistory: usdPriceHistoryByClose,
            priceDetails,
            btcAPIError,
            usdAPIError,
          },
        },
      };
    }
    case appTypes.SHOW_POST_MODAL:
      return {
        ...state,
        showPostModal: true,
        currentShownPost: action.payload,
      };
    case LOCATION_CHANGE:
    case appTypes.HIDE_POST_MODAL:
      return {
        ...state,
        showPostModal: false,
      };
    default:
      return state;
  }
};

export const getIsFetching = state => state.isFetching;
export const getIsBannerClosed = state => state.bannerClosed;
export const getAppUrl = state => state.appUrl;
export const getUsedLocale = state => state.usedLocale;
export const getShowPostModal = state => state.showPostModal;
export const getCurrentShownPost = state => state.currentShownPost;
