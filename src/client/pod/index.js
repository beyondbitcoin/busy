import isArray from 'lodash/isArray';
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { renderRoutes } from 'react-router-config';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import { Tag, Icon } from 'antd';
import { injectIntl, FormattedMessage } from 'react-intl';
import faMapMarkerAlt from '../../../assets/images/svgs/faMapMarkerAlt.svg';
import faLink from '../../../assets/images/svgs/faLink.svg';
import PodHero from './PodHero';
import PodMenuWrapper from './PodMenuWrapper';
import {
  getIsAuthenticated,
  getAuthenticatedUser,
  getUser,
  getIsUserFailed,
  getIsUserLoaded,
} from '../reducers';
import { getAvatarURL } from '../components/Avatar';
import SidenavMain from '../components/Navigation/SidenavMain';
import RightSidebar from '../app/Sidebar/RightSidebar';
import Affix from '../components/Utils/Affix';
import ScrollToTop from "../components/Utils/ScrollToTop";
import ScrollToTopOnMount from '../components/Utils/ScrollToTopOnMount';
import SocialLinks from '../components/SocialLinks';
import PodAvatarLightbox from './PodAvatarLightbox';
import { urlWithoutWWW } from '../helpers/regexHelpers';
import ErrorFetching from '../statics/ErrorFetching';
import ErrorUserNotRegistered from '../statics/ErrorUserNotRegistered';
import PodButtons from '../widgets/PodButtons';
import Promo from '../components/Promo/Promo';
import './Pod.less';

@injectIntl
@connect(
  (state, ownProps) => ({
    authenticated: getIsAuthenticated(state),
    authenticatedUser: getAuthenticatedUser(state),
    user: getUser(state, ownProps.match.params.name),
    loaded: getIsUserLoaded(state, ownProps.match.params.name),
    failed: getIsUserFailed(state, ownProps.match.params.name),
  }),
)
class Pod extends React.Component {
  static propTypes = {
    route: PropTypes.shape().isRequired,
    authenticated: PropTypes.bool.isRequired,
    authenticatedUser: PropTypes.shape().isRequired,
    intl: PropTypes.shape().isRequired,
    match: PropTypes.shape().isRequired,
    user: PropTypes.shape().isRequired,
    loaded: PropTypes.bool,
    failed: PropTypes.bool,
    fetching: PropTypes.bool,
  };

  static defaultProps = {
    authenticatedUserName: '',
    loaded: false,
    failed: false,
    fetching: false,
  };

  state = {
    popoverVisible: false,
    fail_count: 0,
  };

  handleVisibleChange = visible => {
    this.setState({ popoverVisible: visible });
  };

  pageLoadFailed = count => {
    const counted = this.state.fail_count + count;
    if (counted < 3) {
      this.setState({ fail_count: counted });
      return null;
    }
    return <ErrorFetching />;
  };

  numberWithCommas = x => x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');

  render() {
    const { authenticated, authenticatedUser, loaded, failed, match, fetching, user } = this.props;
    this.pageLoadFailed = this.pageLoadFailed.bind(this);
    const username = match.params.name;

    if (failed && !fetching) {
      const pageLoaded = this.pageLoadFailed(1);
      if (pageLoaded !== null) {
        if (user.created === undefined) {
          return <ErrorUserNotRegistered />;
        }
        return pageLoaded;
      }
    }

    const profile           = user.json_metadata || {};
    const socialTags        = profile.profile;
    const busyHost          = global.postOrigin || 'https://whaleshares.io';
    const busyName          = 'whaleshares.io';
    const desc              = profile.about || `Posts by ${username}`;
    const image             = getAvatarURL(username) || '/images/logo.png';
    const url               = `${busyHost}/@${username}`;
    const displayedUsername = profile.name || username || '';
    const hasCover          = !!profile.cover_image;
    const title             = `${displayedUsername} - Whaleshares`;
    const isSameUser        = authenticated && authenticatedUser.name === username;

    let locationUI = null;
    let websiteUI  = null;
    if (socialTags) {
      if (socialTags.location) {
        locationUI = (
          <div>
            <a href='#' title={`Location: ${socialTags.location}`}>
              <Icon component={faMapMarkerAlt}/>
            </a>
          </div>
        );
      }
      if (socialTags.website) {
        websiteUI = (
          <div>
            <a title="Website Link" target="_blank" rel="noopener noreferrer" href={socialTags.website}>
              <Icon type="global"/>
            </a>
          </div>
        );
      }
    }

    let tags = [];
    if (profile.tags && isArray(profile.tags)) {
      tags = profile.tags.map(topic => (
        <Link key={topic} to={`/discover_people/${topic}`}>
          <Tag>{topic}</Tag>
        </Link>
      ));
    }

    return (
      <div className="main-panel">
        <Helmet>
          <title>{title}</title>
          <link rel="canonical" href={url} />
          <meta property="description" content={desc} />

          <meta property="og:title" content={title} />
          <meta property="og:type" content="article" />
          <meta property="og:url" content={url} />
          <meta property="og:image" content={image} />
          <meta property="og:description" content={desc} />
          <meta property="og:site_name" content="Whaleshares" />

          <meta property="twitter:card" content={image ? 'summary_large_image' : 'summary'} />
          <meta property="twitter:site" content={'@Whaleshares_io'} />
          <meta property="twitter:title" content={title} />
          <meta property="twitter:description" content={desc} />
          <meta property="twitter:image" content={image || 'https://whaleshares.io/images/wls-share.png'} />
        </Helmet>
        <ScrollToTop />
        <ScrollToTopOnMount />
        {user && (
          <PodHero
            authenticated={authenticated}
            user={user}
            username={displayedUsername}
            isSameUser={isSameUser}
            coverImage={profile.cover_image}
            hasCover={hasCover}
            isPopoverVisible={this.state.popoverVisible}
            handleVisibleChange={this.handleVisibleChange}
          />
        )}

        <div className="shifted">
          <div className="layout-twocol container">
            <Affix className="leftContainer leftContainer__user" stickPosition={72}>
              <div className="left">
                <SidenavMain friends={[this.props.friends]} />
              </div>
            </Affix>
            <div className="center main-body">

              {loaded && (
                <div className="PodBox">
                  <div className="PodBox__main">
                    <div className="PodBox__row first">
                      <div className="PodBox__avatar">
                        <PodAvatarLightbox podname={username} size={150} />
                      </div>
                      <div className="PodBox__info">
                        <h2>{displayedUsername}</h2>
                        <div className="PodBox__joined">
                          <FormattedMessage
                            id="founded_date"
                            defaultMessage="@{username} was founded {date}"
                            values={{
                              date: this.props.intl.formatDate(user.created, {
                                      year  : 'numeric',
                                      month : 'short',
                                      day   : 'numeric',
                                    }),
                              username:username,
                            }}
                          />
                        </div>
                        <div className="PodBox__description"> {profile.about}</div>
                        {tags.length > 0 && (<div className="PodBox__interests">Focuses on: {tags}</div>)}
                      </div>
                    </div>
                    <div className="PodBox__row last">
                    </div>
                    <div className='PodBox__socialLinks'>  
                      {socialTags && <SocialLinks profile={socialTags} />}
                      {websiteUI}
                      {locationUI}
                    </div>
                    <div className="PodBox__edit"><PodButtons pod={username} /></div>
                  </div>
                  <div className="PodBox__stats">
                    <div className="PodBox__statbox">
                      <span className="value">{user.post_count}</span>{' '}
                      <span className="param">
                        {this.props.intl.formatMessage({ id: 'posts', defaultMessage: 'Posts' })}
                      </span>
                    </div>
                    <div className="PodBox__statbox">
                      <span className="value">{user.pod.member_count.toLocaleString()}</span>
                      <span className="param">
                        {this.props.intl.formatMessage({id: 'members', defaultMessage: 'Members'})}
                      </span>
                    </div>
                    <div className="PodBox__statbox">
                      <span className="value">
                        {this.numberWithCommas(parseFloat(this.props.user.vesting_shares).toFixed(0))}
                      </span>
                      <span className="param">
                        {this.props.intl.formatMessage({ id: 'whalestake', defaultMessage: 'Whalestake' })}
                      </span>
                    </div>
                  </div>
                </div>
              )}
              <PodMenuWrapper />
              <div className="right-sidebar-layout">
                {loaded && (
                  <>
                    <RightSidebar pod={true} />
                    <div className="main-feed">
                      <Promo type="profile" />
                      {renderRoutes(this.props.route.routes)}
                    </div>
                  </>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Pod;
