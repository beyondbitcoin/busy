export const sortComments = (comments, sortType = 'BEST') => {
  const sortedComments = [...comments];

  const totalPayout = a => parseFloat(a.total_payout_value);
  const netRshares = a => a.net_rshares;

  switch (sortType) {
    case 'BEST':
      return sortedComments.sort((a, b) => {
        const aPayout = totalPayout(a);
        const bPayout = totalPayout(b);

        if (aPayout !== bPayout) {
          return bPayout - aPayout;
        }

        return netRshares(b) - netRshares(a);
      });
    case 'NEWEST':
      return sortedComments.sort((a, b) => Date.parse(a.created) - Date.parse(b.created)).reverse();
    case 'OLDEST':
      return sortedComments.sort((a, b) => Date.parse(a.created) - Date.parse(b.created));
    default:
      return sortedComments;
  }
};

export const sortVotes  = (a, b) => b.rshares - a.rshares;
export const sortTips   = (a, b) => parseFloat(b.amount) - parseFloat(a.amount);

export default null;
