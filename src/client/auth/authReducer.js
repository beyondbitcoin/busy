import * as types from './authActions';

const initialState = {
  isAuthenticated: false,
  isFetching: false,
  isReloading: false,
  isLocked: null,
  hasPass: false,
  loaded: false,
  user: {},
  showLoginModal: false,
  userSCMetaData: {},
  pod: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.LOGIN_START:
      if (action.meta && action.meta.refresh) return state;
      return {
        ...state,
        isFetching: true,
        isAuthenticated: false,
        loaded: false,
        user: {},
      };
    case types.LOGIN_SUCCESS:
      if (action.meta && action.meta.refresh) return state;
      return {
        ...state,
        isFetching: false,
        isAuthenticated: true,
        loaded: true,
        user: action.payload.account || state.user,
        userSCMetaData: action.payload.user_metadata,
        isLocked: action.meta.locked,
        hasPass: action.payload.hasPass || state.hasPass,
        pod: action.payload.pod || state.pod,
      };
    case types.LOGIN_ERROR:
      return {
        ...state,
        isFetching: false,
        isAuthenticated: false,
        loaded: true,
      };
    case types.RELOAD_START:
      return {
        ...state,
        isReloading: true,
      };
    case types.RELOAD_SUCCESS:
      return {
        ...state,
        isReloading: false,
        user: action.payload.account || state.user,
        pod: action.payload.pod || state.pod,
      };
    case types.RELOAD_ERROR:
      return {
        ...state,
        isReloading: false,
      };
    case types.LOGOUT:
      return {
        ...state,
        isAuthenticated: false,
        user: {},
        isLocked: null,
        hasPass: false,
        pod: null,
      };
    case types.LOCK:
      return {
        ...state,
        isLocked: true,
      };
    case types.UNLOCK.SUCCESS:
      return {
        ...state,
        isLocked: false,
      };
    case types.UNLOCK.ERROR:
      return {
        ...state,
        isLocked: true,
      };
    case types.OPEN_MODAL:
      return {
        ...state,
        showLoginModal: true,
      };
    case types.CLOSE_MODAL:
      return {
        ...state,
        showLoginModal: false,
      };
    case types.SAVE_NOTIFICATION_MARK.SUCCESS:
      return {
        ...state,
        userSCMetaData: {
          ...state.userSCMetaData,
          notifications: action.payload,
        },
      };
    default:
      return state;
  }
};

export const getIsAuthenticated = state => state.isAuthenticated;
export const getIsAuthFetching = state => state.isFetching;
export const getIsLoaded = state => state.loaded;
export const getIsReloading = state => state.isReloading;
export const getAuthenticatedUser = state => state.user;
export const getAuthenticatedUserName = state => state.user.name;
export const getAuthenticatedUserSCMetaData = state => state.userSCMetaData;
export const getIsLocked = state => state.isLocked;
export const getShowLoginModal = state => state.showLoginModal;
export const getHasPass = state => state.hasPass;
export const getPod = state => state.pod;
export const isPod = state=> state.user.is_pod;
