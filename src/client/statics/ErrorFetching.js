import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { withRouter, Link } from 'react-router-dom';
import { injectIntl, FormattedMessage } from 'react-intl';
import './ErrorPage.less';

const ErrorFetching = ({ intl, staticContext }) => {
  if (staticContext) {
    staticContext.status = 404; // eslint-disable-line no-param-reassign
  }
  return (
    <div className="ErrorPage container">
      <Helmet>
        <title>{intl.formatMessage({ id: 'page_not_found', defaultMessage: 'Page not found' })}</title>
      </Helmet>
      <h1>
        <FormattedMessage id="page_not_found" defaultMessage="Page not found" />
      </h1>
      <h2>
        <FormattedMessage
          id="page_malformed_message"
          defaultMessage="Oops! Something went wrong while fetching this users data. Please try again in a moment."
        />
      </h2>
      <FormattedMessage
        id="homepage_link_text"
        defaultMessage="Here's a link to {link}."
        values={{
          link: (
            <Link to="/">
              <FormattedMessage id="homepage" defaultMessage="the home page" />
            </Link>
          ),
        }}
      />
    </div>
  );
};

ErrorFetching.propTypes = {
  intl: PropTypes.shape().isRequired,
  staticContext: PropTypes.shape(),
};

ErrorFetching.defaultProps = {
  staticContext: null,
};

export default withRouter(injectIntl(ErrorFetching));
