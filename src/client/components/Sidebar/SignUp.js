import React from 'react';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import './SidebarBlock.less';
import SignupButton from '../SignupButton.js';

const SignUp = () => (
  <div>
    <div className="SidebarBlock">
      <h3 className="SidebarBlock__title">
        <FormattedMessage id="new_to_busy" defaultMessage="New?" />
      </h3>
      <SignupButton text="">
        <button className="SidebarBlock__button">
          <FormattedMessage id="signup" defaultMessage="Sign up" />
        </button>
      </SignupButton>      
    </div>
    <ul className="SidebarList">
      <li>
        <Link className="Welcome" to="/welcome">
          <FormattedMessage id="welcome" defaultMessage="Welcome" />
        </Link>
      </li>
    </ul>
  </div>
);

export default SignUp;
