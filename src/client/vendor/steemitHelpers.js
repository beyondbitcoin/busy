import base58 from 'bs58';
import getSlug from 'speakingurl';
import secureRandom from 'secure-random';
import diff_match_patch from 'diff-match-patch';
import busyAPI from '../busyAPI';
import formatter from '../helpers/steemitFormatter';

const dmp = new diff_match_patch();
/**
 * This function is extracted from steemit.com source code and does the same tasks with some slight-
 * adjustments to meet our needs. Refer to the main one in case of future problems:
 * https://github.com/steemit/steemit.com/blob/edac65e307bffc23f763ed91cebcb4499223b356/app/redux/TransactionSaga.js#L340
 *
 */
export const createCommentPermlink = (parentAuthor, parentPermlink) => {
  let permlink;

  // comments: re-parentauthor-parentpermlink-time
  const timeStr = new Date().toISOString().replace(/[^a-zA-Z0-9]+/g, '');
  const newParentPermlink = parentPermlink.replace(/(-\d{8}t\d{9}z)/g, '');
  permlink = `re-${parentAuthor}-${newParentPermlink}-${timeStr}`;

  if (permlink.length > 255) {
    // STEEMIT_MAX_PERMLINK_LENGTH
    permlink = permlink.substring(permlink.length - 255, permlink.length);
  }
  // only letters numbers and dashes shall survive
  permlink = permlink.toLowerCase().replace(/[^a-z0-9-]+/g, '');
  return permlink;
};

/**
 * https://github.com/steemit/steemit.com/blob/47fd0e0846bd8c7c941ee4f95d5f971d3dc3981d/app/utils/ParsersAndFormatters.js
 */
export function parsePayoutAmount(amount) {
  return parseFloat(String(amount).replace(/\s[A-Z]*$/, ''));
}

/**
 * Calculates Payout Details Modified as needed
 * https://github.com/steemit/steemit.com/blob/47fd0e0846bd8c7c941ee4f95d5f971d3dc3981d/app/components/elements/Voting.jsx
 */
export const calculatePayout = post => {
  const payoutDetails = {};
  // const { comment_tips, parent_author, cashout_time } = post;

  const max_payout = parsePayoutAmount(post.max_accepted_payout);
  let payout = parsePayoutAmount(post.total_payout_value);
  if (payout < 0.0) payout = 0.0;
  if (payout > max_payout) payout = max_payout;
  payoutDetails.payoutLimitHit = payout >= max_payout;

  // There is an "active cashout" if: (a) there is a pending payout, OR (b)
  // there is a valid cashout_time AND it's NOT a comment with 0 votes.
  // const cashout_active =
  //   pending_payout > 0 || (cashout_time.indexOf('1969') !== 0 && !(is_comment && comment_tips.length === 0));

  if (max_payout === 0) {
    payoutDetails.isPayoutDeclined = true;
  } else if (max_payout < 1000000) {
    payoutDetails.maxAcceptedPayout = max_payout;
  }

  if (payout > 0) {
    payoutDetails.pastPayouts = payout;
  }

  return payoutDetails;
};

function checkPermLinkLength(permlink) {
  if (permlink.length > 255) {
    // STEEMIT_MAX_PERMLINK_LENGTH
    permlink = permlink.substring(permlink.length - 255, permlink.length);
  }
  // only letters numbers and dashes shall survive
  permlink = permlink.toLowerCase().replace(/[^a-z0-9-]+/g, '');
  return permlink;
}

function slug(text, len = 128) {
  return getSlug(text.replace(/[<>]/g, ''), { truncate: len });
}

/**
 * Generate permlink
 * https://github.com/steemit/steemit.com/blob/ded8ecfcc9caf2d73b6ef12dbd0191bd9dbf990b/app/redux/TransactionSaga.js
 */

export function createPermlink(title, author, parent_author, parent_permlink, body) {
  let permlink;
  if (title && title.trim() !== '') {
    let max_len = (body.length > 40) ? 128 : 40;
    let s = slug(title, max_len);
    if (s === '') {
      s = base58.encode(secureRandom.randomBuffer(4));
    }

    return busyAPI
      .sendAsync('database_api', 'get_content', [author, s])
      .then(content => {
        let prefix;
        if (content.body !== '') {
          // make sure slug is unique
          prefix = `${base58.encode(secureRandom.randomBuffer(4))}-`;
        } else {
          prefix = '';
          if(s && s.match(/(shares|comments|pods|followers|followed|friends)/))
            prefix = `${base58.encode(secureRandom.randomBuffer(4))}-`;
        }
        permlink = prefix + s;
        return checkPermLinkLength(permlink);
      })
      .catch(err => {
        console.warn('Error while getting content', err);
        return permlink;
      });
  }
  // comments: re-parentauthor-parentpermlink-time
  const timeStr = new Date().toISOString().replace(/[^a-zA-Z0-9]+/g, '');
  parent_permlink = parent_permlink.replace(/(-\d{8}t\d{9}z)/g, '');
  permlink = `re-${parent_author}-${parent_permlink}-${timeStr}`;
  return Promise.resolve(checkPermLinkLength(permlink));
}

/**
 * https://github.com/steemit/steemit.com/blob/ded8ecfcc9caf2d73b6ef12dbd0191bd9dbf990b/app/redux/TransactionSaga.js#L412
 */
function createPatch(text1, text2) {
  if (!text1 && text1 === '') return undefined;
  if (text1.length && (text1 === text2)) return "@@ -1 +1 @@\n-"+text1[0]+"\n+"+text1[0]+"\n";
  const patches = dmp.patch_make(text1, text2);
  const patch = dmp.patch_toText(patches);
  return patch;
}

/**
 * https://github.com/steemit/steemit.com/blob/ded8ecfcc9caf2d73b6ef12dbd0191bd9dbf990b/app/redux/TransactionSaga.js#L329
 */
export function getBodyPatchIfSmaller(originalBody, body) {
  if (!originalBody) return body;
  const patch = createPatch(originalBody, body);
  // Putting body into buffer will expand Unicode characters into their true length
  if (patch && patch.length < new Buffer(body, 'utf-8').length) {
    body = patch;
  }
  return body;
}

export const simple_daily_bandwidth_limit = (stake) => {
  /**
   * simple daily bw based on WHALESTAKE:
   * - break into ranges based on log10 scale (0-10, 10-100, 100-1K...)
   * - then find the max-threshold in linear within that range.
   */

  const stakeRanges = [
    1000,           // 0: 1
    10000,          // 1: 10
    100000,         // 2: 100
    1000000,        // 3: 1K
    10000000,       // 4: 10K
    100000000,      // 5: 100K
    1000000000,     // 6: 1M
    10000000000,    // 7: 10M
    100000000000,   // 8: 100M
    1000000000000,  // 9: 1B
  ];

  let range = 0;

  if (stake >= stakeRanges[8]) {         // 100M
    range = 9;
  } else if (stake >= stakeRanges[7]) {  // 10M
    range = 8;
  } else if (stake >= stakeRanges[6]) {  // 1M
    range = 7;
  } else if (stake >= stakeRanges[5]) {  // 100K
    range = 6;
  } else if (stake >= stakeRanges[4]) {  // 10K
    range = 5;
  } else if (stake >= stakeRanges[3]) {  // 1K
    range = 4;
  } else if (stake >= stakeRanges[2]) {  // 100
    range = 3;
  } else if (stake >= stakeRanges[1]) {  // 10
    range = 2;
  } else if (stake >= stakeRanges[0]) {  // 1
    range = 1;
  } else {
    range = 0;
  }

  // return min/max if range = 0 or 9 here
  if (range >= 9) {
    return 512*1024;
  } else if (range <= 0) {
    return 1024; // make sure everyone has a minimum daily bandwidth
  }

  /**
   * range (whalestake) daily_bandwidth (KB)
   * 10 (> 1B    WLS): 1024
   *  9 (> 100M  WLS): 512
   *  8 (> 10    WLS): 256
   *  7 (> 1M    WLS): 128
   *  6 (> 100K  WLS): 64
   *  5 (> 10K   WLS): 32
   *  4 (> 1K    WLS): 16
   *  3 (> 100   WLS): 8
   *  2 (> 10    WLS): 4
   *  1 (> 1     WLS): 2
   *  0 (< 1     WLS): 1
   */
  const max_threshold = 1024 * (1 << (range + 1));
  const min_threshold = Math.floor(max_threshold/2);
  const bw_range = max_threshold - min_threshold;

  // // for pre hf3
  // const max_stake = stakeRanges[range + 1];
  // const min_stake = stakeRanges[range];
  // const stake_range = max_stake - min_stake;
  // const daily_limit = Math.floor((((stake - stakeRanges[range]) * bw_range)/stake_range) + min_threshold);
  // return daily_limit;

  // after hf3
  const max_stake = stakeRanges[range];
  const min_stake = stakeRanges[range-1];
  const stake_range = max_stake - min_stake;
  const daily_limit = Math.floor((((stake - min_stake) * bw_range)/stake_range) + min_threshold);
  return daily_limit;
};
