import _ from "lodash";
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { Button, Popover, Tooltip } from 'antd';
import Slider from '../components/Slider/Slider';
import Confirmation from '../components/StoryFooter/Confirmation';
import FriendButton from './FriendButton';
import withAuthAction from '../auth/withAuthActions';
import { getAuthenticatedUser, getAuthenticatedUserName, getFollowingList, getMutedList, getPendingFollows, getPendingMutes } from '../reducers';
import { followUser, muteUser, tipUser, unfollowUser } from '../user/userActions';
import './UserBoxButton.less';

const ButtonGroup = Button.Group;

@withAuthAction
@connect(
  state => ({
    user: getAuthenticatedUser(state),
    authenticatedUserName: getAuthenticatedUserName(state),
    followingList: getFollowingList(state),
    pendingFollows: getPendingFollows(state),
    pendingMutes: getPendingMutes(state),
    muted: getMutedList(state),
    pendingTips: state.user.following.pendingTips,
  }),
  {
    followUser,
    unfollowUser,
    muteUser,
    tipUser,
  },
)
class UserBoxButton extends React.Component {
  static propTypes = {
    intl: PropTypes.shape().isRequired,
    user: PropTypes.shape().isRequired,
    username: PropTypes.string.isRequired,
    authenticatedUserName: PropTypes.string,
    followingList: PropTypes.arrayOf(PropTypes.string).isRequired,
    muted: PropTypes.arrayOf(PropTypes.string).isRequired,
    pendingMutes: PropTypes.arrayOf(PropTypes.string).isRequired,
    pendingFollows: PropTypes.arrayOf(PropTypes.string).isRequired,
    pendingTips: PropTypes.arrayOf(PropTypes.string).isRequired,
    onActionInitiated: PropTypes.func.isRequired,
    followUser: PropTypes.func,
    unfollowUser: PropTypes.func,
    muteUser: PropTypes.func,
    tipUser: PropTypes.func,
  };

  static defaultProps = {
    authenticatedUserName: undefined,
    followUser: () => {},
    unfollowUser: () => {},
    muteUser: () => {},
    tipUser: () => {},
  };

  constructor(props) {
    super(props);

    this.state = {
      sliderVisible: false,
      sliderValue: 0,
      memo: '',
    };

    this.handleFollowClick = this.handleFollowClick.bind(this);
    this.followClick = this.followClick.bind(this);
    this.handleMuteClick = this.handleMuteClick.bind(this);
    this.handleMemoChange = this.handleMemoChange.bind(this);
  }

  followClick() {
    const { username } = this.props;
    const isFollowed = this.props.followingList.includes(username);
    if (isFollowed) {
      this.props.unfollowUser(username);
    } else {
      this.props.followUser(username);
    }
  }

  handleFollowClick(e) {
    e.preventDefault();
    this.props.onActionInitiated(this.followClick);
  }

  handleMuteClick(e) {
    e.preventDefault();
    const { username } = this.props;
    const isMuted = this.props.muted.includes(username);
    this.props.onActionInitiated(() => {
      if (isMuted) {
        this.props.muteUser(username, 'null');
      } else {
        this.props.muteUser(username);
      }
    });
  }

  handleTipConfirm = () => {
    const { username } = this.props;
    this.setState({ sliderVisible: false });
    this.props.onActionInitiated(() => {
      this.props.tipUser(username, this.state.sliderValue, this.state.memo);
    });
  };

  handleSliderCancel = () => {
    this.setState({ sliderVisible: false });
  };

  handleVisibleChange = visible => {
    this.setState({ sliderVisible: visible });
  };

  handleSliderChange = value => {
    this.setState({ sliderValue: value });
  };

  handleMemoChange = e => {
    this.setState({memo: e.target.value});
  };

  createFollowHoverText(isFollowed, pending) {
    if (isFollowed && !pending) return this.props.intl.formatMessage({ id: 'unfollow', defaultMessage: 'Unfollow' }); else
      return "";
  }

  createMutedHoverText(isMuted, pending) {
    if (isMuted && !pending) return this.props.intl.formatMessage({ id: 'unmute', defaultMessage: 'Unmute' }); else
      return "";
  }

  createFollowText(isFollowed, pending) {
    let followingText = this.props.intl.formatMessage({ id: 'follow', defaultMessage: 'Follow' });
    if (isFollowed && !pending) {
      followingText = this.props.intl.formatMessage({ id: 'followed', defaultMessage: 'Followed' });
    } else if (isFollowed && pending) {
      followingText = this.props.intl.formatMessage({
        id: 'unfollowing',
        defaultMessage: 'Unfollowing',
      });
    } else if (!isFollowed && !pending) {
      followingText = this.props.intl.formatMessage({ id: 'follow', defaultMessage: 'Follow' });
    } else if (!isFollowed && pending) {
      followingText = this.props.intl.formatMessage({ id: 'followed', defaultMessage: 'Followed' });
    }
    return followingText;
  }

  createMuteText(isFollowed, pending) {
    let followingText = this.props.intl.formatMessage({ id: 'mute', defaultMessage: 'Mute' });
    if (isFollowed && !pending) {
      followingText = this.props.intl.formatMessage({ id: 'muted', defaultMessage: 'Muted' });
    } else if (isFollowed && pending) {
      followingText = this.props.intl.formatMessage({ id: 'unmuting', defaultMessage: 'Unmuting' });
    } else if (!isFollowed && !pending) {
      followingText = this.props.intl.formatMessage({ id: 'mute', defaultMessage: 'Mute' });
    } else if (!isFollowed && pending) {
      followingText = this.props.intl.formatMessage({ id: 'muting', defaultMessage: 'Muting' });
    }
    return followingText;
  }

  render() {
    const {
      intl,
      user,
      authenticatedUserName,
      username,
      followingList,
      pendingFollows,
      pendingMutes,
      muted,
      pendingTips
    } = this.props;
    const followed = followingList.includes(username);
    const pending = pendingFollows.includes(username);
    const pendingMute = pendingMutes.includes(username);
    const mute = muted.includes(username);
    const pendingTip = pendingTips.includes(username);
    const maxValue = parseFloat(_.get(user, 'reward_steem_balance', "0.000 WLS").split(' ')[0]);
    if (authenticatedUserName === username) return null;

    return (
      <ButtonGroup className="UserBoxButton">
        <Popover
          title="Tip User"
          trigger="click"
          visible={this.state.sliderVisible}
          onVisibleChange={this.handleVisibleChange}
          content={maxValue > 0 ? (
            <>
              <Slider
                maxValue={maxValue}
                value={this.state.sliderValue}
                onChange={this.handleSliderChange}
                handleMemoChange={this.handleMemoChange}
              />
              <Confirmation onConfirm={this.handleTipConfirm} onCancel={this.handleSliderCancel} />
            </>
          ): (<span>Top up your Tip Balance first</span>)}
        >
          <Button type="dashed" size="small">Tip</Button>
        </Popover>
        <Tooltip placement="top" title={this.createFollowHoverText(followed, pending)}>
          <Button type="dashed" size="small" onClick={this.handleFollowClick}>
            {this.createFollowText(followed, pending)}
          </Button>
        </Tooltip>
        <Tooltip placement="top" title={this.createMutedHoverText(mute, pendingMute)}>
          <Button type="dashed" size="small" onClick={this.handleMuteClick}>
            {this.createMuteText(mute, pendingMute)}
          </Button>
        </Tooltip>
        <FriendButton username={username} />
      </ButtonGroup>
    );
  }
}

export default injectIntl(UserBoxButton);
