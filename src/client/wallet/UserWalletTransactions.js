import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import ReduxInfiniteScroll from '../vendor/ReduxInfiniteScroll';
import Loading from '../components/Icon/Loading';
import WalletTransaction from './WalletTransaction';
import UserAction from '../activity/UserAction';
import './UserWalletTransactions.less';

const UserWalletTransactions = ({
  transactions,
  actions,
  currentUsername,
  totalVestingShares,
  totalVestingFundSteem,
  type,
}) => {
  const authorRewards = actions.filter(action => action.op[0] === 'author_reward');
  const curationRewards = actions.filter(action => action.op[0] === 'curation_reward');

  return (
    <div className="UserWalletTransactions">
      <ReduxInfiniteScroll
        loadMore={() => {}}
        hasMore={false}
        elementIsScrollable={false}
        threshold={500}
        loader={
          <div className="UserWalletTransactions__loader">
            <Loading />
          </div>
        }
        loadingMore={false}
      >
        <div />
        {type === 'author_reward' &&
          authorRewards.map(action => (
            <UserAction
              totalVestingShares={totalVestingShares}
              totalVestingFundSteem={totalVestingFundSteem}
              action={action}
              currentUsername={currentUsername}
            />
          ))}
        {type === 'curation_reward' &&
          curationRewards.map(action => (
            <UserAction
              totalVestingShares={totalVestingShares}
              totalVestingFundSteem={totalVestingFundSteem}
              action={action}
              currentUsername={currentUsername}
            />
          ))}
        {type === 'wallet' &&
          transactions.map(transaction => (
            <WalletTransaction
              key={`${transaction.trx_id}${transaction.actionCount}`}
              transaction={transaction}
              currentUsername={currentUsername}
              totalVestingShares={totalVestingShares}
              totalVestingFundSteem={totalVestingFundSteem}
            />
          ))}
      </ReduxInfiniteScroll>
      <div className="feed-message">
        <a
          href={`https://whaleshares.io/whalesharesexplorer/#account/${currentUsername}`}
          target="_blank"
          rel="noopener noreferrer"
        >
          <i className="iconfont icon-link" />
          <FormattedMessage id="find_more_explorer" defaultMessage="Go To The Explorer To Find More" />
        </a>
      </div>
    </div>
  );
};

UserWalletTransactions.propTypes = {
  transactions: PropTypes.arrayOf(PropTypes.shape()),
  actions: PropTypes.arrayOf(PropTypes.shape()),
  currentUsername: PropTypes.string,
  totalVestingShares: PropTypes.string.isRequired,
  totalVestingFundSteem: PropTypes.string.isRequired,
  type: PropTypes.string,
};

UserWalletTransactions.defaultProps = {
  transactions: [],
  actions: [],
  currentUsername: '',
  type: 'wallet',
};

export default UserWalletTransactions;
