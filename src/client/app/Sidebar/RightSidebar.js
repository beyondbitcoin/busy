import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Route, Switch, withRouter } from 'react-router-dom';
import { getIsAuthFetching } from '../../reducers';
import PostRecommendation from '../../components/Sidebar/PostRecommendation';
import Loading from '../../components/Icon/Loading';
import WalletSidebar from '../../components/Sidebar/WalletSidebar';
import NewsSidebar from '../../components/Sidebar/NewsRecommendation';
import PodSidebar from '../../components/Sidebar/PodSidebar';
import NavigationRight from './NavigationRight';
import Reminders from './Reminders';
import Promo from '../../components/Promo/Promo';
import './RightSidebar.less';


@withRouter
@connect(
  state => ({
    isAuthFetching  : getIsAuthFetching(state),
  }),
)
class RightSidebar extends React.Component {
  static propTypes = {
    isAuthFetching  : PropTypes.bool,
  };

  static defaultProps = {
    isAuthFetching  : false,
  };

  render() {
    const { isAuthFetching, pod }  = this.props;
    let post = PostRecommendation;
    let news = NewsSidebar;
    if (pod) {
      post = PodSidebar;
      news = PodSidebar;
    }

    return (
      <div className="RightSidebar">
        {isAuthFetching 
          ? ( <Loading/> )
          : (
              <>
                <Switch>
                  <Route exact path="/@:name" component={news}/>
                  <Route path="/@:name/" component={post}/>
                  <Route path="/@:name/transfers" render={() => <WalletSidebar/>}/>
                  <Route path="/" component={NavigationRight}/>
                  <Route path="/created/:tag" component={NavigationRight}/>
                  <Route path="/active/:tag" component={NavigationRight}/>
                </Switch>
                <Reminders />
                <Promo type="aside" />
              </>
            )
        }
      </div>
    );
  }
}

export default RightSidebar;
