import produce from 'immer';
import * as actions from './actions';

const initialState = {
  pendingSentRequests: false,
  pendingReceivedRequests: false,
  friends: false,
  submitting: false,
};

const reducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case actions.FRIENDS_SET:
        draft[action.payload.key] = action.payload.value;
        break;
      default:
        break;
    }
  });

export default reducer;

