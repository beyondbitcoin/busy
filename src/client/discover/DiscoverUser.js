import _ from 'lodash';
import PropTypes from 'prop-types';
import React from 'react';
import { FormattedMessage, injectIntl } from "react-intl";
import { Tag, Icon } from "antd";
import urlParse from 'url-parse';
import { Link } from 'react-router-dom';
import Avatar from '../components/Avatar';
import FollowButton from '../widgets/FollowButton';

const DiscoverUser = ({ user, intl }) => {
  const parsedJSON = _.attempt(JSON.parse, user.json_metadata);
  const userJSON = _.isError(parsedJSON) ? {} : parsedJSON;
  const userProfile = _.has(userJSON, 'profile') ? userJSON.profile : {};
  const location = userProfile.location;
  const name = userProfile.name;
  const about = userProfile.about;
  let website = userProfile.website;

  if (user.post_count == 0 || user.is_pod) {
    return null;
  }

  if (website && website.indexOf('http://') === -1 && website.indexOf('https://') === -1) {
    website = `http://${website}`;
  }

  const url = urlParse(website);
  let hostWithoutWWW = url.host;

  if (hostWithoutWWW.indexOf('www.') === 0) {
    hostWithoutWWW = hostWithoutWWW.slice(4);
  }

  let tags = [];
  if (userProfile.tags && _.isArray(userProfile.tags)) {
    tags = userProfile.tags.map(topic => (
      <Link key={topic} to={`/discover_people/${topic}`}>
        <Tag>{topic}</Tag>
      </Link>
    ));
  }

  return (
    <div key={user.name} className="Discover__user">
      <div className="Discover__user__content">
        <div className="Discover__user__links">
          <Link to={`/@${user.name}`}>
            <Avatar username={user.name} size={40} />
          </Link>
          <div className="Discover__user__profile">
            <div className="Discover__user__profile__header">
              <Link to={`/@${user.name}`}>
                <span className="Discover__user__name">
                  <span className="username">{name || user.name}</span>
                </span>
              </Link>
              <div className="Discover__user__follow">
                <FollowButton username={user.name} />
              </div>
            </div>
            <div className="Discover__user__location">
              <span>
                <Icon type="clock-circle" style={{marginRight: '4px'}}/>
                <FormattedMessage id="joined_date" defaultMessage="Joined {date}"
                                  values={{
                                    date: intl.formatDate(user.created, {
                                      year: 'numeric',
                                      month: 'short',
                                      day: 'numeric'
                                    })
                                  }}
                />
              </span>
              {location && (
                <span>
                  <i className="iconfont icon-coordinates text-icon" />
                  {`${location} `}
                </span>
              )}
              {website && (
                <span>
                  <i className="iconfont icon-link text-icon" />
                  <a target="_blank" rel="noopener noreferrer" href={website}>
                    {`${hostWithoutWWW}${url.pathname.replace(/\/$/, '')}`}
                  </a>
                </span>
              )}
            </div>
            <div className="Discover__user__about">{about}</div>
            {tags.length > 0 && (<div>Writes about: {tags}</div>)}
          </div>
        </div>
      </div>
      <div className="Discover__user__divider" />
    </div>
  );
};

DiscoverUser.propTypes = {
  intl: PropTypes.shape().isRequired,
  user: PropTypes.shape().isRequired,
};

export default injectIntl(DiscoverUser);
