import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import UserActivityActionsList from './UserActivityActionsList';
import UserActivitySearchMessage from './UserActivitySearchMessage';
import { EXPLORER_URL } from '../../common/constants/settings';
import './UserActivityActions.less';

const UserActivityActions = ({ username, isCurrentUser }) => (
  <div className="UserActivityActions">
    <UserActivitySearchMessage />
    <UserActivityActionsList isCurrentUser={isCurrentUser} />
    <div className="feed-message">
      <a href={`${EXPLORER_URL}/#account/${username}`} target="_blank" rel="noopener noreferrer">
        <i className="iconfont icon-link" />
        <FormattedMessage id="find_more_explorer" defaultMessage="Go to the Explorer to find more" />
      </a>
    </div>
  </div>
);

UserActivityActions.propTypes = {
  isCurrentUser: PropTypes.bool,
  username: PropTypes.string,
};

UserActivityActions.defaultProps = {
  isCurrentUser: false,
  username: '',
};

export default UserActivityActions;
