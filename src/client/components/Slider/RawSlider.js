import React from 'react';
import PropTypes from 'prop-types';
import { Radio } from 'antd';
import './RawSlider.less';

const RadioButton = Radio.Button;
const RadioGroup  = Radio.Group;

class RawSlider extends React.Component {
  static propTypes = {
    initialValue  : PropTypes.number,
    maxValue      : PropTypes.number,
    onChange      : PropTypes.func,
    tipFormatter  : PropTypes.func,
  };

  static defaultProps = {
    initialValue  : 1,
    maxValue      : 1,
    onChange      : () => {},
    tipFormatter  : value => `${value}%`,
  };

  state = {
    value: 1,
  };

  componentWillMount() {
    this.setState({ value: this.props.initialValue });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.initialValue !== this.props.initialValue) {
      this.setState({
        value: nextProps.initialValue,
      });
    }
  }
  handlePresetChange = event => {
    if ( event.target.value <= this.props.maxValue ) {
      this.setState({ value: event.target.value }, () => {
        this.props.onChange(event.target.value);
      });
    }
  };

  handleChange = value => {
    this.setState({ value }, () => {
      this.props.onChange(value);
    });
  };

  render() {
    const { value } = this.state;

    return (
      <div className="RawSlider">
        <div className="RawSlider__presets">
          <RadioGroup value={value} size="large" onChange={this.handlePresetChange}>
            <RadioButton value={1}>1 WLS</RadioButton>
            <RadioButton value={5}>5 WLS</RadioButton>
            <RadioButton value={10}>10 WLS</RadioButton>
            <RadioButton value={20}>20 WLS</RadioButton>
            <RadioButton value={50}>50 WLS</RadioButton>
            <RadioButton value={100}>100 WLS</RadioButton>
          </RadioGroup>
        </div>
      </div>
    );
  }
}

export default RawSlider;
