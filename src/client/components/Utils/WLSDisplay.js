import React from 'react';
import PropTypes from 'prop-types';
import { FormattedNumber } from 'react-intl';

const WLSDisplay = ({ value, fraction }) => (
  <span>
    <FormattedNumber value={value} minimumFractionDigits={fraction} maximumFractionDigits={fraction} />
    {' WLS'}
  </span>
);

WLSDisplay.propTypes = {
  value: PropTypes.number,
  fraction: PropTypes.number,
};

WLSDisplay.defaultProps = {
  value: 0,
  fraction: 3,
};

export default WLSDisplay;
