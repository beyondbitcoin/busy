import Wrapper from '../client/Wrapper';
import Bookmarks from '../client/bookmarks/Bookmarks';
import Drafts from '../client/post/Write/Drafts';
import Replies from '../client/replies/Replies';
import MyComments from '../client/my_comments';
import Wallet from '../client/wallet/Wallet';
import Editor from '../client/post/Write/Write';
import ProfileWrap from "../client/profile_wrap";
// import User from '../client/user/User';
import UserProfile from '../client/user/UserProfile';
import UserShares from '../client/user/UserShares';
// import UserComments from '../client/user/UserComments';
import UserPods from '../client/user/UserPods';
import UserFollowers from '../client/user/UserFollowers';
import UserFollowing from '../client/user/UserFollowing';
import Post from '../client/post/Post';
import Page from '../client/feed/Page';
import Landing from '../client/landing/Landing';
import Discover from '../client/discover/Discover';
import UserFriends from '../client/user/UserFriends';
import UsersMuted from '../client/user/UsersMuted';
import Notifications from '../client/notifications/Notifications';
import Error404 from '../client/statics/Error404';
import Privacy from '../client/statics/Privacy';
import Welcome from '../client/statics/Welcome';
import Donate from '../client/statics/Donate';
import XSS from '../client/statics/XSS';
import Login from '../client/auth/Login';
import PodMembers from '../client/pods/PodMembers';
import PodToken from '../client/pods/PodToken';
import PodDescription from '../client/pods/PodDescription';
import PodNews from '../client/pods/PodNews';
import PodList from '../client/pods/PodList';
import SendFriendRequest from '../client/friends/SendFriendRequest';
import ReceivedFriendRequest from '../client/friends/ReceivedFriendRequest';
import MyFriends from '../client/friends/MyFriends';
import Tips from '../client/tips/index';
import TipsReceived from '../client/tips/received';
import TipsSent from '../client/tips/sent';

const routes = [
  {
    component: Wrapper,
    routes: [
      {path: '/', exact: true, component: Landing},
      {path: '/login', exact: true, component: Login},
      {path: '/bookmarks', exact: true, component: Bookmarks},
      {path: '/drafts', exact: true, component: Drafts},
      {path: '/replies', exact: true, component: Replies},
      {path: '/comments', exact: true, component: MyComments},
      {path: '/tips/(received|sent)?', exact: true, component: Tips,
        routes: [
          {path: '/tips/received', exact: true, component: TipsReceived},
          {path: '/tips/sent', exact: true, component: TipsSent},
        ]
      },
      {path: '/status', exact: true, component: Wallet},
      {path: '/editor', component: Editor},
      {path: '/pod_list', exact: true, component: PodList},
      {path: '/friend_send_request', exact: true, component: SendFriendRequest},
      {path: '/friend_received_request', exact: true, component: ReceivedFriendRequest},
      {path: '/my_friends', exact: true, component: MyFriends},
      {path: '/notifications', exact: true, component: Notifications},
      {path: '/@:name/(shares|pods|followers|followed|friends|muted|members|news|token|about)?', component: ProfileWrap,
        exact: true,
        routes: [
          {path: '/@:name', exact: true, component: UserProfile},
          {path: '/@:name/shares', exact: true, component: UserShares},
          // {path: '/@:name/comments', exact: true, component: UserComments},
          {path: '/@:name/pods', exact: true, component: UserPods},
          {path: '/@:name/followers', exact: true, component: UserFollowers},
          {path: '/@:name/followed', exact: true, component: UserFollowing},
          {path: '/@:name/friends', exact: true, component: UserFriends},
          {path: '/@:name/muted', exact: true, component: UsersMuted},
          {path: '/@:name/members', exact: true, component: PodMembers},
          {path: '/@:name/news', exact: true, component: PodNews},
          {path: '/@:name/token', exact: true, component: PodToken},
          {path: '/@:name/about', exact: true, component: PodDescription},
        ],
      },
      {path: '/discover_people/:interest?', exact: true, component: Discover},
      {path: '/:category?/@:author/:permlink', component: Post},
      {path: '/:sortBy(created)/:category?', component: Page},
      {path: '/myfeed', exact: true, component: Page},
      {path: '/privacy', component: Privacy},
      {path: '/welcome', component: Welcome},
      {path: '/donate', component: Donate},
      {path: '/xss/test', exact: true, component: XSS},
      {path: '*', component: Error404},
    ],
  },
];

export default routes;
