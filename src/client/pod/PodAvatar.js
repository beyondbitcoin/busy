import React from 'react';
import PropTypes from 'prop-types';
import './PodAvatar.less';
import { WLS_IMG_PROXY } from '../../common/constants/settings';

export function getAvatarURL(podname, size = 100) {
  let wxh = 128;
  if (size <= 32) {
    wxh = 32;
  } else if (size <= 48) {
    wxh = 48;
  } else if (size <= 64) {
    wxh = 64;
  } else if (size <= 96) {
    wxh = 96;
  } else {
    wxh = 128;
  }

  return `${WLS_IMG_PROXY}/pod_profileimage/${podname}/${wxh}x${wxh}`;
}

const PodAvatar = ({ podname, size }) => {
  let style = {
    minWidth: `${size}px`,
    width: `${size}px`,
    height: `${size}px`,
  };

  const url = getAvatarURL(podname, size);

  if (podname) {
    style = {
      ...style,
      backgroundImage: `url(${url})`,
    };
  }

  return <div className="PodAvatar" style={style} />;
};

PodAvatar.propTypes = {
  podname: PropTypes.string.isRequired,
  size: PropTypes.number,
};

PodAvatar.defaultProps = {
  size: 100,
};

export default PodAvatar;
