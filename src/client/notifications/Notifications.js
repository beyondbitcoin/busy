import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { PageHeader, Button } from 'antd';
import SidenavMain from '../components/Navigation/SidenavMain';
import Affix from '../components/Utils/Affix';
import { getNotifications, getNotificationsMore } from '../user/userActions';
import {
  getAuthenticatedUserSCMetaData,
  getNotifications as getNotificationsState,
  getIsLoadingNotifications,
  getIsLoadingNotificationsMore,
  getIsHasMoreNotifications,
  getAuthenticatedUserName,
} from '../reducers';
import { saveNotificationsMark } from '../auth/authActions';
import requiresLogin from '../auth/requiresLogin';
import Loading from '../components/Icon/Loading';
import NotificationItem from "../components/Navigation/Notifications/NotificationItem";
import ReduxInfiniteScroll from '../vendor/ReduxInfiniteScroll';
import './Notifications.less';
import '../components/Navigation/Notifications/Notification.less';

class Notifications extends React.Component {
  static propTypes = {
    loadingNotifications: PropTypes.bool.isRequired,
    loadingNotificationsMore: PropTypes.bool.isRequired,
    getNotifications: PropTypes.func.isRequired,
    getNotificationsMore: PropTypes.func.isRequired,
    hasMoreNotifications: PropTypes.bool.isRequired,
    notifications: PropTypes.array,
    currentAuthUsername: PropTypes.string,
    userSCMetaData: PropTypes.shape(),
    saveNotificationsMark: PropTypes.func.isRequired,
  };

  static defaultProps = {
    notifications: [],
    currentAuthUsername: '',
    userSCMetaData: {},
  };

  componentDidMount() {
    const { notifications, currentAuthUsername, userSCMetaData } = this.props;
    var notify_user = userSCMetaData.notifications && userSCMetaData.notifications.user || '';
    var notify_tm = userSCMetaData.notifications && userSCMetaData.notifications.tm || 0;
    if (_.isEmpty(notifications) || currentAuthUsername != notify_user || new Date().getMinutes() != notify_tm) {
      this.props.saveNotificationsMark(currentAuthUsername, new Date().getMinutes());
      this.props.getNotifications();
    }
  }

  render() {
    const {
      notifications, currentAuthUsername, userSCMetaData, loadingNotifications, loadingNotificationsMore,
      hasMoreNotifications
    } = this.props;
    // const lastSeenTimestamp = _.get(userSCMetaData, 'notifications_last_timestamp');
    const read_mark = userSCMetaData.notifications[currentAuthUsername] ? userSCMetaData.notifications[currentAuthUsername].read : 0;

    return (
      <div className="shifted">
        <div className="feed-layout container">
          <Affix className="leftContainer" stickPosition={77}>
            <div className="left">
              <SidenavMain />
            </div>
          </Affix>
          <div className="NotificationsPage">
            <PageHeader
              title={
                <div className="NotificationsPage__title">
                  <h1><FormattedMessage id="notifications" defaultMessage="Notifications"/></h1>
                </div>
              }
              extra={[
                <Button key='btnMarkAll' onClick={() => {
                  if (_.isEmpty(notifications)) return;
                  const last_index = notifications[0][0];
                  this.props.saveNotificationsMark(this.props.currentAuthUsername, null, last_index);
                }}>Mark all as read</Button>,
              ]}
            >
              <div className="NotificationsPage__content">
                {loadingNotifications && (<div className="NotificationsPage__loading"><Loading/></div>)}
                {this.props.notifications && (this.props.notifications.length > 0) && (
                  <ReduxInfiniteScroll
                    elementIsScrollable={false}
                    loadingMore={loadingNotificationsMore}
                    hasMore={hasMoreNotifications}
                    loader={<Loading/>}
                    loadMore={this.props.getNotificationsMore}
                  >
                    {_.map(notifications, (item) => {
                      const [idx, obj] = item;
                      const key = `notification_${idx}`;
                      const isRead = read_mark >= idx;
                      return <NotificationItem key={key} idx={idx} obj={obj}
                                               currentAuthUsername={currentAuthUsername} read={isRead}/>;
                    })}
                  </ReduxInfiniteScroll>
                )}
                {_.isEmpty(notifications) && !loadingNotifications && (
                  <div className="Notification Notification__empty">
                    <FormattedMessage id="notifications_empty_message"
                                      defaultMessage="You currently have no notifications."/>
                  </div>
                )}
              </div>
            </PageHeader>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    notifications: getNotificationsState(state),
    userSCMetaData: getAuthenticatedUserSCMetaData(state),
    currentAuthUsername: getAuthenticatedUserName(state),
    loadingNotifications: getIsLoadingNotifications(state),
    loadingNotificationsMore: getIsLoadingNotificationsMore(state),
    hasMoreNotifications: getIsHasMoreNotifications(state),
  }),
  {
    getNotifications,
    getNotificationsMore,
    saveNotificationsMark,
  },
)(requiresLogin(Notifications));
