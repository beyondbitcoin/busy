import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import { loginWithPostingKey } from '../auth/authActions';
import './Login.less';
import SignupButton from '../components/SignupButton.js';

@connect(
  () => ({}),
  {
    loginWithPostingKey,
  },
)
class Login extends React.Component {
  static propTypes = {
    loginWithPostingKey: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      username: '',
      postingKey: '',
      pin: '',
    };
  }

  handleChange = event => {
    let value = event.target.value;
    if (event.target.name === 'username') value = value.replace(/[^.-a-z0-9]/ig,'').toLowerCase();
    this.setState({ [event.target.name]: value });
  };

  handleSubmit = () => {
    this.props.loginWithPostingKey(this.state.username, this.state.postingKey, this.state.pin);
  };

  render() {
    const FormItem = Form.Item;

    return (
      <div className="LoginForm">
        <div className="column LoginForm__visual" />
        <div className="column LoginForm__text">
          <div className="LoginForm__main">
            <h1>Login to Whaleshares</h1>
            <p>Please use your Social Key.</p>
            <Form className="login-form">
              <FormItem>
                <Input
                  prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  placeholder="Username"
                  name="username"
                  value={this.state.username}
                  onChange={this.handleChange}
                />
              </FormItem>
              <FormItem>
                <Input
                  prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  type="password"
                  placeholder="Social Key (leave blank for WhaleVault)"
                  name="postingKey"
                  value={this.state.postingKey}
                  onChange={this.handleChange}
                />
              </FormItem>
              {/*
              <FormItem extra="Pin is an optional 4-digit code you set to lock and unlock your password to keep it more secure. When locked, password can't be used without the pin you set.">
                <Input
                  prefix={<Icon type="key" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  type="pin"
                  placeholder="4-digit pin (optional)"
                  name="pin"
                  value={this.state.pin}
                  onChange={this.handleChange}
                />
              </FormItem>
              */}
              <FormItem>
                <Checkbox>Keep me logged in</Checkbox>
                <Button type="primary" htmlType="submit" className="login-form-button" onClick={this.handleSubmit}>
                  Log In
                </Button>
                Or{' '}
                <SignupButton text="Sign Up!" button={false} />
              </FormItem>
            </Form>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(Login);
