import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { withRouter, Link } from 'react-router-dom';
import { injectIntl, FormattedMessage } from 'react-intl';
import './ErrorPage.less';

const ErrorUserNotRegistered = ({ intl, staticContext }) => {
  if (staticContext) {
    staticContext.status = 404; // eslint-disable-line no-param-reassign
  }
  return (
    <div className="ErrorPage container">
      <Helmet>
        <title>{intl.formatMessage({ id: 'page_not_found', defaultMessage: 'User not found' })}</title>
      </Helmet>
      <h1>
        <FormattedMessage id="user_not_found" defaultMessage="User not found" />
      </h1>
      <img className="full-page-art" src="/images/user-not-found.svg" alt="illustration" />
      <h2>
        <FormattedMessage
          id="page_malformed_message"
          defaultMessage="Oops! It appears this user isn't registered. Please check the spelling, and try again."
        />
      </h2>
      <FormattedMessage
        id="homepage_link_text"
        defaultMessage="Here's a link to {link}."
        values={{
          link: (
            <Link to="/">
              <FormattedMessage id="homepage" defaultMessage="the home page" />
            </Link>
          ),
        }}
      />
    </div>
  );
};

ErrorUserNotRegistered.propTypes = {
  intl: PropTypes.shape().isRequired,
  staticContext: PropTypes.shape(),
};

ErrorUserNotRegistered.defaultProps = {
  staticContext: null,
};

export default withRouter(injectIntl(ErrorUserNotRegistered));
