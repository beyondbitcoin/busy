import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Route, Switch, withRouter } from 'react-router-dom';
import { getIsAuthFetching } from '../reducers';
import { updateRecommendations } from '../user/userActions';
import PostRecommendation from '../components/Sidebar/PostRecommendation';
import Loading from '../components/Icon/Loading';
import PodSidebar from '../components/Sidebar/PodSidebar';
import NavigationRight from '../app/Sidebar/NavigationRight';
import './PodRightSidebar.less';

@withRouter
@connect(
  state => ({
    isAuthFetching: getIsAuthFetching(state),
  }),
  {
    updateRecommendations,
  },
)
class PodRightSidebar extends React.Component {
  static propTypes = {
    isAuthFetching: PropTypes.bool,
    showPostRecommendation: PropTypes.func,
  };

  static defaultProps = {
    isAuthFetching: false,
    showPostRecommendation: null,
  };

  render() {
    const {showPostRecommendation, isAuthFetching} = this.props;

    return (
      <div className="PodRightSidebar">
        {isAuthFetching 
          ? ( <Loading/> )
          : (
              <>
                <Switch>
                  <Route path="/@:name" component={PodSidebar}/>
                  <Route path="/@:name/news" component={PodSidebar}/>
                  <Route path="/@:name/members" component={PodSidebar}/>
                  <Route path="/@:name/token" component={PodSidebar}/>
                  <Route path="/@:name/about" component={PodSidebar}/>

                  <Route path="/" component={NavigationRight}/>
                  <Route path="/created/:tag" component={NavigationRight}/>
                  <Route path="/active/:tag" component={NavigationRight}/>
                </Switch>
                {showPostRecommendation && <PostRecommendation isAuthFetching={isAuthFetching}/>}
              </>
            )
        }
      </div>
    );
  }
}

export default PodRightSidebar;
