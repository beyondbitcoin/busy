import * as actions from './userActions';
import * as appTypes from '../app/appActions';
import _ from 'lodash';
import DMCA from '../../common/constants/dmca.json';

const initialState = {
  recommendations: [],
  following: {
    muted: _.get(DMCA, 'spammers', []),
    list: [],
    pendingFollows: [],
    pendingMutes: [],
    pendingTips: [], // for tipping user in user profile page
    isFetching: false,
    fetched: false,
  },
  notifications: [],
  latestNotification: {},
  loadingNotifications: false,
  loadingNotificationsMore: false,
  hasMoreNotifications: false,
  fetchFollowListError: false,
  pods: [],
};

// filterRecommendations generates a random list of `count` recommendations
// include users followed by the current user.
const filterRecommendations = (following, count = 5) => {
  // const usernames = Object.values(following);
  // return people
  //   .filter(p => !usernames.includes(p))
  //   .sort(() => 0.5 - Math.random())
  //   .slice(0, count)
  //   .map(name => ({ name }));
  return [];
};

export default function userReducer(state = initialState, action) {
  switch (action.type) {
    case actions.GET_USER_PODS.SUCCESS:
      return {
        ...state,
        pods: [...state.pods, ...action.payload],
        hasMore: (action.payload.length >= 100),
      };
    case actions.GET_FOLLOWING_START:
      return {
        ...state,
        following: {
          ...state.following,
          list: [],
          isFetching: true,
          fetched: false,
        },
        fetchFollowListError: false,
      };
    case actions.GET_FOLLOWING_ERROR:
      return {
        ...state,
        following: {
          ...state.following,
          list: [],
          isFetching: false,
          fetched: true,
        },
        fetchFollowListError: true,
      };
    case actions.GET_FOLLOWING_SUCCESS:
      return {
        ...state,
        recommendations: filterRecommendations(action.payload),
        following: {
          ...state.following,
          list: action.payload,
          isFetching: false,
          fetched: true,
        },
        fetchFollowListError: false,
      };
    case actions.GET_MUTED.SUCCESS:
      return {
        ...state,
        following: {
          ...state.following,
          muted: action.payload,
        },
      };
    case actions.MUTE_USER.START:
    case actions.UNMUTE_USER.START:
      return {
        ...state,
        following: {
          ...state.following,
          pendingMutes: [...state.following.pendingMutes, action.meta.username],
        },
      };
    case actions.FOLLOW_USER_START:
    case actions.UNFOLLOW_USER_START:
      return {
        ...state,
        following: {
          ...state.following,
          pendingFollows: [...state.following.pendingFollows, action.meta.username],
        },
      };
    case actions.FOLLOW_USER_SUCCESS:
      return {
        ...state,
        following: {
          ...state.following,
          list: [...state.following.list, action.meta.username],
          pendingFollows: state.following.pendingFollows.filter(user => user !== action.meta.username),
        },
      };
    case actions.UNFOLLOW_USER_SUCCESS:
      return {
        ...state,
        following: {
          ...state.following,
          list: state.following.list.filter(user => user !== action.meta.username),
          pendingFollows: state.following.pendingFollows.filter(user => user !== action.meta.username),
        },
      };
    case actions.UNMUTE_USER.SUCCESS:
      return {
        ...state,
        following: {
          ...state.following,
          muted: state.following.muted.filter(user => user !== action.meta.username),
          pendingMutes: state.following.pendingMutes.filter(user => user !== action.meta.username),
        },
      };
    case actions.MUTE_USER.SUCCESS:
      return {
        ...state,
        following: {
          ...state.following,
          muted: [...state.following.muted, action.meta.username],
          pendingMutes: state.following.pendingMutes.filter(user => user !== action.meta.username),
        },
      };
    case actions.FOLLOW_USER_ERROR:
    case actions.UNFOLLOW_USER_ERROR:
    case actions.MUTE_USER.ERROR:
      return {
        ...state,
        following: {
          ...state.following,
          pendingFollows: state.following.pendingFollows.filter(user => user !== action.meta.username),
          pendingMutes: state.following.pendingMutes.filter(user => user !== action.meta.username),
        },
      };
    case actions.TIP_USER.START:
      return {
        ...state,
        following: {
          ...state.following,
          pendingTips: [...state.following.pendingTips, action.meta.username],
        },
      };
    case actions.TIP_USER.SUCCESS:
    case actions.TIP_USER.ERROR:
      return {
        ...state,
        following: {
          ...state.following,
          pendingTips: state.following.pendingTips.filter(user => user !== action.meta.username),
        },
      };
    case actions.UPDATE_RECOMMENDATIONS:
      return {
        ...state,
        recommendations: filterRecommendations(state.following.list),
      };

    case actions.GET_NOTIFICATIONS.START:
      return {
        ...state,
        loadingNotifications: true,
      };

    case actions.GET_NOTIFICATIONS.SUCCESS:
      return {
        ...state,
        notifications: action.payload,
        loadingNotifications: false,
        hasMoreNotifications: action.payload[action.payload.length-1][0] > 1,
      };

    case actions.GET_NOTIFICATIONS.ERROR:
      return {
        ...state,
        loadingNotifications: false,
      };

    case actions.GET_NOTIFICATIONS_MORE.START:
      return {
        ...state,
        loadingNotificationsMore: true,
      };

    case actions.GET_NOTIFICATIONS_MORE.SUCCESS:
      return {
        ...state,
        notifications: state.notifications.concat(action.payload),
        loadingNotificationsMore: false,
        hasMoreNotifications: action.payload[action.payload.length-1][0] > 1,
      };

    case actions.GET_NOTIFICATIONS_MORE.ERROR:
      return {
        ...state,
        loadingNotificationsMore: false,
      };

    case appTypes.ADD_NEW_NOTIFICATION:
      return {
        ...state,
        notifications: [action.payload, ...state.notifications],
        latestNotification: action.payload,
      };
    default: {
      return state;
    }
  }
}

export const getFollowingList = state => state.following.list;
export const getMutedList = state => state.following.muted;
export const getPendingMutes = state => state.following.pendingMutes;
export const getPendingFollows = state => state.following.pendingFollows;
export const getIsFetchingFollowingList = state => state.following.isFetching;
export const getRecommendations = state => state.recommendations;
export const getFollowingFetched = state => state.following.fetched;
export const getNotifications = state => state.notifications;
export const getIsLoadingNotifications = state => state.loadingNotifications;
export const getIsLoadingNotificationsMore = state => state.loadingNotificationsMore;
export const getIsHasMoreNotifications = state => state.hasMoreNotifications;
export const getFetchFollowListError = state => state.fetchFollowListError;
export const getLatestNotification = state => state.latestNotification;
// export const getPods = state => state.pods;
