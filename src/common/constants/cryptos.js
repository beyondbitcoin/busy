export const WLS = {
  id: 'WLS',
  name: 'WLS',
  symbol: 'WLS',
};

export const CRYPTO_MAP = {
  [WLS.symbol]: WLS,
};
