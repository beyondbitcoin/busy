export const SUPPORTED_LANGUAGES = {
  'en-US': { longName: 'English', shortName: 'English' },
  //  'id-ID': { longName: 'Bahasa Indonesia - Indonesian', shortName: 'Bahasa Indonesia' },
  //  'ms-MY': { longName: 'Bahasa Melayu - Malay', shortName: 'Bahasa Melayu' },
  //  'ca-ES': { longName: 'Català - Catalan', shortName: 'Català' },
  //  'cs-CZ': { longName: 'Čeština - Czech', shortName: 'Čeština' },
  //  'da-DK': { longName: 'Dansk - Danish', shortName: 'Dansk' },
  //  'de-DE': { longName: 'Deutsch - German', shortName: 'Deutsch' },
  //  'et-EE': { longName: 'Eesti - Estonian', shortName: 'Eesti' },
  'es-ES': { longName: 'Español - Spanish', shortName: 'Español' },
  //  'fil-PH': { longName: 'Filipino', shortName: 'Filipino' },
  //  'fr-FR': { longName: 'Français - French', shortName: 'Français' },
  //  'hr-HR': { longName: 'Hrvatski - Croatian', shortName: 'Hrvatski' },
  //  'it-IT': { longName: 'Italiano - Italian', shortName: 'Italiano' },
  //  'hu-HU': { longName: 'Magyar - Hungarian', shortName: 'Magyar' },
  //  'nl-NL': { longName: 'Nederlands - Dutch', shortName: 'Nederlands' },
  //  'no-NO': { longName: 'Norsk - Norwegian', shortName: 'Norsk' },
  //  'pl-PL': { longName: 'Polski - Polish', shortName: 'Polski' },
  //  'pt-BR': { longName: 'Português - Portuguese', shortName: 'Português' },
  //  'ro-RO': { longName: 'Română - Romanian', shortName: 'Română' },
  //  'sl-SI': { longName: 'Slovenščina - Slovenian', shortName: 'Slovenščina' },
  //  'sv-SE': { longName: 'Svenska - Swedish', shortName: 'Svenska' },
  //  'vi-VN': { longName: 'Tiếng Việt - Vietnamese', shortName: 'Tiếng Việt' },
  //  'tr-TR': { longName: 'Türkçe - Turkish', shortName: 'Türkçe' },
  //  'yo-NG': { longName: 'Yorùbá - Yoruba', shortName: 'Yorùbá' },
  //  'el-GR': { longName: 'Ελληνικά - Greek', shortName: 'Ελληνικά' },
  //  'bg-BG': { longName: 'Български език - Bulgarian', shortName: 'Български език' },
  //  'ru-RU': { longName: 'Русский - Russian', shortName: 'Русский' },
  //  'uk-UA': { longName: 'Українська мова - Ukrainian', shortName: 'Українська мова' },
  //  'he-IL': { longName: 'עִבְרִית - Hebrew', shortName: 'עִבְרִית' },
  //  'ar-SA': { longName: 'العربية - Arabic‏', shortNameP: 'العربية' },
  //  'ne-NP': { longName: 'नेपाली - Nepali', shortName: 'नेपाली' },
  //  'hi-IN': { longName: 'हिन्दी - Hindi', shortName: 'हिन्दी' },
  //  'as-IN': { longName: 'অসমীয়া - Assamese', shortName: 'অসমীয়া' },
  //  'bn-IN': { longName: 'বাংলা - Bengali', shortName: 'বাংলা' },
  //  'ta-IN': { longName: 'தமிழ் - Tamil', shortName: 'தமிழ்' },
  //  'lo-LA': { longName: 'ພາສາລາວ - Lao', shortName: 'ພາສາລາວ' },
  //  'th-TH': { longName: 'ภาษาไทย - Thai', shortName: 'ภาษาไทย' },
  //  'ko-KR': { longName: '한국어 - Korean', shortName: '한국어' },
  //  'ja-JP': { longName: '日本語 - Japanese', shortName: '日本語' },
  //  'zh-CN': { longName: '简体中文 - Simplified Chinese', shortName: '简体中文' },
  // zh: '繁體中文 - Traditional Chinese',
};

export const FIXED_TAGS = [
  'animals',
  'art',
  'business',
  'comedy',
  'entertainment',
  'family',
  'finance',
  'food',
  'gaming',
  'health',
  'homesteading',
  'industry',
  'internet',
  'introduceyourself',
  'lifestyle',
  'music',
  'nature',
  'off-topic',
  'photography',
  'poetry',
  'politics',
  'religion',
  'science',
  'society',
  'sports',
  'technology',
  'travel',
  'writing',
  'whaleshares',
];

export const DEFAULT_POD_RANKS = {
  posts: [0, 10, 100, 1000, 10000],
  comments: [0, 10, 100, 1000, 10000],
  names: ["Level 0", "Level 1", "Level 2", "Level 3", "Level 4"],
  badges: [
    "https://whaleshares.io/images/icons/icon-128x128.png",
    "https://whaleshares.io/images/icons/icon-128x128.png",
    "https://whaleshares.io/images/icons/icon-128x128.png",
    "https://whaleshares.io/images/icons/icon-128x128.png",
    "https://whaleshares.io/images/icons/icon-128x128.png",
  ]
};

export const MOODS = {
  'angry'     : 'angry',
  'astonished': 'astonished',
  'calm'      : 'pensive',
  'cool'      : 'sunglasses-smile',
  'crazy'     : 'zany_face',
  'crying'    : 'cry',
  'dabbing'   : 'dabbing',
  'grinning'  : 'grinning',
  'innocent'  : 'innocent',
  'loving'    : 'heart_eyes',
  'lucky'     : 'star-struck-grinning_face_with_star_eyes',
  'masked'    : 'mask',
  'malicious' : 'malicious_face',
  'nerdy'     : 'nerd_face',
  'rich'      : 'money_mouth_face',
  'rotfl'     : 'rolling_on_the_floor_laughing', 
  'sad'       : 'white_frowning_face',
  'sick'      : 'nauseated_face',
  'shocked'   : 'open_mouth',
  'sleepy'    : 'sleeping',
  'smirky'    : 'smirk',
  'sobbing'   : 'sob',
  'wondering' : 'face_with_monocle',
  'zipped'    : 'zipper_mouth_face',
};

export const SIGNUP_URL = 'https://signup.whaleshares.io';
export const WLS_RPC_URL = process.env.WLS_RPC_URL || 'https://pubrpc.whaleshares.io';
export const WALLET_URL = process.env.WALLET_URL ||'https://wallet.whaleshares.io';
export const EXPLORER_URL = process.env.EXPLORER_URL || 'https://explorer.whaleshares.io';
export const WHALEPAPER_URL =
  'https://gitlab.com/beyondbitcoin/whaleshares-chain/wikis/uploads/71010b5a2493e95f343b0c7de1da5fce/Whalepaper_v1.pdf';
export const DISCORD_URL = 'https://discord.gg/3pqBXKY';
//export const WLS_IMG_PROXY = 'https://hotroast.net/imgp';
export const WLS_IMG_PROXY = 'https://imgp.whaleshares.io';

export default null;
