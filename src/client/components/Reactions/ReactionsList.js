import React from 'react';
import PropTypes from 'prop-types';
import { Scrollbars } from 'react-custom-scrollbars';
import InfiniteSroll from 'react-infinite-scroller';
import { take } from 'lodash';
import UserCard from '../UserCard';
import WLSDisplay from '../Utils/WLSDisplay';
import './ReactionsList.less';

export default class UserList extends React.Component {
  static propTypes = {
    votes: PropTypes.arrayOf(PropTypes.shape()),
  };

  static defaultProps = {
    votes: [],
    ratio: 0,
  };

  state = { page: 1 };

  paginate = () => this.setState(prevState => ({ page: prevState.page + 1 }));

  render() {
    const { votes } = this.props;
    const defaultPageItems = 20;
    const noOfItemsToShow = defaultPageItems * this.state.page;

    return (
      <Scrollbars autoHide style={{ height: '400px' }}>
        <InfiniteSroll
          pageStart={0}
          loadMore={this.paginate}
          hasMore={votes.length > noOfItemsToShow}
          useWindow={false}
        >
          <div className="ReactionsList__content">
            {take(votes, noOfItemsToShow).map(vote => (
              <UserCard
                key={vote.tipper}
                username={vote.tipper}
                alt={
                  <span>
                   <WLSDisplay value={parseFloat(vote.amount)}/>
                  </span>
                }
              />
            ))}
          </div>
        </InfiniteSroll>
      </Scrollbars>
    );
  }
}
