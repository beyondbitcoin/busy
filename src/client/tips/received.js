import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { FormattedRelative } from "react-intl";
import { Icon, Table } from 'antd';
import Avatar from '../components/Avatar';
import ReduxInfiniteScroll from "../vendor/ReduxInfiniteScroll";
import Loading from "../components/Icon/Loading";
import { fetchTipsReceived, fetchTipsReceivedMore } from './actions';
import '../styles/modules/account_list.less';
import ShowMemo from '../components/ShowMemo';

class TipsReceived extends React.Component {
  componentDidMount() {
    if (this.props.tips_received === false) this.props.fetchTipsReceived();
  }

  render() {
    const columns = [
      {
        title: 'From',
        dataIndex: 'from',
        key: 'from',
        render: (text) => (<Link to={`/@${text}`} style={{display: 'inline-flex'}}>
          <Avatar size={24} username={text}/>
          <span style={{marginLeft: '5px'}}>{text}</span>
        </Link>),
      },
      {
        title: 'Date',
        dataIndex: 'timestamp',
        key: 'timestamp',
        render: (text) => (<FormattedRelative value={`${text}Z`}/>)
      },
      {
        title: 'Amount',
        dataIndex: 'amount',
        key: 'amount',
      },
      {
        title: '',
        dataIndex: 'memo',
        key: 'memo',
        render: (text) => ( <ShowMemo user={this.props.user.name} memo={text} icon={true} /> )
      },
      {
        title: '',
        dataIndex: 'link',
        key: 'link',
        render: (text) => (text ? <Link to={text}><Icon type="link" /></Link> : null),
      },
    ];

    let data = [];
    if (this.props.tips_received) {
      data = this.props.tips_received.map((item) => {
          const [idx, obj] = item;
          const {timestamp, op} = obj;
          const [op_type, op_obj] = op;
          let from = null;
          let amount = null;
          let link = null;
          let memo = null;
          if (op_type === 'social_action') {
            from = op_obj.account;
            const [action_type, action_obj] = op_obj.action;
            memo = action_obj.memo;
            if (action_type === 5) { // to user
              amount = action_obj.amount;
            } else if (action_type === 6) { // to comment
              amount = action_obj.amount;
              link = `/@${action_obj.author}/${action_obj.permlink}`;
            }
          }

          return {
            id: `id_${idx}`,
            from,
            timestamp,
            amount,
            link,
            memo,
          };
        }
      );
    }

    return (
      <div>
        <ReduxInfiniteScroll
          hasMore={this.props.tips_received_hasMore}
          loadingMore={this.props.loading}
          loader={<Loading/>}
          loadMore={this.props.fetchTipsReceivedMore}
          elementIsScrollable={false}
          items={[<Table key="received-tips-list" rowKey="id" pagination={false} columns={columns} dataSource={data}/>]}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    ...ownProps,
    ...state.tips,
    ...state.auth,
  };
};
const mapDispatchToProps = {
  fetchTipsReceived,
  fetchTipsReceivedMore
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);
export default compose(
  withRouter,
  withConnect
)(TipsReceived);
