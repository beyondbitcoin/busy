import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { Link } from 'react-router-dom';
import { FormattedMessage, FormattedNumber, FormattedRelative } from 'react-intl';
import { getFromMetadata } from '../../helpers/parser';
import { getProxyImageURL } from '../../helpers/image';
import { image } from '../../vendor/steemitLinks';

const PostRecommendationLink = ({ post, navigateToPost, haveImage = true, author = false }) => {
  const images = getFromMetadata(post.json_metadata, 'image');
  const firstImage = _.head(images);
  let imagePath = '';

  if (images && firstImage) {
    imagePath = getProxyImageURL(true, post.author, post.permlink, firstImage, 'small');
  } else {
    const bodyImg = post.body.match(image());
    if (bodyImg && bodyImg.length) {
      imagePath = getProxyImageURL(true, post.author, post.permlink, bodyImg[0], 'small');
    }
  }

  return (
    <div className="PostRecommendation__link" key={post.id}>

      {haveImage && (
        <div className="PostRecommendation__link__image-container">
          <Link
            to={`/${post.category}/@${post.author}/${post.permlink}`}
            onClick={() => navigateToPost(post.author)}
            className="PostRecommendation__link__post-title"
          >
            <img alt="" src={imagePath} className="PostRecommendation__link__image" onError={(e) => {e.target.onError=null;e.target.src="/images/no-image-found.jpg";}} />
          </Link>
        </div>
      )}
      <div>
        <div className="PostRecommendation__link__header">
          <Link
            to={`/@${post.author}/${post.permlink}`}
            onClick={() => navigateToPost(post.author)}
            className="PostRecommendation__link__post-title"
          >
            {post.title}
          </Link>
        </div>
      </div>
    </div>
  );
};

PostRecommendationLink.propTypes = {
  post: PropTypes.shape(),
  navigateToPost: PropTypes.func,
  haveImage: PropTypes.bool,
  author: PropTypes.bool,
};

PostRecommendationLink.defaultProps = {
  post: {},
  navigateToPost: () => {},
  haveImage: true,
  author: false,
};

export default PostRecommendationLink;
