import _ from 'lodash';
import PropTypes from 'prop-types';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Alert, Icon, Select } from 'antd';
import Scroll from 'react-scroll';
import { connect } from "react-redux";
import { compose } from 'redux';
import withEditor from '../Editor/withEditor';
import EditorInput from '../Editor/EditorInput';
import Body, { remarkable } from '../Story/Body';
import Avatar from '../Avatar';
import './CommentForm.less';

const Element = Scroll.Element;
const { Option } = Select;

@withEditor
class CommentForm extends React.Component {
  static propTypes = {
    inputValue      : PropTypes.string.isRequired,
    isLoading       : PropTypes.bool,
    isSmall         : PropTypes.bool,
    onImageInvalid  : PropTypes.func,
    onImageUpload   : PropTypes.func,
    onSubmit        : PropTypes.func,
    parentCallback  : PropTypes.func,
    parentPost      : PropTypes.shape().isRequired,
    submitted       : PropTypes.bool,
    top             : PropTypes.bool,
    username        : PropTypes.string.isRequired,
  };

  static defaultProps = {
    inputValue      : '',
    isLoading       : false,
    isSmall         : false,
    onImageInvalid  : () => {},
    onImageUpload   : () => {},
    onSubmit        : () => {},
    parentCallback  : () => {},
    submitted       : false,
    top             : false,
  };

  constructor(props) {
    super(props);

    this.state = {
      body              : '',
      bodyHTML          : '',
      isDisabledSubmit  : false,
      storage           : '',
    };

    this.handleBodyUpdate = this.handleBodyUpdate.bind(this);
    this.handleSubmit     = this.handleSubmit.bind(this);
    this.setBodyAndRender = this.setBodyAndRender.bind(this);
    this.setInput         = this.setInput.bind(this);
  }

  componentDidMount() {
    if (this.input && !this.props.top) {
      this.input.focus();
    }
  }

  componentWillReceiveProps(nextProps) {
    if ((!nextProps.isLoading && nextProps.inputValue !== '') || nextProps.submitted) {
      this.setBodyAndRender(nextProps.inputValue);
    }
  }

  setInput(input) {
    this.input = input;
  }

  setBodyAndRender(body) {
    this.setState(
      {
        body,
        bodyHTML: remarkable.render(body),
      },
      () => {
        if (this.input) {
          this.input.value = body;
        }
      },
    );
  }

  handleBodyUpdate(body) {
    this.setBodyAndRender(body);
  }

  handleStorageChange = (value) => {
    this.setState({ storage: value });
  };

  handleSubmit(e) {
    e.stopPropagation();
    this.setState({ isDisabledSubmit: true });
    if (this.state.body) {
      this.props.onSubmit(this.props.parentPost, this.state.body, this.state.storage).then(response => {
        if (!_.get(response, 'error', false)) {
          this.setBodyAndRender('');
          this.input.style.height = "100px";
        }
      }).catch(() => {
        this.setState({ isDisabledSubmit: false });
      });
    }
  }

  render() {
    const { parentPost, isRestricted, username, isSmall, isLoading } = this.props;
    const { body, storage, bodyHTML } = this.state;
    const buttonClass = isLoading ? 'CommentForm__button_disabled' : 'CommentForm__button_primary';

    if (isRestricted) {
      let restrictedMessage = null;

      if (_.get(parentPost, "allow_friends", false)) {
        restrictedMessage = <span>Tips: Everyone. Comments: Friends Only. <br />
                            Send a Friend Request to join the conversation.</span>;
      } else {
        restrictedMessage = <span>Tips: Everyone. Comments: Pod Members Only. <br />
                            Send a Join Request to collaborate with the members of this pod.</span>;
      }

      return (
        <div className="CommentForm">
          <Alert
            message={restrictedMessage}
            type="info"
            showIcon
            style={{width: '100%'}}
          />
        </div>
      );
    }

    return (
      <div className="CommentForm">
        <Avatar username={username} size={!isSmall ? 40 : 32} />
        <div className="CommentForm__text">
          <Element name="commentFormInputScrollerElement">
            <EditorInput
              inputId         = {`${this.props.parentPost.id}-comment-inputfile`}
              inputRef        = {this.setInput}
              onChange        = {this.handleBodyUpdate}
              onImageInvalid  = {this.props.onImageInvalid}
              onImageUpload   = {this.props.onImageUpload}
              value           = {body}
            />
          </Element>
          <div>
            <Select style={{ width: 240 }} size="small" defaultValue={storage} onChange={this.handleStorageChange}>
              <Option value={''}>Onchain</Option>
              <Option value='cnc'>Content Network</Option>
            </Select>
          </div>
          <button
            className   = {`CommentForm__button ${buttonClass}`}
            disabled    = {isLoading}
            onClick     = {this.handleSubmit}
          >
            {isLoading && <Icon type="loading" />}
            {isLoading ? (
              <FormattedMessage id="comment_send_progress" defaultMessage="Commenting" />
            ) : (
              <FormattedMessage id="comment_send" defaultMessage="Comment" />
            )}
          </button>
          {bodyHTML && (
            <div className="CommentForm__preview">
              <span className="Editor__label">
                <FormattedMessage id="preview" defaultMessage="Preview" />
              </span>
              <Body body={bodyHTML} />
            </div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const {username, parentPost, parentCallback} = ownProps;
  let isRestricted = false;

  try {
    if (username) {
      const {author, allow_friends, pod} = parentPost;

      if (allow_friends) {
        if (author !== username) {
          const friends = _.get(state, 'friends.friends', []);
          const isFriend = _.includes(friends, author);
          isRestricted = !isFriend;
        }
      } else if (pod) {
        const pods = _.get(state, 'auth.user.pods', []);
        const isPodMember = _.includes(pods, pod);
        isRestricted = !isPodMember;
      }

      parentCallback(isRestricted);
    }
  } catch (e) {
    // do nothing
  }

  return {
    ...ownProps,
    isRestricted,
  };
};
const mapDispatchToProps = {};
const withConnect = connect(mapStateToProps, mapDispatchToProps);
export default compose(
  withConnect,
)(CommentForm);
