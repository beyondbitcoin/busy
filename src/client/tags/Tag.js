import React from 'react';
import PropTypes from 'prop-types';
import { FormattedNumber } from 'react-intl';
import { Link } from 'react-router-dom';
import WLSDisplay from '../components/Utils/WLSDisplay';

const Tag = ({ tag }) => (
  <div className="page">
    <div className="my-5 text-center">
      <h4>
        <Link to={`/created/${tag.name}`}>{tag.name}</Link>{' '}
      </h4>
      <p>
        <i className="iconfont icon-activity" /> <FormattedNumber value={tag.comments} />{' '}
        <i className="iconfont icon-collection" /> <WLSDisplay value={parseFloat(tag.total_payouts)} />
      </p>
    </div>
  </div>
);

Tag.propTypes = {
  tag: PropTypes.shape().isRequired,
};

export default Tag;
