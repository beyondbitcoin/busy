import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import _ from 'lodash';
import { Helmet } from 'react-helmet';
import sanitize from 'sanitize-html';
import { dropCategory, isBannedPost } from '../helpers/postHelpers';
import {
  getAppUrl,
  getAuthenticatedUser,
  getBookmarks,
  getFollowingList,
  getIsEditorSaving,
  getPendingBookmarks,
  getPendingFollows,
  getPendingLikes,
  getRewriteLinks,
} from '../reducers';
import { editPost } from './Write/editorActions';
import { votePost } from './postActions';
import { toggleBookmark } from '../bookmarks/bookmarksActions';
import { followUser, unfollowUser } from '../user/userActions';
import { getAvatarURL } from '../components/Avatar';
import { getHtml } from '../components/Story/Body';
import { jsonParse } from '../helpers/formatter';
import StoryFull from '../components/Story/StoryFull';
import DMCARemovedMessage from '../components/Story/DMCARemovedMessage';
import withAuthActions from '../auth/withAuthActions';
import embedjs from '../utils/embedjs';
import Promo from '../components/Promo/Promo';

@connect(
  state => ({
    user: getAuthenticatedUser(state),
    bookmarks: getBookmarks(state),
    pendingBookmarks: getPendingBookmarks(state),
    pendingLikes: getPendingLikes(state),
    followingList: getFollowingList(state),
    pendingFollows: getPendingFollows(state),
    saving: getIsEditorSaving(state),
    appUrl: getAppUrl(state),
    rewriteLinks: getRewriteLinks(state),
  }),
  {
    editPost,
    votePost,
    toggleBookmark,
    followUser,
    unfollowUser,
    push,
  },
)
@withAuthActions
class PostContent extends React.Component {
  static propTypes = {
    user: PropTypes.shape().isRequired,
    content: PropTypes.shape().isRequired,
    signature: PropTypes.string,
    pendingLikes: PropTypes.shape(),
    followingList: PropTypes.arrayOf(PropTypes.string),
    pendingFollows: PropTypes.arrayOf(PropTypes.string),
    pendingBookmarks: PropTypes.arrayOf(PropTypes.number).isRequired,
    saving: PropTypes.bool.isRequired,
    rewriteLinks: PropTypes.bool.isRequired,
    appUrl: PropTypes.string.isRequired,
    bookmarks: PropTypes.shape(),
    editPost: PropTypes.func,
    toggleBookmark: PropTypes.func,
    votePost: PropTypes.func,
    followUser: PropTypes.func,
    unfollowUser: PropTypes.func,
    push: PropTypes.func,
    onActionInitiated: PropTypes.func,
  };

  static defaultProps = {
    signature: null,
    pendingLikes: {},
    followingList: [],
    pendingFollows: [],
    bookmarks: {},
    editPost: () => {},
    toggleBookmark: () => {},
    votePost: () => {},
    followUser: () => {},
    unfollowUser: () => {},
    push: () => {},
    onActionInitiated: () => {},
  };

  componentDidMount() {
    if (typeof window === 'undefined') return;
    const { hash } = window.location;
    // PostContent renders only when content is loaded so it's good moment to scroll to comments.
    if (hash.indexOf('comments') !== -1 || /#@[a-zA-Z-.]+\/[a-zA-Z-]+/.test(hash)) {
      const el = document.getElementById('comments');
      if (el) el.scrollIntoView({ block: 'start' });
    }
  }

  handleLikeClick = (post, amount, memo, onSuccess) => {
    this.props.onActionInitiated(() => this.props.votePost(post.id, post.author, post.permlink, amount, memo, onSuccess));
  };

  handleSaveClick = post =>
    this.props.onActionInitiated(() => this.props.toggleBookmark(post.id, post.author, post.permlink));

  handleFollowClick = post => {
    const isFollowed = this.props.followingList.includes(post.author);
    if (isFollowed) {
      this.props.onActionInitiated(() => this.props.unfollowUser(post.author));
    } else {
      this.props.onActionInitiated(() => this.props.followUser(post.author));
    }
  };

  handleEditClick = post => {
    if (post.depth === 0) return this.props.editPost(post);
    this.props.push(`${post.url}-edit`);
    return Promise.resolve(null);
  };

  render() {
    const {
      user,
      content,
      signature,
      pendingLikes,
      followingList,
      pendingFollows,
      bookmarks,
      pendingBookmarks,
      saving,
      rewriteLinks,
      appUrl,
    } = this.props;

    if (isBannedPost(content)) return <DMCARemovedMessage className="center" />;

    const postMetaData = jsonParse(content.json_metadata);
    const busyHost = appUrl || 'https://whaleshares.io';
    const canonicalHost = busyHost;
    const userVote = _.find(content.comment_tips, { tipper: user.name }) || {};
    const postState = {
      isSaved: !!bookmarks[content.id],
      isLiked: userVote.tipper !== undefined,
      userFollowed: followingList.includes(content.author),
    };
    const pendingLike = pendingLikes[content.id] && (pendingLikes[content.id].amount > 0);
    const { title, category, created, author, body } = content;
    var postMetaImage = postMetaData.image && postMetaData.image[0];
    if (!postMetaImage) {
      const embeds = embedjs.getAll(body);
      postMetaImage = embeds && embeds[0] && embeds[0].thumbnail ? embeds[0].thumbnail : null;
    }
    const htmlBody = getHtml(true, author, content.permlink, body, {}, 'text');
    const bodyText = sanitize(htmlBody, { allowedTags: [] });
    const desc = `${_.truncate(bodyText, { length: 143 })} by ${author}`;
    const image = postMetaImage || '';  //getAvatarURL(author) || '/images/logo.png';
    const canonicalUrl = `${canonicalHost}${dropCategory(content.url)}`;
    const url = `${busyHost}${dropCategory(content.url)}`;
    const ampUrl = `${url}/amp`;
    const metaTitle = `${title}`;

    return (
      <div>
        <Helmet>
          <title>{title}</title>
          <link rel="canonical" href={canonicalUrl} />
          <link rel="amphtml" href={ampUrl} />
          <meta property="description" content={desc} />
          <meta property="og:title" content={metaTitle} />
          <meta property="og:type" content="article" />
          <meta property="og:url" content={url} />
          <meta property="og:image" content={image} />
          <meta property="og:description" content={desc} />
          <meta property="og:site_name" content="Whaleshares" />
          <meta property="article:tag" content={category} />
          <meta property="article:published_time" content={created} />
          <meta property="twitter:card" content={image ? 'summary_large_image' : 'summary'} />
          <meta property="twitter:site" content={'@Whaleshares_io'} />
          <meta property="twitter:title" content={metaTitle} />
          <meta property="twitter:description" content={desc} />
          <meta property="twitter:image" content={image} />
        </Helmet>
        <Promo type="post" />
        <StoryFull
          user={user}
          post={content}
          postState={postState}
          signature={signature}
          commentCount={content.children}
          pendingLike={pendingLike}
          pendingFollow={pendingFollows.includes(content.author)}
          pendingBookmark={pendingBookmarks.includes(content.id)}
          saving={saving}
          ownPost={author === user.name}
          rewriteLinks={rewriteLinks}
          onLikeClick={this.handleLikeClick}
          onSaveClick={this.handleSaveClick}
          onFollowClick={this.handleFollowClick}
          onEditClick={this.handleEditClick}
        />
      </div>
    );
  }
}

export default PostContent;
