import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';
import { Icon } from 'antd';
import { injectIntl, FormattedMessage, FormattedNumber } from 'react-intl';
import Reward from 'react-rewards';
import withAuthActions from '../auth/withAuthActions';
import { getAuthenticatedUser } from '../reducers';
import { claimRewards } from './walletActions';
import { reload } from '../auth/authActions';
import { notify } from '../app/Notification/notificationActions';
import Action from '../components/Button/Action';
import './ClaimRewardsBlock.less';
import IconTips from '../../../assets/images/svgs/IconTips.svg';
import '../components/Sidebar/SidebarContentBlock.less';

@injectIntl
@connect(
  state => ({
    user: getAuthenticatedUser(state),
  }),
  {
    // getUserAccountHistory,
    reload,
    claimRewards,
    notify,
  },
)
@withAuthActions
class ClaimRewardsBlock extends Component {
  static propTypes = {
    user: PropTypes.shape(),
    intl: PropTypes.shape().isRequired,
    // getUserAccountHistory: PropTypes.func.isRequired,
    reload: PropTypes.func.isRequired,
    claimRewards: PropTypes.func.isRequired,
    notify: PropTypes.func.isRequired,
    onActionInitiated: PropTypes.func.isRequired,
  };

  static defaultProps = {
    user: {},
  };

  state = {
    loading: false,
    rewardClaimed: false,
  };

  handleClaimRewards = () => {
    this.props.onActionInitiated(() => {
      this.claimRewardsBalance();
    });
  };

  claimRewardsBalance = () => {
    const { user } = this.props;
    const {
      name,
      reward_vesting_balance: vestingBalance,
    } = user;
    this.setState({
      loading: true,
    });
    this.props.claimRewards(vestingBalance).then(() => {
      this.setState({
        loading: false,
        rewardClaimed: true,
      });

      // this.props.getUserAccountHistory(name).then(() => this.props.reload());
      this.props.reload();

      this.props.notify(
        this.props.intl.formatMessage({
          id: 'claimed',
          defaultMessage: 'Claimed'
        }),
        'success'
      );

      this.reward.rewardMe();
    }).catch(() => {
      this.setState({
        loading: false,
        rewardClaimed: false,
      });
      this.props.notify(
        this.props.intl.formatMessage({
          id: 'error_could_not_claim',
          defaultMessage: 'Oops! Could not claim, try again later'
        }),
        'error'
      );
    });
  };

  renderReward = (value, currency, rewardField) => (
    <div className="ClaimRewardsBlock__reward">
      <span className="ClaimRewardsBlock__reward__field">
        <FormattedMessage
          id={rewardField}
          defaultMessage={_.startCase(rewardField.replace('_', ''))}
        />
      </span>
      <span className="ClaimRewardsBlock__reward__value">
        <FormattedNumber value={value} minimumFractionDigits={3} maximumFractionDigits={3} />
        {` ${currency}`}
      </span>
    </div>
  );

  render() {
    const { user, intl } = this.props;
    const { rewardClaimed } = this.state;
    const rewardSP = parseFloat(user.reward_vesting_steem);
    const userHasRewards = rewardSP > 0;

    const buttonText = rewardClaimed
      ? intl.formatMessage({
          id: 'tips_claimed',
          defaultMessage: 'Tips Claimed',
        })
      : intl.formatMessage({
          id: 'claim_tips',
          defaultMessage: 'Claim Tips to Whalestake',
        });

    // comment this line for animation (could not animation on removed component)
    // if (!userHasRewards || rewardClaimed) return null;

    return (
      <div className="SidebarContentBlock ClaimRewardsBlock">
        <h4 className="SidebarContentBlock__title">
          <Icon component={IconTips} className="iconfont icon-rewards" />
          <FormattedMessage id="tips_to_claim" defaultMessage="Tips to Claim" />
        </h4>
        <div className="SidebarContentBlock__content">
          <div>
            {this.renderReward(rewardSP, '', 'steem_power')}
          </div>
          <div style={{pointerEvents: 'none'}}>
            <Reward
              ref={(ref) => {
                this.reward = ref
              }}
              type='emoji'
              springAnimation={false}
            >
              <div/>
            </Reward>
          </div>
          {!(!userHasRewards || rewardClaimed) && (
            <Action
              text={buttonText}
              disabled={rewardClaimed}
              onClick={this.handleClaimRewards}
              loading={this.state.loading}
              primary
            />
          )}
        </div>
      </div>
    );
  }
}

export default ClaimRewardsBlock;
