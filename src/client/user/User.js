import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { renderRoutes } from 'react-router-config';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import { Tag, Icon } from 'antd';
import { injectIntl, FormattedMessage } from 'react-intl';
import faMapMarkerAlt from '../../../assets/images/svgs/faMapMarkerAlt.svg';
import faLink from '../../../assets/images/svgs/faLink.svg';
import faUserAlt from '../../../assets/images/svgs/faUserAlt.svg';
import UserHero, { UserMenuWrapper } from './UserHero';
import {
  getIsAuthenticated,
  getAuthenticatedUser,
  getUser,
  getIsUserFailed,
  getIsUserLoaded,
} from '../reducers';
// import { getAccount } from './usersActions';
import { getAvatarURL } from '../components/Avatar';
import SidenavMain from '../components/Navigation/SidenavMain';
import RightSidebar from '../app/Sidebar/RightSidebar';
import Affix from '../components/Utils/Affix';
import ScrollToTop from "../components/Utils/ScrollToTop";
import ScrollToTopOnMount from '../components/Utils/ScrollToTopOnMount';
import SocialLinks from '../components/SocialLinks';
import AvatarLightbox from '../components/AvatarLightbox';
import UserBoxButton from '../widgets/UserBoxButton';
import { urlWithoutWWW } from '../helpers/regexHelpers';
import ErrorFetching from '../statics/ErrorFetching';
import ErrorUserNotRegistered from '../statics/ErrorUserNotRegistered';
import Promo from '../components/Promo/Promo';
import './UserBox.less';

@injectIntl
@connect(
  (state, ownProps) => ({
    authenticated     : getIsAuthenticated(state),
    authenticatedUser : getAuthenticatedUser(state),
    user              : getUser(state, ownProps.match.params.name),
    loaded            : getIsUserLoaded(state, ownProps.match.params.name),
    failed            : getIsUserFailed(state, ownProps.match.params.name),
  }),
  {
    // getAccount,
  },
)
class User extends React.Component {
  static propTypes = {
    route             : PropTypes.shape().isRequired,
    authenticated     : PropTypes.bool.isRequired,
    authenticatedUser : PropTypes.shape().isRequired,
    intl              : PropTypes.shape().isRequired,
    match             : PropTypes.shape().isRequired,
    user              : PropTypes.shape().isRequired,
    loaded            : PropTypes.bool,
    failed            : PropTypes.bool,
    fetching          : PropTypes.bool,
    // getAccount: PropTypes.func,
  };

  static defaultProps = {
    authenticatedUserName : '',
    loaded                : false,
    failed                : false,
    fetching              : false,
    // getAccount: () => {},
  };

  state = {
    popoverVisible  : false,
    isFollowing     : false,
    fail_count      : 0,
  };

  // componentDidMount() {
  //   const { user } = this.props;
  //   if (!user.id && !user.failed) {
  //     this.props.getAccount(this.props.match.params.name);
  //   }
  // }
  //
  // componentDidUpdate(prevProps) {
  //   if (prevProps.match.params.name !== this.props.match.params.name) {
  //     this.props.getAccount(this.props.match.params.name);
  //   }
  // }

  handleVisibleChange = visible => {
    this.setState({ popoverVisible: visible });
  };

  pageLoadFailed = count => {
    const counted = this.state.fail_count + count;
    if (counted < 4) {
      this.setState({ fail_count: counted });
      return null;
    }
    return <ErrorFetching />;
  };

  numberWithCommas = x => x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');

  render() {
    const { authenticated, authenticatedUser, loaded, failed, match, fetching, user } = this.props;
    const { isFollowing } = this.state;
    this.pageLoadFailed = this.pageLoadFailed.bind(this);
    const username = match.params.name;

    if (failed && !fetching) {
      const pageLoaded = this.pageLoadFailed(1);
      if (pageLoaded !== null) {
        if (user.created === undefined) {
          return <ErrorUserNotRegistered />;
        }
        return pageLoaded;
      }
    }

    const { profile         = {} } = user.json_metadata || {};
    const busyHost          = global.postOrigin || 'https://whaleshares.io';
    const busyName          = 'whaleshares.io';
    const desc              = profile.about || `Posts by ${username}`;
    const image             = getAvatarURL(username) || '/images/logo.png';
    const url               = `${busyHost}/@${username}`;
    const displayedUsername = profile.name || username || '';
    const hasCover          = !!profile.cover_image;
    const title             = `${displayedUsername} - Whaleshares`;
    const isSameUser        = authenticated && authenticatedUser.name === username;

    let locationUI = null;
    if (profile.location) {
      locationUI = (
        <div>
          <a href='#' title={`Location: ${profile.location}`}>
            <Icon component={faMapMarkerAlt}/>
          </a>
        </div>
      );
    }

    let websiteUI = null;
    if (profile.website) {
      websiteUI = (
        <div>
          <a title="Website Link" target="_blank" rel="noopener noreferrer" href={profile.website} >
            <Icon type="global" />
          </a>
        </div>
      );
    }

    let tags = [];
    if (profile.tags && _.isArray(profile.tags)) {
      tags = profile.tags.map(topic => (
        <Link key={topic} to={`/discover_people/${topic}`}>
          <Tag>{topic}</Tag>
        </Link>
      ));
    }

    return (
      <div className="main-panel">
        <Helmet>
          <title>{title}</title>
          <link rel="canonical" href={url} />
          <meta property="description" content={desc} />

          <meta property="og:title" content={title} />
          <meta property="og:type" content="article" />
          <meta property="og:url" content={url} />
          <meta property="og:image" content={image} />
          <meta property="og:description" content={desc} />
          <meta property="og:site_name" content="Whaleshares" />

          <meta property="twitter:card" content={image ? 'summary_large_image' : 'summary'} />
          <meta property="twitter:site" content={'@Whaleshares_io'} />
          <meta property="twitter:title" content={title} />
          <meta property="twitter:description" content={desc} />
          <meta property="twitter:image" content={image || 'https://whaleshares.io/images/wls-share.png'} />
        </Helmet>
        <ScrollToTop />
        <ScrollToTopOnMount />
        {user && (
          <UserHero
            authenticated       = {authenticated}
            user                = {user}
            username            = {displayedUsername}
            isSameUser          = {isSameUser}
            coverImage          = {profile.cover_image}
            isFollowing         = {isFollowing}
            hasCover            = {hasCover}
            onFollowClick       = {this.handleFollowClick}
            isPopoverVisible    = {this.state.popoverVisible}
            handleVisibleChange = {this.handleVisibleChange}
          />
        )}
        <div className="shifted">
          <div className="layout-twocol container">
            <Affix className="leftContainer leftContainer__user" stickPosition={72}>
              <div className="left">
                <SidenavMain friends={this.props.friends} />
              </div>
            </Affix>
            <div className="center main-body">

              {loaded && (
                <div className="UserBox">
                  <div className="UserBox__main">
                    <div className="UserBox__row first">
                      <div className="UserBox__avatar">
                        <AvatarLightbox username={username} size={150} />
                      </div>
                      <div className="UserBox__info">
                        <h2>{displayedUsername}</h2>
                        <div className="UserBox__joined">
                          <FormattedMessage
                            id              ="joined_date_profile"
                            defaultMessage  ="@{username} joined Whaleshares at {date}"
                            values          ={{
                                                date: this.props.intl.formatDate(user.created, {
                                                  year  : 'numeric',
                                                  month : 'short',
                                                  day   : 'numeric',
                                                }),
                                                username : username,
                                              }}
                          />
                        </div>
                        <div className="UserBox__description">{profile.about}</div>
                        {tags.length > 0 && (<div className="UserBox_interests">
                          <FormattedMessage
                            id              = "posts_about"
                            defaultMessage  = "Posts about: "
                            />{tags}
                            </div>)}
                      </div>
                    </div>
                    <div className="UserBox__row last">
                    </div>
                    <div className='UserBox__socialLinks'>
                      <SocialLinks profile={profile} />
                      {websiteUI}
                      {locationUI}
                    </div>
                    <div className="UserBox__edit"><UserBoxButton username={user.name} /></div>
                  </div>
                  <div className="UserBox__stats">
                    <div className="UserBox__statbox">
                      <span className="value">{user.post_count}</span>{' '}
                      <span className="param">
                      {this.props.intl.formatMessage({ id: 'posts', defaultMessage: 'Posts' })}
                    </span>
                    </div>
                    <div className="UserBox__statbox">
                      <span className="value">{user.follower_count}</span>
                      <span className="param">
                        {this.props.intl.formatMessage({id: 'followers', defaultMessage: 'Followers'})}
                      </span>
                    </div>
                    <div className="UserBox__statbox">
                    <span className="value">
                      {this.numberWithCommas(parseFloat(this.props.user.vesting_shares).toFixed(0))}
                    </span>
                      <span className="param">
                      {this.props.intl.formatMessage({ id: 'whalestake', defaultMessage: 'Whalestake' })}
                    </span>
                    </div>
                  </div>
                </div>
              )}
              <UserMenuWrapper ownProfile={isSameUser} />
              <div className="right-sidebar-layout">
                {loaded && (
                  <>
                    <RightSidebar key={user.name} />
                    <div className="main-feed">
                      <Promo type="profile" />
                      {renderRoutes(this.props.route.routes)}
                    </div>
                  </>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default User;
