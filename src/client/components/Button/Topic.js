import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Icon } from 'antd';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import BTooltip from '../BTooltip';
import './Topic.less';

class Topic extends React.Component {
  static propTypes = {
    name: PropTypes.string,
    favorite: PropTypes.bool,
    closable: PropTypes.bool,
  };

  static defaultProps = {
    name: '',
    favorite: false,
    closable: false,
  };

  feedButtonType = () => {
    const { name, favorite, closable } = this.props;

    if (!closable) {
      return (
          <Link
            className={classNames('Interest', {
              'Topic--favorite': favorite,
            })}
            to={`/created/${name}`}
          >
            {name}
          </Link>
        );
    }

    return (
      <FormattedMessage id="dismiss_filter" defaultMessage="Dismiss Filter">
        { tipTool => 
          <BTooltip title={tipTool}>
            <Link
              role="presentation"
              className="Interest"
              to={'/created/'}
            >
              {name}{' '}<Icon type="filter"/>
            </Link>
          </BTooltip>
        } 
      </FormattedMessage>
      );

  }

  render() {
    const button = this.feedButtonType();

    return (
      <>
        {button}
      </>
    );
  }
}

export default Topic;
