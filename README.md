# Whaleshares Frontend v2

[![pipeline status](https://gitlab.com/beyondbitcoin/busy/badges/master/pipeline.svg)](https://gitlab.com/beyondbitcoin/busy/commits/master)
[![coverage report](https://gitlab.com/beyondbitcoin/busy/badges/master/coverage.svg)](https://gitlab.com/beyondbitcoin/busy/commits/master)


![Preview](https://gitlab.com/beyondbitcoin/busy/wikis/uploads/3bb03b455515556764c56d3691b6d9c9/Screen_Shot_2019-04-04_at_4.04.14_PM.png)

## Quick Setup

```
git clone https://gitlab.com/beyondbitcoin/busy.git
cd busy

yarn
yarn dev
```

