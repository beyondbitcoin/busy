import _ from 'lodash';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import Story from '../components/Story/Story';
import {
  getAuthenticatedUser,
  getPosts,
  getBookmarks,
  getPendingBookmarks,
  getPendingLikes,
  getFollowingList,
  getPendingFollows,
  getIsEditorSaving,
  getShowNSFWPosts,
  getCommentsList,
  getMutedList,
} from '../reducers';
import { votePost } from '../post/postActions';
import { toggleBookmark } from '../bookmarks/bookmarksActions';
import { editPost } from '../post/Write/editorActions';
import { followUser, unfollowUser } from '../user/userActions';

const mapStateToProps = (state, { id, feedType }) => {
  const user = getAuthenticatedUser(state);
  let post;

  switch (feedType) {
    case 'comments':
      post = getCommentsList(state)[id];
      break;
    default:
      post = getPosts(state)[id];
      break;
  }

  const isMuted = _.includes(getMutedList(state), post.author);
  const userVote = _.find(post.comment_tips, { tipper: user.name }) || {};
  const postState = {
    isSaved: !!getBookmarks(state)[post.id],
    isLiked: userVote && userVote.tipper != null,
    userFollowed: _.includes(getFollowingList(state), post.author),
    isFriend: _.includes(state.friends.friends, post.author),
  };
  const pendingVote = getPendingLikes(state)[post.id];
  const pendingLike = pendingVote && (pendingVote.amount > 0);

  return {
    user,
    post,
    postState,
    pendingLike,
    isMuted,
    pendingFollow: getPendingFollows(state).includes(post.author),
    pendingBookmark: getPendingBookmarks(state).includes(post.id),
    saving: getIsEditorSaving(state),
    ownPost: user.name === post.author,
    showNSFWPosts: getShowNSFWPosts(state),
  };
};

export default connect(
  mapStateToProps,
  {
    votePost,
    toggleBookmark,
    editPost,
    followUser,
    unfollowUser,
    push,
  },
)(Story);
