import _ from 'lodash';

export const getFollowingUpvotes = (activeVotes, following) =>
  activeVotes.filter(vote => _.includes(following, vote.tipper));

export const sortVotes = (votes, sortBy) => votes.sort((a, b) => a[sortBy] - b[sortBy]);
