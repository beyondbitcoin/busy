import React, { Component, Fragment } from 'react';
import SidebarBlock from './SidebarBlock';

let reminderList = new Map();
reminderList.set(1,'You can trade WLS on Ionomy and Hive-Engine.');
reminderList.set(2,'The more Whalestake you hold, the higher your Daily Reward will be.');
reminderList.set(3,'Witnesses keep this platform going. You can support them by casting votes in the Wallet section.');
reminderList.set(4,'Whaleshares is an opensource project. Programmers are invited to fork the code from Gitlab and help improve it.');
reminderList.set(5,'Whaleshares is more about engagement than any else.');
reminderList.set(6,'You can fill your Tipbalance using (part of) your Daily Reward.');
reminderList.set(7,'You are likely to get more support when your Shares-tab shows you are supportive as well.');
reminderList.set(8,'Using related predefined interest tags for your posts, increases exposure to an interested audience.');
reminderList.set(9,'Don\'t worry if you can only give small rewards. It\'s the intent that counts!');
reminderList.set(10,'Make replying to comments, users left on your posts, part of your posting routine.');
reminderList.set(11,'Adding a story to a picture increases support.');
reminderList.set(12,'Can\'t post to friends restricted posts? Send a friend request to the author of the post.');
reminderList.set(13,'Can\'t post to podmembers restricted posts? Send a pod join request from the Wallet section.');
reminderList.set(14,'You can set a mood emojicon on post creation.');
reminderList.set(15,'Supporting a friend by sending them (part of) your Daily Reward, instantly increases their DaBa.');
reminderList.set(16,'Got Stuck? Put your question in a post or comment, or join Discord.');
reminderList.set(17,'Out of DaBa? <ul><li>Shorten your text,</li><li>Use the Content Network,</li><li>Buy WLS</li></ul>');
reminderList.set(18,'<ul><li>Never Share Your Keys,</li><li>Never Share Your Keys,</li><li>Never Share Your Keys.</li></ul>');
reminderList.set(19,'Save your keys in multiple places. Preferably offline in different formats.');
reminderList.set(20,'Set an avatar for your account. It will help other users to recognize and remember you.');
reminderList.set(21,'DaBa stands for Daily Bandwidth. It represents the amount of data you are allowed to store in the blockchain.');
reminderList.set(22,'Your DaBa (Daily Bandwidth) regenerates, 20% per 24 hours.');
reminderList.set(23,'You can reward a user\'s account as well as their posts and comments.');
reminderList.set(24,'You can encrypt a memo by making # the first character. This memo can then only be read by the receiver.');
reminderList.set(25,'The whaleshares tag should only be used for posts ABOUT the Whaleshares Platform or Community.');
reminderList.set(26,'Use the introduceyourself tag on your introduction post (only).');
reminderList.set(27,'Be the supportive user you want others to be.');
reminderList.set(28,'Participate in contests and challenges to earn some additional WLS.');
reminderList.set(29,'Invite your friends to sign up using Twitter.');
reminderList.set(30,'Use the WhaleVault browser extention to safely use your keys online without the need to enter them.');
reminderList.set(31,'Use the markup tools to make your posts easier to read.');
reminderList.set(32,'Supply a link to the source page (not Google search), in case you use images that are not your own.');
reminderList.set(33,'When posts are restricted to friends or podmembers, you can still reward the post and add a message in the memo.');
reminderList.set(34,'Use the Discover People option to find users who write about a topic you are interested in.');
reminderList.set(35,'Set the topics you write about, in your profile settings. This will make it easier for people who are interested in those topics to find you.');
reminderList.set(36,'Don\'t consider yourself a writer? You can still support the Whaleshares project by becoming a witness. We\'ll be happy to help you set up your server.');
reminderList.set(37,'Join a Pod to collaborate with new friends over Shared Interests');
reminderList.set(38,'You risk losing support when setting unrelated predefined tags for your post. For example, \'art\' tag for a soccer post');
reminderList.set(39,'Be Yourself and Have Fun.');
reminderList.set(40,'Brave enough to try an alternative Whaleshares front end? Take a look at wls.social');

const Reminders = () => {
  	const key      = Math.floor(Math.random() * reminderList.size) + 1;
    const content  = reminderList.get(key);

    return (
      <SidebarBlock 
        title   = "Did You Know?"
        icon    = "bulb"
        content = <div dangerouslySetInnerHTML={{__html:content}} /> 
      />

    );
};

export default Reminders;
