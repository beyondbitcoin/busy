import React from 'react';
import PropTypes from 'prop-types';
import { Button, Menu, Dropdown } from 'antd';
import { SIGNUP_URL } from '../../common/constants/settings';

const TWITSIGNUP_URL = "https://sharebits.io/wls_signup.html";

const signup_menu = (
  <Menu>
    <Menu.Item>
      <a target="_blank" rel="noopener noreferrer" href={TWITSIGNUP_URL}>
        Signup via Twitter (nearly instant!)
      </a>
    </Menu.Item>
    <Menu.Item>
      <a rel="noopener noreferrer" href={SIGNUP_URL}>
        Signup via Email (2-3 day turnaround)
      </a>
    </Menu.Item>
  </Menu>
);

const RenderText = (props) => {
  if (props.props.text == '') return null;
  if (props.props.button) return (
    <Button type="primary" size="large">{props.props.text}</Button>
  )
  return (<span>{props.props.text}</span>);
}

const SignupButton = (props) => (
  <Dropdown overlay={signup_menu}>
    <a href={TWITSIGNUP_URL} className="ant-dropdown-link" onClick={e => { window.open(TWITSIGNUP_URL); e.preventDefault(); }}>
      <RenderText props={props} />
      {props.children}
    </a>
  </Dropdown>
);

SignupButton.propTypes = {
  text: PropTypes.string,
  button: PropTypes.bool,
};

SignupButton.defaultProps = {
  text: 'Sign Up',
  button: true,
};

export default SignupButton;
