import React from 'react';

const SvgIconWallet = props => (
  <svg id="icon-wallet_svg__wallet" viewBox="0 0 958 702.533" {...props}>
    <defs>
      <style>{'.icon-wallet_svg__cls-1{fill:#b9bff2}'}</style>
    </defs>
    <path
      className="icon-wallet_svg__cls-1"
      d="M893.2 163.733H126.8c-52.69 0-95.8 43.11-95.8 95.8v510.934c0 52.69 43.11 95.8 95.8 95.8h766.4c52.69 0 95.8-43.11 95.8-95.8V259.533c0-52.69-43.11-95.8-95.8-95.8zm0 638.667H126.8a32.027 32.027 0 0 1-31.933-31.933V259.533A32.027 32.027 0 0 1 126.8 227.6h766.4a32.027 32.027 0 0 1 31.933 31.933v95.8h-287.4a159.667 159.667 0 0 0 0 319.334h287.4v95.8A32.027 32.027 0 0 1 893.2 802.4zm31.933-191.6h-287.4c-52.69 0-95.8-43.11-95.8-95.8s43.11-95.8 95.8-95.8h287.4z"
      transform="translate(-31 -163.733)"
    />
    <circle className="icon-wallet_svg__cls-1" cx={622.7} cy={351.267} r={47.9} />
  </svg>
);

export default SvgIconWallet;
