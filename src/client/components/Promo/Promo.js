/* eslint-disable react/prop-types */
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Icon } from "antd";
import faCommentAltDollar from '../../../../assets/images/svgs/faCommentAltDollar.svg';
import './Promo.less';
import fetch from 'isomorphic-fetch';
import { WLS_IMG_PROXY } from '../../../common/constants/settings';

class Promo extends React.Component {
  constructor(props) {
    super(props);
      this.state = {
        ad: null,
        isLoaded: false
      }
    }

  componentDidMount(){
      fetch('https://whaleshares.io/promoapi?pos='+this.props.type)
          .then((res) => res.json())
          .then((json) => {
              this.setState({
                  ad: json.result,
                  isLoaded: true
              });
          })
  }

 render() {

  const { isLoaded, ad } = this.state;
  const props = this.props;

  if (!isLoaded || ad == null) return <div></div>;

  const promoOn = true; // Use true/false to switch on/off ad on Status page

  let name = ad.name;    // sponsor username
  let title = ad.title;   // sponsored post title
  let text = ad.text;    // sponsored post lead
  let image = ad.image;   // sponsored image for top spot
  let more = ad.more;    // sponsored post url
  let classes = ''; // ad classes depending on spot
  let promoId = ''; // sponsored post rubric's text id

  switch ( props.type ) {
    case 'profile':
    case 'post':
    case 'top':
    case 'feed':
      classes = 'main-content Promo PromoTop';
      promoId = 'promo_title_top';
      break;
    case 'aside':
    case 'side':
      classes = 'Promo PromoAside';
      promoId = 'promo_rubric_aside';
      break; 
    default:
      break;
  }

  if (props.type == 'post') return (
    promoOn && (
      <div className={classes}>
        <div className="promo-verbal">
          <div className="promo-header">
            <Icon component={faCommentAltDollar}/>
            <span className="promo-title">
              <b><FormattedMessage id={promoId} defaultMessage="Promotion" /></b>
            </span>
          </div>
        </div>
        <div style={{marginTop:"0px",marginBottom:"-15px",marginLeft:"65px",overflow:"hidden"}}>
          <iframe data-aa="2103340" loading="lazy" src="//ad.a-ads.com/2103340?size=468x60" style={{width:'468px', height:'60px'}}></iframe>
        </div>
      </div>
  ))

  if (props.type == 'profile') return (
    promoOn && (
      <div className={classes} style={{marginTop:"-29px",marginBottom:"-5px",marginLeft:"-15px"}} >
        <div className="promo-verbal">
          <div className="promo-header">
            <Icon component={faCommentAltDollar}/>
            <span className="promo-title">
              <b><FormattedMessage id={promoId} defaultMessage="Promotion" /></b>
            </span>
          </div>
        </div>
        <div style={{marginTop:"-15px",marginBottom:"-15px",overflow:"hidden"}}>
          <iframe data-aa="2103340" loading="lazy" src="//ad.a-ads.com/2103340?size=468x60" style={{width:'468px', height:'60px'}}></iframe>
        </div>
      </div>
  ))

  return (
    promoOn && (
      <div className={classes}>
        <div className="promo-verbal">
          <div className="promo-header">
            <Icon component={faCommentAltDollar}/>
            <span className="promo-title">
              <b><FormattedMessage id={promoId} defaultMessage="Promotion" /></b>
            </span>
          </div>
          <div className="promo-content">
            <h3><a className="promo-sponsor" href={`/@${name}`}>@{name}:</a> {title}</h3>
            <p>{text}</p><br/>
            <a className="more" href={`/@${name}/${more}`}>
              <FormattedMessage id="read" defaultMessage="Read" />
            </a>
          </div>
        </div>
        { image !== "" && ['top','feed'].includes(props.type) ? (
          <div className="promo-visual"> {/* top spot only */}
            <img src={`${WLS_IMG_PROXY}/800x600/${image}`} alt="pic" />
          </div>
        ) : ('') }
        { ['top','feed'].includes(props.type) ? (
          <div>
            <div>
              <p><i>Add your own <b>week-long promotion</b> to the rotation by sending or tipping <b>10 WLS or more</b> to <b>null</b> with post link as memo!</i></p>
            </div>
            <div style={{marginTop:"10px",marginBottom:"-15px",overflow:"hidden"}}>
             <iframe data-aa="2103340" loading="lazy" src="//ad.a-ads.com/2103340?size=468x60" style={{width:'468px', height:'60px'}}></iframe>
            </div>
          </div>
        ) : (
          <div style={{marginLeft:"25px",overflow:"hidden"}}>
            <iframe data-aa="2103966" loading="lazy" src="//ad.a-ads.com/2103966?size=200x200" style={{width:'200px', height:'200px'}}></iframe>
          </div>
        ) }
      </div>
    )
  )
 }

}

export default Promo;
