// eslint-disable-next-line import/no-extraneous-dependencies
import '@babel/polyfill';
import '@babel/polyfill/node_modules/core-js/fn/array/fill';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import Logger from 'js-logger';
import { AppContainer } from 'react-hot-loader';
import { message } from 'antd';
import TagManager from 'react-gtm-module'
import { history } from './routes';
import getStore from './store';
import { loadTranslations } from './translations';
import AppHost from './AppHost';

Logger.useDefaults();

if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('/service-worker.js');
}

const store = getStore();

const tagManagerArgs = {
  gtmId: 'GTM-PC2J26S'
}

message.config({
  top: 62,
  duration: 5,
});

TagManager.initialize(tagManagerArgs);

const render = async Component => {
  await loadTranslations(store);

  ReactDOM.hydrate(
    <Provider store={store}>
      {process.env.NODE_ENV !== 'production' ? (
        <AppContainer>
          <Component history={history} />
        </AppContainer>
      ) : (
        <Component history={history} />
      )}
    </Provider>,
    document.getElementById('app'),
  );
};

render(AppHost);

// Hot Module Replacement API
if (module.hot) {
  module.hot.accept('./AppHost', () => {
    render(AppHost);
  });
}
