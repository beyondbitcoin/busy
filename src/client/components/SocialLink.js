import React from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'antd';
import faTwitch from '../../../assets/images/svgs/faTwitch.svg';
import FaTwitch from '../../../assets/images/svgs/faTwitch.svg';
import faTwitterSquare from '../../../assets/images/svgs/faTwitterSquare.svg';
import faFacebookSquare from '../../../assets/images/svgs/faFacebookSquare.svg';
import faYoutubeSquare from '../../../assets/images/svgs/faYoutubeSquare.svg';
import faInstagram from '../../../assets/images/svgs/faInstagram.svg';
import faLinkedin from '../../../assets/images/svgs/faLinkedin.svg';

const SocialLink = ({ profile, url }) => {
  const icons = {
    facebook  : faFacebookSquare,
    instagram : faInstagram,
    twitch    : faTwitch,
    twitter   : faTwitterSquare,
    linkedin  : faLinkedin,
    youtube   : faYoutubeSquare,
  };

  return (
    <div>
      <a target="_blank" rel="noopener noreferrer" href={url} title={`${profile.name} Link`}>
        <Icon component={icons[profile.icon]} style={{color:profile.color}}/>
      </a>
    </div>
    );
};

SocialLink.propTypes = {
  profile: PropTypes.shape({
    id    : PropTypes.string.isRequired,
    icon  : PropTypes.string.isRequired,
    color : PropTypes.string.isRequired,
    name  : PropTypes.string.isRequired,
  }).isRequired,
  url: PropTypes.string,
};

SocialLink.defaultProps = {
  url: '',
};

export default SocialLink;
