/**
 * Whaleshare Uri Protocol (wlsuri) allows social sites and other apps to link to the wallet for requesting an action
 * from user (eg., joining pod)
 *
 * /wls_uri/action_name/base64_params
 *
 * Example:
 * /wls_uri/pod_action_join_request/eyAicG9kIjogInRlc3QxIiwgImFjdGlvbiI6IFswLCB7ICJqb2luX2ZlZSI6ICIwLjAwMCBXTFMiLCAiZmVlIjogIjAuMTAwIFdMUyIsICJtZW1vIjogIiJ9XX0=
 *
 * where base64_params is
 * { "pod": "test1", "action": [0, { "join_fee": "0.000 WLS", "fee": "0.100 WLS", "memo": ""}]}
 */

export const createPodJoinLink = (podname) => {
  const params = {pod: podname};
  const buff = new Buffer.from(JSON.stringify(params));
  const params_base64 = buff.toString('base64');
  return `/#/wls_uri/pod_action_join_request/${params_base64}`;
};


export default createPodJoinLink;
