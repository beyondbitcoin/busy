import _ from 'lodash';
import assert from 'assert';
// import Cookie from 'js-cookie';
import { Signature } from '@whaleshares/wlsjs/lib/auth/ecc';
import { push } from 'react-router-redux';
import { createAction } from 'redux-actions';
import { addDraft, removeDraft } from '../../helpers/localStorageHelpers';
import { jsonParse } from '../../helpers/formatter';
import { createPermlink, getBodyPatchIfSmaller } from '../../vendor/steemitHelpers';
import { notify } from '../../app/Notification/notificationActions';
import { POST_AUDIENCE_FRIENDS } from '../../helpers/postHelpers';
import { sleep } from '../../utils/misc';
import { getBase64JsonContent, storeCNCContent } from '../../CncApi';

export const CREATE_POST = '@editor/CREATE_POST';
export const CREATE_POST_START = '@editor/CREATE_POST_START';
export const CREATE_POST_SUCCESS = '@editor/CREATE_POST_SUCCESS';
export const CREATE_POST_ERROR = '@editor/CREATE_POST_ERROR';

export const NEW_POST = '@editor/NEW_POST';
export const newPost = createAction(NEW_POST);

export const SAVE_DRAFT = '@editor/SAVE_DRAFT';
export const SAVE_DRAFT_START = '@editor/SAVE_DRAFT_START';
export const SAVE_DRAFT_SUCCESS = '@editor/SAVE_DRAFT_SUCCESS';
export const SAVE_DRAFT_ERROR = '@editor/SAVE_DRAFT_ERROR';

export const DELETE_DRAFT = '@editor/DELETE_DRAFT';
export const DELETE_DRAFT_START = '@editor/DELETE_DRAFT_START';
export const DELETE_DRAFT_SUCCESS = '@editor/DELETE_DRAFT_SUCCESS';
export const DELETE_DRAFT_ERROR = '@editor/DELETE_DRAFT_ERROR';

export const ADD_EDITED_POST = '@editor/ADD_EDITED_POST';
export const addEditedPost = createAction(ADD_EDITED_POST);

export const DELETE_EDITED_POST = '@editor/DELETE_EDITED_POST';
export const deleteEditedPost = createAction(DELETE_EDITED_POST);

const convertImage = blob =>
  new Promise(resolve => {
    const reader = new FileReader();
    reader.addEventListener('load', () => {
      resolve(reader.result);
    });
    reader.readAsDataURL(blob);
  });

export const uploadImage = blob => (dispatch, getState, { steemAPI, Keys }) => {
  const { username, keys } = Keys.decrypt();
  return new Promise((resolve, reject) => {
    convertImage(blob).then(async data => {
      let indexData = 0;
      if (data[23] === ',') {
        indexData = 23;
      } else if (data[22] === ',') {
        indexData = 22;
      } else if (data[21] === ',') {
        indexData = 21;
      } else {
        reject(new Error('could not find index of [,]'));
      }

      let path;
      const prefix = data.substring(0, indexData);
      const b64 = data.substring(indexData + 1);
      if (/data:image\/(jpeg|jpg|png|gif)/.test(prefix) === false) reject(new Error('invalid content type'));
      const buffer = Buffer.from(b64, 'base64');
      if (steemAPI.chainLib.config.whalevault != null) {
        const response = await steemAPI.chainLib.config.whalevault.promiseRequestSignBuffer('wls_busy',
          `wls:${username}`,
          buffer.toString('binary'), 'Posting', 'image_upload', 'raw');
        if (response.error != null) throw response.error;
        path = Signature.fromHex(response.result).toBuffer().toString('base64');
      } else {
        path = Signature.signBuffer(buffer, keys.postingKey)
          .toBuffer()
          .toString('base64');
      }
      const encoded = encodeURIComponent(path);
      const urlPath = `https://whaleshares.io/imageupload/${username}/${encoded}`;
      fetch(urlPath, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          data,
        }),
      })
        .then(url => url.json())
        .then(url => {
          resolve({ url: url.data });
        });
    });
  });
};

export const saveDraft = (post, redirect) => dispatch =>
  dispatch({
    type: SAVE_DRAFT,
    payload: {
      promise: addDraft(post),
    },
    meta: { postId: post.id },
  }).then(() => {
    if (redirect) dispatch(push(`/editor?draft=${post.id}`));
  });

export const deleteDraft = draftIds => dispatch =>
  dispatch({
    type: DELETE_DRAFT,
    payload: {
      promise: removeDraft(draftIds),
    },
    meta: { ids: draftIds },
  });

export const editPost = post => dispatch => {
  const jsonMetadata = jsonParse(post.json_metadata);
  const draft = {
    ...post,
    originalBody: post.body,
    jsonMetadata,
    lastUpdated: new Date(),
    isUpdating: true,
  };
  dispatch(saveDraft({ postData: draft, id: post.id }, true));
};

const requiredFields = 'parentAuthor,parentPermlink,author,permlink,title,body,jsonMetadata'.split(',');

const broadcastComment = (
  parentAuthor,
  parentPermlink,
  author,
  title,
  body,
  jsonMetadata,
  reward,
  upvote,
  permlink,
  authUsername,
  keys,
  steemAPI,
  post_to,
  isUpdating = false,
) => {
  const operations = [];

  if (isUpdating === true) {
    let social_action_comment_update = {
      permlink,
      title,
      body,
      json_metadata: JSON.stringify(jsonMetadata),
    };

    const social_action = [
      "social_action",
      {
        account: author,
        action: [ 1, social_action_comment_update ]
      }
    ];

    operations.push(social_action);
  } else {
    let social_action_comment_create = {
      permlink,
      parent_author: parentAuthor,
      parent_permlink: parentPermlink,
      // allow_votes: true,
      // allow_curation_rewards: true,
      title,
      body,
      json_metadata: JSON.stringify(jsonMetadata),
    };

    switch (post_to) {
      case '': // public
        break;
      case POST_AUDIENCE_FRIENDS:
        social_action_comment_create.allow_friends = true;
        break;
      default: // pod
        social_action_comment_create.pod = post_to;
        break;
    }

    const social_action = [
      "social_action",
      {
        account: author,
        action: [ 0, social_action_comment_create ]
      }
    ];

    operations.push(social_action);
  }

  if (upvote) {
    operations.push([
      'vote',
      {
        voter: author,
        author,
        permlink,
        weight: 10000,
      },
    ]);
  }

  const useKey = steemAPI.chainLib.config.whalevault != null ? `${keys.keys.postingKey}sendPost` : keys.keys.postingKey;

  return steemAPI.chainLib.broadcast.sendAsync({ operations, extensions: [] }, [ useKey ]);
};

export const createPost = (postData, errorMessages) => {
  requiredFields.forEach(field => {
    assert(postData[field] != null, `Developer Error: Missing required field ${field}`);
  });

  const {
    parentAuthor,
    parentPermlink,
    author,
    title,
    body,
    jsonMetadata,
    reward,
    upvote,
    draftId,
    isUpdating,
    post_to,
  } = postData;
  const is_cnc_storage = _.get(jsonMetadata, 'storage', '') === 'cnc';
  let newBody = body;

  return async (dispatch, getState, { steemAPI, Keys }) => {
    try {
      dispatch({ type: CREATE_POST_START});
      await sleep(100);
      if (is_cnc_storage) {
        /**
         * 1. Upload the body content to CNC first
         * 2. Then put the hash to post body
         */

        const {username, keys} = Keys.decrypt();
        const content_base64 = getBase64JsonContent(body);
        const buffer = Buffer.from(content_base64, 'utf8');
        let sig;
        if (steemAPI.chainLib.config.whalevault != null) {
          const response = await steemAPI.chainLib.config.whalevault.promiseRequestSignBuffer('wls_busy', `wls:${username}`,
            buffer.toString('utf8'), 'Posting', 'content_upload', 'raw');
          if (response.error != null) throw response.error;
          sig = Signature.fromHex(response.result).toBuffer().toString('base64');
        } else {
          sig = Signature.signBuffer(buffer, keys.postingKey).toBuffer().toString('base64');
        }

        newBody = await storeCNCContent(username, sig, content_base64);
      } else { // onchain
        newBody = isUpdating ? getBodyPatchIfSmaller(postData.originalBody, body) : body;
      }

      // //////////////////////////////////////////////////
      const permlink = isUpdating
        ? postData.permlink
        : await createPermlink(title, author, parentAuthor, parentPermlink, body);
      const state = getState();
      const authUser = state.auth.user;

      await broadcastComment(
        parentAuthor,
        parentPermlink,
        author,
        title,
        newBody,
        jsonMetadata,
        !isUpdating && reward,
        !isUpdating && upvote,
        permlink,
        authUser.name,
        Keys.decrypt(),
        steemAPI,
        post_to,
        isUpdating,
      );

      if (draftId) {
        dispatch(deleteDraft([draftId]));
        dispatch(addEditedPost(permlink));
      }
      dispatch(push(`/@${author}/${permlink}`));
      dispatch({ type: CREATE_POST_SUCCESS});
    } catch (err) {
      console.log(err);
      dispatch({ type: CREATE_POST_ERROR, payload: {result: err.message}});
      if (err && _.get(err, 'data') && _.get(err, 'data.code') && errorMessages[_.get(err, 'data.code')]) {
        dispatch(notify(errorMessages[err.data.code], 'error'));
      } else {
        dispatch(notify(err.message, 'error'));
      }
    }
  };
};
