import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl, FormattedMessage, FormattedRelative, FormattedDate, FormattedTime } from 'react-intl';
import { Link, withRouter } from 'react-router-dom';
import { Tag, Tooltip } from 'antd';
import formatter from '../../helpers/steemitFormatter';
import { isPostDeleted, isPostTaggedNSFW, dropCategory, isBannedPost } from '../../helpers/postHelpers';
import withAuthActions from '../../auth/withAuthActions';
import StoryPreview from './StoryPreview';
import StoryFooter from '../StoryFooter/StoryFooter';
import { getFromMetadata } from '../../helpers/parser';
import Avatar from '../Avatar';
import Topic from '../Button/Topic';
import NSFWStoryPreviewMessage from './NSFWStoryPreviewMessage';
import HiddenStoryPreviewMessage from './HiddenStoryPreviewMessage';
import DMCARemovedMessage from './DMCARemovedMessage';
import { WALLET_URL } from '../../../common/constants/settings';
import { createPodJoinLink } from '../../utils/wlsuri';
import './Story.less';

const RestrictedPostMessage = (props) => {
  const { post }  = props;
  const postType  = post.pod ? 'Pod' : 'Friends';
  const audience  = post.pod ? `${post.pod} Pod` : 'Friends';
  const message   = { 
                      Pod     : <div><a href={`${WALLET_URL}${createPodJoinLink(post.pod)}`} target='_blank'>Become a member</a> of <Link to={`/@${post.pod}`}>{audience}</Link> to join the conversation...</div>,
                      Friends : <div><Link to={`/@${post.author}`}>{post.author}</Link> posted to {audience}<br/><Link to={`/@${post.author}`}>Become Friends</Link> or check <Link to="/myfeed">My Feed</Link> if you are already friends</div>,
                    };
  return <div className='Story'> 
           <div className='Story__restrictedpostmessage'>
              {message[postType]}
           </div>
         </div>
}

@withRouter
@injectIntl
@withAuthActions
class Story extends React.Component {
  static propTypes = {
    user: PropTypes.shape().isRequired,
    post: PropTypes.shape().isRequired,
    postState: PropTypes.shape().isRequired,
    showNSFWPosts: PropTypes.bool.isRequired,
    onActionInitiated: PropTypes.func.isRequired,
    pendingLike: PropTypes.bool,
    pendingFollow: PropTypes.bool,
    pendingBookmark: PropTypes.bool,
    isMuted: PropTypes.bool,
    saving: PropTypes.bool,
    ownPost: PropTypes.bool,
    history: PropTypes.shape(),
    showPostModal: PropTypes.func,
    votePost: PropTypes.func,
    toggleBookmark: PropTypes.func,
    editPost: PropTypes.func,
    followUser: PropTypes.func,
    unfollowUser: PropTypes.func,
    push: PropTypes.func,
  };

  static defaultProps = {
    pendingLike: false,
    pendingFollow: false,
    pendingBookmark: false,
    saving: false,
    ownPost: false,
    history: {},
    isMuted: false,
    showPostModal: () => {},
    votePost: () => {},
    toggleBookmark: () => {},
    editPost: () => {},
    followUser: () => {},
    unfollowUser: () => {},
    push: () => {},
  };

  constructor(props) {
    super(props);

    this.state = {
      showHiddenStoryPreview: false,
      displayLoginModal: false,
    };

    this.getDisplayStoryPreview = this.getDisplayStoryPreview.bind(this);
    this.handlePostPopoverMenuClick = this.handlePostPopoverMenuClick.bind(this);
    this.handleShowStoryPreview = this.handleShowStoryPreview.bind(this);
    this.handlePostModalDisplay = this.handlePostModalDisplay.bind(this);
    this.handlePreviewClickPostModalDisplay = this.handlePreviewClickPostModalDisplay.bind(this);
    this.handleLikeClick = this.handleLikeClick.bind(this);
    this.handleFollowClick = this.handleFollowClick.bind(this);
    this.handleEditClick = this.handleEditClick.bind(this);
  }

  shouldComponentUpdate(nextProps, nextState) {
    return !_.isEqual(nextProps, this.props) || !_.isEqual(nextState, this.state);
  }

  getDisplayStoryPreview() {
    const { post, showNSFWPosts } = this.props;
    const { showHiddenStoryPreview } = this.state;
    const postAuthorReputation = formatter.reputation(post.author_reputation);

    if (showHiddenStoryPreview) return true;

    if (postAuthorReputation >= 0 && isPostTaggedNSFW(post)) {
      return showNSFWPosts;
    } else if (postAuthorReputation < 0) {
      return false;
    }

    return true;
  }

  handleLikeClick(post, amount) {
    this.props.onActionInitiated(() => this.props.votePost(post.id, post.author, post.permlink, amount));
  }

  handleFollowClick(post) {
    const { userFollowed } = this.props.postState;
    if (userFollowed) {
      this.props.unfollowUser(post.author);
    } else {
      this.props.followUser(post.author);
    }
  }

  handleEditClick(post) {
    if (post.depth === 0) return this.props.editPost(post);
    return this.props.push(`${post.url}-edit`);
  }

  clickMenuItem(key) {
    const { post } = this.props;
    switch (key) {
      case 'follow':
        this.handleFollowClick(post);
        break;
      case 'save':
        this.props.toggleBookmark(post.id, post.author, post.permlink);
        break;
      case 'edit':
        this.handleEditClick(post);
        break;
      default:
    }
  }

  handlePostPopoverMenuClick(key) {
    this.props.onActionInitiated(this.clickMenuItem.bind(this, key));
  }

  handleShowStoryPreview() {
    this.setState({
      showHiddenStoryPreview: true,
    });
  }

  handlePostModalDisplay(e) {
    e.preventDefault();
    const { post } = this.props;
    const isReplyPreview = _.isEmpty(post.title) || post.title !== post.root_title;
    const openInNewTab = _.get(e, 'metaKey', false) || _.get(e, 'ctrlKey', false);
    const postURL = dropCategory(post.url);

    if (isReplyPreview) {
      this.props.history.push(postURL);
    } else if (openInNewTab) {
      if (typeof window !== 'undefined') {
        if (window) {
          const url = `${window.location.origin}${postURL}`;
          window.open(url);
        }
      }
    } else {
      this.props.showPostModal(post);
    }
  }

  handlePreviewClickPostModalDisplay(e) {
    e.preventDefault();

    const { post } = this.props;
    const isReplyPreview = _.isEmpty(post.title) || post.title !== post.root_title;
    const elementNodeName = _.toLower(_.get(e, 'target.nodeName', ''));
    const elementClassName = _.get(e, 'target.className', '');
    const showPostModal = elementNodeName !== 'i' && elementClassName !== 'PostFeedEmbed__playButton';
    const openInNewTab = _.get(e, 'metaKey', false) || _.get(e, 'ctrlKey', false);
    const postURL = dropCategory(post.url);

    if (isReplyPreview) {
      this.props.history.push(postURL);
    } else if (openInNewTab && showPostModal) {
      if (window) {
        const url = `${window.location.origin}${postURL}`;
        window.open(url);
      }
    } else if (showPostModal) {
      this.props.showPostModal(post);
    }
  }

  renderStoryPreview() {
    const { post } = this.props;
    const showStoryPreview = this.getDisplayStoryPreview();
    const hiddenStoryPreviewMessage = isPostTaggedNSFW(post) ? (
      <NSFWStoryPreviewMessage onClick={this.handleShowStoryPreview} />
    ) : (
      <HiddenStoryPreviewMessage onClick={this.handleShowStoryPreview} />
    );

    if (isBannedPost(post)) {
      return <DMCARemovedMessage />;
    }

    return showStoryPreview ? (
      <a
        href={dropCategory(post.url)}
        target="_blank"
        rel="noopener noreferrer"
        onClick={this.handlePreviewClickPostModalDisplay}
        className="Story__content__preview"
      >
        <StoryPreview post={post} />
      </a>
    ) : (
      hiddenStoryPreviewMessage
    );
  }

  render() {
    const {
      user,
      post,
      postState,
      pendingLike,
      pendingFollow,
      pendingBookmark,
      saving,
      ownPost,
      isMuted,
      location,
      feedType,
    } = this.props;

    if (isMuted) return null;

    if (isPostDeleted(post)) return null;

    let whitelist = ['whaleshares'].includes(post.author);
    let allow_friends = post.allow_friends && !whitelist;

    if (location.hash == '#start') {
      const tips = parseInt(post.total_payout_value);
      if (!whitelist && (tips < 5 || allow_friends)) return null;
    }

    /* 
      Listing on ProfilePage
      - publiclisting (not logged in)
      - podnews (News-tab pod account)
      - friendslisting (profile page of friend)
      Feed
      - myfeed
      - publicfeed 
    */

    // not logged in or public feed
    if (feedType === 'publiclisting' || feedType === 'publicfeed' && !ownPost) {
      //if (!ownPost) {
        if (allow_friends && !postState.isFriend) return <RestrictedPostMessage post={post}/>
        //if (post.pod) {
        //  if (!user.pods || !user.pods.includes(post.pod)) return <RestrictedPostMessage post={post}/>;
        //}
      //}
    }

    // Return public posts only on Pod Account News-tab
    if (feedType === 'podnews') {
      if (post.pod || allow_friends) return null;
    }

    if (feedType === 'friendslisting') {
      // replace pod post with message, if friend or follower is not a pod member
      if (post.pod) {
        if (!user.pods || !user.pods.includes(post.pod)) return <RestrictedPostMessage post={post}/>;
      }
    }

    if ( feedType === 'myfeed') {
      //replace friends posts with message, if the author is followed instead of friend
      if (allow_friends && !postState.isFriend) return <RestrictedPostMessage post={post}/>;
      //replace pod posts with message, if user is not a pod member
      //if (post.pod) {
      //  if (!user.pods || !user.pods.includes(post.pod)) return <RestrictedPostMessage post={post}/>;
      //}
    }

    const mood = getFromMetadata(post.json_metadata, 'mood');
    const mentions = getFromMetadata(post.json_metadata, 'users') || [];
    const mentioned = (!ownPost && mentions.length) ? mentions.includes(user.name) : false;
    const showPodJoin = post.pod && (!user.pods || !user.pods.includes(post.pod));

    return (
      <div className="Story" id={`${post.author}-${post.permlink}`}>
      {showPodJoin && <RestrictedPostMessage post={post}/>}
      {mentioned && <div className="mentioned"></div>}
        <div className="Story__content">
          <div className="Story__header">
            <Link to={`/@${post.author}`}>
              <Avatar username={post.author} size={40} />
            </Link>
            <div className="Story__header__text">
              <span className="Story__header__flex">
                <Link to={`/@${post.author}`}>
                  <h4>
                    <span className="username">{post.author}</span>
                  </h4>
                </Link>
                <span className="Story__topics">
                  <Topic name={post.category} />
                </span>
              </span>
              <span className="Story__date">
                <FormattedMessage id="posted" defaultMessage="Posted" />{' for '}

                {post.pod && (<Link to={`/@${post.pod}`}><span className='audience-podpost'>{post.pod} Pod Members</span></Link>)}
                {allow_friends && (<span className="audience-friends">Friends</span>)} 
                {!post.pod && !allow_friends && (<span className="audience-public">Everyone</span>)}
                {' to comment on, '}
                <Tooltip
                  title={
                    <React.Fragment>
                      <FormattedDate value={`${post.created}Z`} /> <FormattedTime value={`${post.created}Z`} />
                    </React.Fragment>
                  }
                >
                  <span>
                    <FormattedRelative value={`${post.created}Z`} />
                  </span>
                </Tooltip>
              </span>
            </div>
          </div>
          <div className="Story__content">
            <a
              href={dropCategory(post.url)}
              target="_blank"
              rel="noopener noreferrer"
              onClick={this.handlePostModalDisplay}
              className = "Story__content__title"
            >
              <h2>
                {post.depth !== 0 && <Tag color="#4f545c">RE</Tag>}
                {post.title || post.root_title}
              </h2>
            </a>
            {this.renderStoryPreview()}
          </div>
          <div className="Story__footer">
            <StoryFooter
              user={user}
              post={post}
              postState={postState}
              pendingLike={pendingLike}
              ownPost={ownPost}
              onLikeClick={this.handleLikeClick}
              onEditClick={this.handleEditClick}
              pendingFollow={pendingFollow}
              pendingBookmark={pendingBookmark}
              saving={saving}
              handlePostPopoverMenuClick={this.handlePostPopoverMenuClick}
              mood={mood}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Story;
