import React from 'react';
import { FormattedMessage } from 'react-intl';

const EmptyUserProfile = (props) => (

  <div className="text-center">
    <h3>
      <FormattedMessage
        id="empty_user_profile"
        defaultMessage="@{user} didn't {type} any stories yet."
        values={{type:(props.type && props.type.length > 0) ? props.type : "publish",
        	user:props.user
        }}
      />
    </h3>
  </div>
);

export default EmptyUserProfile;
